package com.lup.core.controller;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.PageUtils;
import com.lup.core.utils.ResultUtils;

import java.util.List;

public class LupBaseController {
    //page helper分页
    public int startPage(JSONObject data, boolean isSort) {
        return PageUtils.startPage(data, isSort);
    }

    public int startPage(JSONObject data) {
        return PageUtils.startPage(data, true);
    }

    //带分页
    public JSONObject getDataListPage(List<?> list) {
        return ResultUtils.success(PageUtils.pageList(list));
    }

    //不带分页
    public JSONObject getDataList(List<?> list) {
        return ResultUtils.success(list);
    }

    //返回一条数据
    public JSONObject getDataOne(Object obj) {
        return ResultUtils.success(obj);
    }

    //成功信息
    public JSONObject success() {
        return ResultUtils.success("操作成功");
    }

    //成功信息
    public JSONObject success(String msg) {
        return ResultUtils.success(msg);
    }

    //成功信息
    public JSONObject success(JSONObject data) {
        return ResultUtils.success(data);
    }

    //失败信息
    public JSONObject fail(String msg) {
        return ResultUtils.fail(msg);
    }

    //失败信息
    public JSONObject fail() {
        return ResultUtils.fail("操作失败");
    }
}
