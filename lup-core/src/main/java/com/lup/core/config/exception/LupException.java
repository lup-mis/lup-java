package com.lup.core.config.exception;

import java.io.Serial;

public class LupException  extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;

    private int code;
    private String msg;

    public LupException(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public LupException(String msg) {
        this.code = -200;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
}
