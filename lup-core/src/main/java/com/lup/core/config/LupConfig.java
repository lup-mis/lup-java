/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-15 22:28:19;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
//这里哪个模块运行就获取哪个配置的yml
@PropertySource("classpath:application.yml")//读取application.yml文件
@Data
public class LupConfig {

    @Value("${spring.profiles.active}")
    private String env;

    //debug 开关
    @Value("${lup-config.debug:false}")
    private boolean debug;

    //mysql 主机地址
    @Value("${lup-config.db.host:#{null}}")
    private String dbHost;

    //mysql 端口号
    @Value("${lup-config.db.port:#{null}}")
    private String dbPort;

    //mysql 数据库名称
    @Value("${lup-config.db.dbname:#{null}}")
    private String dbName;

    //mysql 数据库用户名
    @Value("${lup-config.db.username:#{null}}")
    private String dbUserName;

    //mysql 数据库密码
    @Value("${lup-config.db.password:#{null}}")
    private String dbPassword;

    //tomcat 端口号
    @Value("${lup-config.server.port:#{null}}")
    private String serverPort;

    //服务器最大上传mb
    @Value("${lup-config.server.max-file-size:#{null}}")
    private String serverMaxFileSize;

    //服务器最大 提交 mb
    @Value("${lup-config.server.max-request-size:#{null}}")
    private String serverMaxRequestSize;

    //redis 主机ip
    @Value("${lup-config.redis.host:#{null}}")
    private String redisHost;

    //redis db 索引
    @Value("${lup-config.redis.database:#{null}}")
    private String redisDatabase;

    //redis 端口号
    @Value("${lup-config.redis.port:#{null}}")
    private String redisPort;

    //redis 密码
    @Value("${lup-config.redis.password:#{null}}")
    private String redisPassword;

    //redis 默认前缀
    @Value("${lup-config.redis.redis-key-prefix:#{null}}")
    private String redisKeyPrefix;

    //雪花id 工作ID (0~31)
    @Value("${lup-config.snowflake-worker-id:#{null}}")
    private long snowflakeWorkerId;

    //雪花id 数据中心ID (0~31)
    @Value("${lup-config.snowflake-datacenter-id:#{null}}")
    private long snowflakeDatacenterId;

    //token 过期时间
    @Value("${lup-config.token-expire-time:#{null}}")
    private int tokenExpireTime;

    //token 续期时间
    @Value("${lup-config.token-need-create-time:#{null}}")
    private int tokenNeedCreateTime;

    //后台静态页面地址的url
    @Value("${lup-config.http-host:#{null}}")
    private String httpHost;

    //后台静态资源路径
    @Value("${lup-config.admin-static-path:#{null}}")
    private String adminStaticPath;

    //附件保存路径
    @Value("${lup-config.upload-path:#{null}}")
    private String uploadPath;

    //允许的附件类型
    @Value("${lup-config.upload-allow-type:#{null}}")
    private String uploadAllowType;

    //java的路径
    @Value("${lup-config.dev-java-workspace:#{null}}")
    private String devJavaWorkspace;

    //html的路径
    @Value("${lup-config.dev-html-workspace:#{null}}")
    private String devHtmlWorkspace;

    //TOKEN 拦截路径
    @Value("${lup-config.token-interceptor-path:#{null}}")
    private String tokenInterceptorPath;

    //阿里云oss配置start
    @Value("${lup-config.oss.endpoint:#{null}}")
    private String ossEndpoint;

    @Value("${lup-config.oss.accessKeyId:#{null}}")
    private String ossAccessKeyId;

    @Value("${lup-config.oss.accessKeySecret:#{null}}")
    private String ossAccessKeySecret;

    @Value("${lup-config.oss.bucketName:#{null}}")
    private String ossBucketName;

    @Value("${lup-config.oss.folder:#{null}}")
    private String ossFolder;

    //阿里云oss配置end
}
