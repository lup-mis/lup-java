/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 05:32:37;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.config.exception;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;

@ControllerAdvice
public class ValidatedExceptionHandler {

    /**
     *  拦截验证Exception类的异常,用于拦截实体类校验为空的方法
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public JSONObject exceptionHandler(MethodArgumentNotValidException e) {
        JSONObject result = new JSONObject(true);
        result.put("code", -200);
        result.put("msg", Objects.requireNonNull(e.getBindingResult().getFieldError()).getDefaultMessage());
        return result;
    }
}