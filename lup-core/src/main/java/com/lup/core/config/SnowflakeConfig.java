/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-15 22:28:19;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.config;

import com.lup.core.utils.RedisUtils;
import com.lup.core.utils.Snowflake;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SnowflakeConfig {

    private final LupConfig lupConfig;
    private final RedisUtils redisUtils;
    public SnowflakeConfig(LupConfig lupConfig, RedisUtils redisUtils) {
        this.lupConfig = lupConfig;
        this.redisUtils = redisUtils;
    }

    @Bean
    @ConditionalOnMissingBean
    public Snowflake snowflakeIdWorker() {
        return new Snowflake(lupConfig.getSnowflakeWorkerId(), lupConfig.getSnowflakeDatacenterId(), this.redisUtils);
    }
}