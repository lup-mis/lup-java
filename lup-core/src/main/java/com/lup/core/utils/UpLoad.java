package com.lup.core.utils;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.LupConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
@Component
public record UpLoad(LupConfig lupConfig) {

    public JSONObject fileUpload(MultipartFile file, String path,  String thumb) {

        String tempPath = lupConfig.getUploadPath() + "uploads/";

        if (path != null && !path.equals("")) {
            String firstChar = String.valueOf(path.charAt(0));
            String endChar = path.substring(path.length() - 1);
            if (firstChar.equals("/")) {
                path = path.substring(1);
            }
            if (endChar.equals("/")) {
                path = path.substring(0, path.length() - 1);
            }
            tempPath = tempPath + path + "/";
        }

        if (null == file) {
            return ResultUtils.fail("获取上传文件失败,请检查file上传组件的名称是否正确");
        } else if (file.isEmpty()) {
            return ResultUtils.fail("没有选择文件");
        } else {
            SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("yyyy");
            String year = simpleDateFormatYear.format(new Date());
            SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MM");
            String month = simpleDateFormatMonth.format(new Date());
            if (path == null || path.equals("")) {
                tempPath = tempPath + "" + year + "" + month + "/";
            }
            int randNum = new Random().nextInt(999999);
            File fileDir = new File(tempPath);
            if (!fileDir.exists()) {
                if (!fileDir.mkdirs()) {
                    return ResultUtils.fail("目录创建失败！");
                }
            }
            String fileOriName = file.getOriginalFilename();


            assert fileOriName != null;
            int lastIndexOf = fileOriName.lastIndexOf(".");
            //获取文件的后缀名 .xxx
            String suffix = fileOriName.substring(lastIndexOf).toLowerCase(Locale.ROOT);
            String[] uploadAllowTypeArray = lupConfig.getUploadAllowType().split(",");
            if (!Arrays.asList(uploadAllowTypeArray).contains(suffix)) {
                return ResultUtils.fail("不支持该文件类型！");
            }

            String filePathNoSuffix = DateUtils.getNowDateString() + "_" + randNum;
            String fileServerName = filePathNoSuffix + suffix;
            String filePath = tempPath + fileServerName;
            File dest = new File(filePath);
            String workSpace = lupConfig.getUploadPath();

            //保存文件
            try {
                file.transferTo(dest);
                Set<String> thumbArray = new HashSet<>();
                //判断是否图片，只有图片才进行裁切
//                BufferedImage bi = ImageIO.read(dest);
                BufferedImage bi = CMYKUtil.readImage(dest);

                if (thumb != null && !thumb.equals("") && bi != null) {
                    //开始对图片进行裁剪
                    String[] extraArray = thumb.split("_");
                    for (String _thumb : extraArray) {
                        String[] _thumbArray = _thumb.split(",");

                        int _width = Integer.parseInt(_thumbArray[0]);
                        int _height = Integer.parseInt(_thumbArray[1]);
                        String newFilePath = tempPath + filePathNoSuffix + "_w" + _width + "_h" + _height + suffix;

                        boolean status = ImageUtil.scale(filePath, newFilePath, _width, _height, true);

                        if (status) {
                            thumbArray.add(lupConfig.getHttpHost() + newFilePath.replace(workSpace, "/"));
                        }
                    }
                }
                JSONObject _json = new JSONObject(true);
                _json.put("suffix", suffix);
                _json.put("oriName", fileOriName);
                _json.put("serverName", fileServerName);
                _json.put("fileSize", new Formatter().format("%.2f", (double) file.getSize() / 1024) + "kb");
                _json.put("filePath", filePath.replace(workSpace, "/"));
                _json.put("fileUrl", lupConfig.getHttpHost() + filePath.replace(workSpace, "/"));
                _json.put("savePath", filePath);
                _json.put("host", lupConfig.getHttpHost());
                _json.put("thumb", StringUtils.join(thumbArray, ","));
                return ResultUtils.success("上传成功", _json);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultUtils.fail("文件上传发生异常");
            }
        }
    }
}