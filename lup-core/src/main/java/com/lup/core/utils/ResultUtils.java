/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Objects;

public class ResultUtils {




    public static JSONObject success() {
        JSONObject json = new JSONObject(true);
        json.put("code", 200);
        json.put("msg", "操作成功");
        return json;
    }

    public static JSONObject success(String msg) {
        JSONObject json = new JSONObject(true);
        json.put("code", 200);
        json.put("msg", msg);
        return json;
    }

    public static JSONObject success(Object obj) {
        JSONObject json = new JSONObject(true);
        json.put("code", 200);
        json.put("msg", "成功");
        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }

    public static JSONObject success(Object obj, Object queryList) {
        JSONObject json = new JSONObject(true);
        json.put("code", 200);
        json.put("msg", "成功");

        json.put("queryList", Objects.requireNonNullElseGet(queryList, JSONArray::new));

        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }


    public static JSONObject success(String msg, Object obj) {
        JSONObject json = new JSONObject(true);
        json.put("code", 200);
        json.put("msg", msg);
        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }

    public static JSONObject fail() {
        JSONObject json = new JSONObject(true);
        json.put("code", -200);
        json.put("msg", "操作失败");
        return json;
    }

    public static JSONObject fail(String msg) {
        JSONObject json = new JSONObject(true);
        json.put("code", -200);
        json.put("msg", msg);
        return json;
    }

    public static JSONObject fail(Object obj) {
        JSONObject json = new JSONObject(true);
        json.put("code", -200);
        json.put("msg", "失败");
        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }

    public static JSONObject fail(String msg, Object obj) {
        JSONObject json = new JSONObject(true);
        json.put("code", -200);
        json.put("msg", msg);
        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }

    public static JSONObject json(int code, String msg) {
        JSONObject json = new JSONObject(true);
        json.put("code", code);
        json.put("msg", msg);
        return json;
    }

    public static JSONObject json(int code, String msg, Object obj) {
        JSONObject json = new JSONObject(true);
        json.put("code", code);
        json.put("msg", msg);
        json.put("data", Objects.requireNonNullElseGet(obj, JSONArray::new));
        return json;
    }
}
