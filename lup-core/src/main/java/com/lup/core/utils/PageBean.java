/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.github.pagehelper.PageInfo;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

public class PageBean<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = -9202109574544652243L;

    private long total; // 总记录数
    private int page; // 第几页
    private int limit; // 每页记录数
    private int pageCount; // 总页数
    private List<T> rows; // 结果集


    /**
     * 包装Page对象，因为直接返回Page对象，在JSON处理以及其他情况下会被当成List来处理， 而出现一些问题。
     */
    public PageBean(List<T> list) {
        PageInfo<?> pageInfo = new PageInfo<>(list);
        this.page = pageInfo.getPageNum();
        this.limit = pageInfo.getPageSize();
        this.total = pageInfo.getTotal();
        this.pageCount = pageInfo.getPages();
        this.rows = list;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}