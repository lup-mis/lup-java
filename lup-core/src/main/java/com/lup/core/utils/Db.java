/**
 * [LupMisAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-22 22:39:30;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public record Db(DataSource dataSource, NamedParameterJdbcTemplate db) {

    //查询多条数据
    public List<Map<String, Object>> fetchAll(String sql, JSONObject pars) {
        Map<String, Object> parsMap = new HashMap<>();
        if (pars != null) {
            parsMap = JSONObject.parseObject(pars.toJSONString());
        }
        List<Map<String, Object>> result = new LinkedList<>();
        List<Map<String, Object>> rows = db.queryForList(sql, parsMap);
        for (Map<String, Object> item : rows) {
            String id = item.getOrDefault("id", "#lup#null#lup#").toString();
            if (!id.equals("#lup#null#lup#")) {
                item.put("id", id);
            }
            result.add(Utils.toReplaceKeyLow(item));
        }
        rows.clear();
        parsMap.clear();
        return result;
    }


    //查询一条数据
    public Map<String, Object> fetch(String sql, JSONObject pars) {

        //判断是否有limit
        if (!sql.toLowerCase(Locale.ROOT).contains(" limit ")) sql = sql + " LIMIT 0,1";

        Map<String, Object> parsMap = new HashMap<>();
        if (pars != null) {
            parsMap = JSONObject.parseObject(pars.toJSONString());
        }
        Map<String, Object> result = null;
        List<Map<String, Object>> rows = db.queryForList(sql, parsMap);
        if (rows.size() > 0) {
            result = Utils.toReplaceKeyLow(rows.get(0));
            String id = result.getOrDefault("id", "#lup#null#lup#").toString();
            if (!id.equals("#lup#null#lup#")) {
                result.put("id", id);
            }
        }
        return result;
    }

    //查询一个字段
    public String fetchColumn(String sql, JSONObject pars) {

        //判断是否有limit
        if (!sql.toLowerCase(Locale.ROOT).contains(" limit ")) sql = sql + " LIMIT 0,1";
        Map<String, Object> parsMap = new HashMap<>();
        if (pars != null) {
            parsMap = JSONObject.parseObject(pars.toJSONString());
        }
        String result = "";
        List<Map<String, Object>> rows = db.queryForList(sql, parsMap);
        if (rows.size() > 0) {
            result = rows.get(0).get(rows.get(0).keySet().toArray()[0].toString()).toString();
        }
        return result;
    }

    //添加
    public int insert(String table, JSONObject pars) {
        JSONArray field = new JSONArray();
        JSONArray bind = new JSONArray();
        Map<String, Object> parsMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : pars.entrySet()) {
            field.add(Utils.humpToLine(entry.getKey()));
            bind.add(":" + Utils.lineToHump(entry.getKey()));
            parsMap.put(Utils.lineToHump(entry.getKey()), entry.getValue());
        }
        String sql = "INSERT INTO " + table + "(" + StringUtils.join(field, ",") + ") VALUES(" + StringUtils.join(bind, ",") + ")";
        return db.update(sql, parsMap);
    }

    //修改,where 多个条件用逗号隔开
    public int update(String table, String where, JSONObject pars) {
        JSONArray setArray = new JSONArray();
        JSONArray whereArray = new JSONArray();
        Map<String, Object> parsMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : pars.entrySet()) {
            String field = Utils.humpToLine(entry.getKey());
            String bind = Utils.lineToHump(entry.getKey());
            String[] whereArr = where.split(",");
            for (String s : whereArr) {
                if (!s.equals(field)) {
                    setArray.add(field + " = :" + bind);
                } else {
                    whereArray.add(field + " = :" + bind);
                }
            }
            parsMap.put(Utils.lineToHump(entry.getKey()), entry.getValue());
        }
        String sql = "UPDATE " + table + " SET " + StringUtils.join(setArray, ",") + " WHERE " + StringUtils.join(whereArray, " AND ");
        return db.update(sql, parsMap);
    }

    //删除,where 多个条件用逗号隔开
    public int delete(String table, String where, JSONObject pars) {
        JSONArray whereArray = new JSONArray();
        Map<String, Object> parsMap = new HashMap<>();
        for (Map.Entry<String, Object> entry : pars.entrySet()) {
            String field = Utils.humpToLine(entry.getKey());
            String bind = Utils.lineToHump(entry.getKey());
            String[] whereArr = where.split(",");
            for (String s : whereArr) {
                if (s.equals(field)) {
                    whereArray.add(field + " = :" + bind);
                }
            }
            parsMap.put(Utils.lineToHump(entry.getKey()), entry.getValue());
        }
        String sql = "DELETE FROM  " + table + " WHERE " + StringUtils.join(whereArray, " AND ");
        return db.update(sql, parsMap);
    }

    //执行sql语句
    public int exec(String sql, JSONObject pars) {
        Map<String, Object> parsMap = new HashMap<>();
        if (pars != null) {
            parsMap = JSONObject.parseObject(pars.toJSONString());
        }
        return db.update(sql, parsMap);
    }

    //分页查询
    public String getCountSql(String sql) {
        String regex = "(^SELECT)(.*?)( FROM .*)";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(sql);
        boolean rs = m.find();
        if (!rs || m.groupCount() != 3) {
            return "";
        } else {
            return m.group(1) + " COUNT(0) " + m.group(3);
        }
    }

    /**
     * 分页查询
     */
    public Map<String, Object> page(String sql, String sqlCount, JSONObject pars) {
        int total;// 总记录数
        int pageCount; // 总页数
        int page = pars.getIntValue("page");
        if (page < 1) page = 1;
        int limit = pars.getIntValue("limit");
        if (limit <= 0) limit = 20;
        total = Integer.parseInt(this.fetchColumn(sqlCount, pars));
        double _pageCount = ((double) total / (double) limit);
        pageCount = (int) (Math.ceil(_pageCount));
        if (page > pageCount) page = pageCount;
        int start = (page - 1) * limit;
        sql += " LIMIT " + start + "," + limit;
        Map<String, Object> parsMap = JSONObject.parseObject(pars.toJSONString());

        List<Map<String, Object>> result = new LinkedList<>();

        List<Map<String, Object>> list = db.queryForList(sql, parsMap);
        for (Map<String, Object> item : list) {
            if (!item.getOrDefault("id", "#lup#null#lup#").toString().equals("#lup#null#lup#")) {
                item.put("id", item.get("id").toString());
            }
            result.add(Utils.toReplaceKeyLow(item));
        }
        Map<String, Object> pageResult = new LinkedHashMap<>();
        pageResult.put("total", total);
        pageResult.put("page", page);
        pageResult.put("limit", limit);
        pageResult.put("pageCount", pageCount);
        pageResult.put("rows", result);
        list.clear();
        parsMap.clear();
        return pageResult;
    }

}
