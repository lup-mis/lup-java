/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class PageUtils {

    /**
     * 分页处理
     */
    public static int startPage(JSONObject dataJson, boolean isSort) {

        String sortField = "id";
        String sortOrder = "desc";
        int page = 1;
        int limit = 20;
        if (dataJson != null) {
            page = JsonUtils.getInt("page", dataJson, page);
            limit = JsonUtils.getInt("limit", dataJson, limit);
        }
        if (isSort) {
            if (dataJson != null) {
                if (StringUtils.isNotEmpty(JsonUtils.getString("sortField", dataJson)) && StringUtils.isNotEmpty(JsonUtils.getString("sortOrder", dataJson))) {
                    if (JsonUtils.getString("sortField", dataJson, "").equals("systemSerialNumber")) {
                        sortField = "id";
                    } else {
                        sortField = Utils.humpToLine(JsonUtils.getString("sortField", dataJson, ""));
                    }
                    sortOrder = Utils.humpToLine(JsonUtils.getString("sortOrder", dataJson, ""));
                }
            }
            PageHelper.startPage(page, limit, sortField + " " + sortOrder);
        } else {
            PageHelper.startPage(page, limit);
        }
        int start = (page - 1) * limit;
        return start + 1;
    }

    public static JSONObject sort(JSONObject dataJson) {
        String sortField = "id";
        String sortOrder = "desc";
        if (dataJson != null) {
            if (StringUtils.isNotEmpty(JsonUtils.getString("sortField", dataJson)) && StringUtils.isNotEmpty(JsonUtils.getString("sortOrder", dataJson))) {
                sortField = dataJson.getString("sortField");
                sortOrder = dataJson.getString("sortOrder");
            }
        } else {
            dataJson = new JSONObject();
        }
        if (sortField.equals("system_serial_number") || sortField.equals("systemSerialNumber")) {
            sortField = "id";
        }
        dataJson.put("sortField", sortField);
        dataJson.put("sortOrder", sortOrder);
        return dataJson;
    }

    /**
     * 分页格式化返回
     */
    public static PageBean<?> pageList(List<?> rows) {
        return new PageBean<>(rows);
    }

}
