/**
 * [LupMisAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-22 22:39:30;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Producer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

@Component
public record KaptchaUtils(Producer captchaProducer, Snowflake snowflake, RedisUtils redisUtils) {

    public JSONObject code(String verifyCodeToken) throws IOException {
        String text = captchaProducer.createText();
        //个位数字相加
        String s1 = text.substring(0, 1);
        String s2 = text.substring(1, 2);
        int count = Integer.parseInt(s1) + Integer.parseInt(s2);
        String operation = "+";
        if (Integer.parseInt(s1) > Integer.parseInt(s2)) {
            operation = "-";
            count = Integer.parseInt(s1) - Integer.parseInt(s2);
        }
        //生成图片验证码
        BufferedImage image = captchaProducer.createImage(s1 + operation + s2 + "=?");
        //转base64
        Base64.Encoder encoder = Base64.getEncoder();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();//io流
        ImageIO.write(image, "png", bo);//写入流中
        byte[] bytes = bo.toByteArray();//转换成字节
        String png_base64 = encoder.encodeToString(bytes).trim();//转换成base64串
        //删除 \r\n
        png_base64 = png_base64.replaceAll("\n", "").replaceAll("\r", "");
        JSONObject json = new JSONObject();
        String token = "verifyCode:" + snowflake.nextId();
        if (StringUtils.isNotEmpty(verifyCodeToken)) token = verifyCodeToken;
        redisUtils.add(token, count, 900, TimeUnit.SECONDS);
        json.put("verifyCodeToken", token);
        json.put("imgBase64", "data:image/png;base64," + png_base64);
        return json;
    }
}
