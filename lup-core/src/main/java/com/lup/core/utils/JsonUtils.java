/**
 * [LupMisAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-22 22:39:30;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import com.alibaba.fastjson.*;

import java.util.*;

public class JsonUtils {

    //实体转json对象
    public static JSONObject domain2JsonObject(Object t){
        return JSONObject.parseObject(JSON.toJSONString(t));
    }
    public static JSONArray domain2JsonArray(Object t){
        return JSONArray.parseArray(JSON.toJSONString(t));
    }
    //json对象转实体
    public static  <T> T jsonObject2Domain(JSONObject json, Class<T> clazz){
        return JSONObject.parseObject(json.toJSONString(), clazz);
    }
    //json 数组转实体
    public static  <T> List<T> jsonArray2Domain(JSONArray jsonArray, Class<T> clazz){
        return JSONArray.parseArray(jsonArray.toJSONString(), clazz);
    }
    /**
     * 根据某字段进行jsonArray进行排序
     */
    public static JSONArray jsonArraySort(JSONArray jsonArr, String sortKey, boolean is_desc) {
        JSONArray sortedJsonArray = new JSONArray();
        List<JSONObject> jsonValues = new ArrayList<>();
        for (int i = 0; i < jsonArr.size(); i++) {
            jsonValues.add(jsonArr.getJSONObject(i));
        }
        jsonValues.sort(new Comparator<>() {
            private final String KEY_NAME = sortKey;

            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = "";
                String valB = "";
                try {
                    valA = a.getString(KEY_NAME);
                    valB = b.getString(KEY_NAME);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (is_desc) {
                    return -valA.compareTo(valB);
                } else {
                    return -valB.compareTo(valA);
                }

            }
        });
        for (int i = 0; i < jsonArr.size(); i++) {
            sortedJsonArray.add(jsonValues.get(i));
        }
        return sortedJsonArray;
    }

    //获取String类型
    public static String getString(String key, JSONObject jsonObject) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return JsoupUtil.clean(eval.toString().trim());
        } catch (Exception e) {
            return null;
        }
    }

    public static String getString(String key, JSONObject jsonObject, String defaultValue) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return JsoupUtil.clean(eval.toString().trim());
        } catch (Exception e) {
            return defaultValue;
        }
    }

    //获取Integer类型
    public static Integer getInt(String key, JSONObject jsonObject) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return Integer.parseInt(JsoupUtil.clean(eval.toString().trim()));
        } catch (Exception e) {
            return null;
        }
    }

    public static Integer getInt(String key, JSONObject jsonObject, int defaultValue) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return Integer.parseInt(JsoupUtil.clean(eval.toString().trim()));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    //获取Long类型
    public static Long getLong(String key, JSONObject jsonObject) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return Long.parseLong(JsoupUtil.clean(eval.toString().trim()));
        } catch (Exception e) {
            return null;
        }
    }

    public static Long getLong(String key, JSONObject jsonObject, long defaultValue) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return Long.parseLong(JsoupUtil.clean(eval.toString().trim()));
        } catch (Exception e) {
            return defaultValue;
        }
    }


    //获取Object类型
    public static Object getObject(String key, JSONObject jsonObject) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return JsoupUtil.clean(eval.toString().trim());
        } catch (Exception e) {
            return null;
        }
    }

    public static Object getObject(String key, JSONObject jsonObject, Object object) {
        try {
            Object eval = JSONPath.eval(jsonObject, key);
            return JsoupUtil.clean(eval.toString().trim());
        } catch (Exception e) {
            return object;
        }
    }

    //把jsonString转为Map
    public static Map<String, Object> jsonStringToMap(String jsonString) {
        Map<String, Object> map = new HashMap<>();
        JSONObject jsonObject = JSONObject.parseObject(jsonString);
        for (Object k : jsonObject.keySet()) {
            Object o = jsonObject.get(k.toString());
            if (o instanceof JSONArray) {
                List<Map<String, Object>> list = new ArrayList<>();
                for (Object obj : (JSONArray) o) {
                    list.add(jsonStringToMap(obj.toString()));
                }
                map.put(k.toString(), list);
            } else if (o instanceof JSONObject) {
                // 如果内层是json对象的话，继续解析
                map.put(k.toString(), jsonStringToMap(o.toString()));
            } else {
                // 如果内层是普通对象的话，直接放入map中
                // map.put(k.toString(), o.toString().trim());
                if (o instanceof String) {
                    //如果是字符串类型则过滤xss
                    map.put(k.toString(), JsoupUtil.clean(o.toString().trim()));
                } else {
                    map.put(k.toString(), o);
                }
            }
        }
        return map;
    }

}
