/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import eu.bitwalker.useragentutils.Version;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.DigestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class Utils {



    public static boolean arrayContains(String[] array, Object search) {
        return Arrays.asList(array).contains(search);
    }

    /**
     * 判断是否为空
     */
    public static boolean empty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj.toString().trim().equals("")) {
            return true;
        } else if (obj.toString().trim().equals("0")) {
            return true;
        }
        return false;
    }

    /**
     * 判断是否为空
     */
    public static boolean empty(Object obj, boolean isZero) {
        if (obj == null) {
            return true;
        } else if (obj.toString().trim().equals("")) {
            return true;
        }
        if (isZero) {
            return obj.toString().trim().equals("0");
        }
        return false;
    }
    //md5加密
    public static String md5(String str) {
        return DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 获取uuid
     */
    public static String uuid() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replace("-", "");
        return uuid;
    }

    public static String getOperatingSystem() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            OperatingSystem operatingSystem = UserAgent.parseUserAgentString(request.getHeader("User-Agent")).getOperatingSystem();
            return operatingSystem.getName() + " / " + operatingSystem.getDeviceType();
        } catch (Exception e) {
            return "unknown";
        }
    }

    public static String getBrowser() {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            //获取浏览器信息
            Browser browser = UserAgent.parseUserAgentString(request.getHeader("User-Agent")).getBrowser();
            //获取浏览器版本号
            Version version = browser.getVersion(request.getHeader("User-Agent"));
            return browser.getName() + " / " + version.getVersion();
        } catch (Exception e) {
            return "unknown";
        }
    }

    public static String getUserAgent() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return request.getHeader("User-Agent");
    }

    //密码加密
    public static String password(String str) {
        str = str + "https://www.lizhongwen.com";
        return DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
    }

    //获取签名字符串
    public static String getSign(String str, String secretKey) {
        str = str + "secretKey=" + secretKey;
        return DigestUtils.md5DigestAsHex(str.getBytes(StandardCharsets.UTF_8));
    }

    //工具 ,map key 转下划线
    public static Map<String, Object> toReplaceKeyLow(Map<String, Object> map) {
        Map<String, Object> re_map = new LinkedHashMap<>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            re_map.put(underlineToCamels(entry.getKey()), map.get(entry.getKey()));
        }
        map.clear();
        return re_map;
    }

    public static String underlineToCamels(String inputString) {
        StringBuilder sb = new StringBuilder();
        boolean nextUpperCase = false;
        for (int i = 0; i < inputString.length(); i++) {
            char c = inputString.charAt(i);
            if (c == '_') {
                if (sb.length() > 0) {
                    nextUpperCase = true;
                }
            } else {
                if (nextUpperCase) {
                    sb.append(Character.toUpperCase(c));
                    nextUpperCase = false;
                } else {
                    sb.append(Character.toLowerCase(c));
                }
            }
        }
        return sb.toString();
    }

    //驼峰转下划线
    public static String humpToLine(String str) {
        Pattern humpPattern = Pattern.compile("[A-Z]");
        Matcher matcher = humpPattern.matcher(str);
        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    //首字母大写
    public static String ucFirst(String str) {
        str = str.substring(0, 1).toUpperCase() + str.substring(1);
        return str;

    }

    //首字母小写
    public static String lcFirst(String str) {
        str = str.substring(0, 1).toLowerCase() + str.substring(1);
        return str;

    }

    //下划线转驼峰
    public static String lineToHump(String str) {
        str = str.toLowerCase();
        Pattern linePattern = Pattern.compile("_(\\w)");
        Matcher matcher = linePattern.matcher(str);
        StringBuilder sb = new StringBuilder();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 获取resource目录下的文件
     */
    public static String readResourcesFile(String file) {
        try {

            Resource resource = new ClassPathResource(file);
            InputStream is = resource.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);

            StringBuilder str = new StringBuilder();
            BufferedReader br = new BufferedReader(isr);
            String data;
            while ((data = br.readLine()) != null) {
                str.append(data).append(" \n");
            }
            br.close();
            isr.close();
            is.close();
            return str.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //读取文件
    public static String readFile(String fileName) {
        File file = new File(fileName);
        BufferedReader reader = null;
        StringBuilder sbf = new StringBuilder();
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempStr;
            while ((tempStr = reader.readLine()) != null) {
                sbf.append(tempStr);
            }
            reader.close();
            return sbf.toString();
        } catch (IOException e) {
            log.error(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    log.error(e1.getMessage());
                }
            }
        }
        return sbf.toString();
    }
    /**
     * 删除单个文件
     *
     * @param sPath 被删除文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 路径为文件且不为空则进行删除
        if (file.isFile() && file.exists()) {
            file.delete();
            flag = true;
        }
        return flag;
    }
    /**
     * 删除目录（文件夹）以及目录下的文件
     *
     * @param sPath 被删除目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String sPath) {
        //如果sPath不以文件分隔符结尾，自动添加文件分隔符
        if (!sPath.endsWith(File.separator)) {
            sPath = sPath + File.separator;
        }
        File dirFile = new File(sPath);
        //如果dir对应的文件不存在，或者不是一个目录，则退出
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        //删除文件夹下的所有文件(包括子目录)
        File[] files = dirFile.listFiles();
        assert files != null;
        for (File file : files) {
            //删除子文件
            if (file.isFile()) {
                flag = deleteFile(file.getAbsolutePath());
            } //删除子目录
            else {
                flag = deleteDirectory(file.getAbsolutePath());
            }
            if (!flag) break;
        }
        if (!flag) return false;
        //删除当前目录
        return dirFile.delete();
    }
    public static boolean deletePath(String sPath) {
        boolean flag = false;
        File file = new File(sPath);
        // 判断目录或文件是否存在
        if (!file.exists()) {  // 不存在返回 false
            return flag;
        } else {
            // 判断是否为文件
            if (file.isFile()) {  // 为文件时调用删除文件方法
                return deleteFile(sPath);
            } else {  // 为目录时调用删除目录方法
                return deleteDirectory(sPath);
            }
        }
    }
    //写文件
    public static void write(String filePath, String str) {
        try {

            File file = new File(filePath);
            if (!file.getParentFile().exists()) {
                if (!file.getParentFile().mkdirs()) {
                    return;
                }
            }
            FileOutputStream fos = new FileOutputStream(filePath);
            // 创建转换流对象，构造方法，绑定字节输出流
            OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
            osw.write(str);
            osw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static boolean isDemo(String uri){
        String[] coreController = new String[]{
                "sys/account/edit",
                "sys/account/del",
                "sys/account/add",
                "sys/account/updPwd",
                "sys/action/del",
                "sys/action/add",
                "sys/action/edit",
                "sys/ajax/tableEdit",
                "sys/config/edit",
                "sys/dict/del",
                "sys/dict/add",
                "sys/dict/edit",
                "sys/dictData/add",
                "sys/dictData/del",
                "sys/dictData/edit",
                "sys/field/add",
                "sys/field/del",
                "sys/field/edit",
                "sys/files/add",
                "sys/files/del",
                "sys/files/edit",
                "sys/org/add",
                "sys/org/del",
                "sys/org/edit",
                "sys/page/add",
                "sys/page/del",
                "sys/page/edit",
                "sys/page/initBtn",
                "sys/pathReplace/doReplace",
                "sys/role/add",
                "sys/role/del",
                "sys/role/edit",
                "sys/table/add",
                "sys/table/del",
                "sys/table/edit",
                "sys/tree/add",
                "sys/tree/del",
                "sys/tree/edit"
        };
        for (String item : coreController){
            if(uri.contains(item)){
                return true;
            }
        }
        return false;
    }

}
