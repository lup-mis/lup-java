package com.lup.core.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;

 */
public class ImageUtil {
    /**
     * 缩放图片方法
     *
     * @param srcImageFile 要缩放的图片路径
     * @param result       缩放后的图片路径
     * @param height       目标高度像素
     * @param width        目标宽度像素
     * @param bb           是否补白
     */
    public static boolean scale(String srcImageFile, String result,  int width,int height, boolean bb) {
        try {
            double ratio; // 缩放比例
            File f = new File(srcImageFile);
            BufferedImage bi = ImageIO.read(f);
            Image item = bi.getScaledInstance(width, height, Image.SCALE_SMOOTH);//bi.SCALE_SMOOTH  选择图像平滑度比缩放速度具有更高优先级的图像缩放算法。
            // 计算比例
            if ((bi.getHeight() > height) || (bi.getWidth() > width)) {
                double ratioHeight = (Integer.valueOf(height)).doubleValue() / bi.getHeight();
                double ratioWidth = (Integer.valueOf(width)).doubleValue() / bi.getWidth();
                ratio = Math.max(ratioHeight, ratioWidth);
                AffineTransformOp op = new AffineTransformOp(AffineTransform//仿射转换
                        .getScaleInstance(ratio, ratio), null);//返回表示剪切变换的变换
                item = op.filter(bi, null);//转换源 BufferedImage 并将结果存储在目标 BufferedImage 中。
            }
            if (bb) {//补白
                BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);//构造一个类型为预定义图像类型之一的 BufferedImage。
                Graphics2D g = image.createGraphics();//创建一个 Graphics2D，可以将它绘制到此 BufferedImage 中。
                g.setColor(Color.white);//控制颜色
                g.fillRect(0, 0, width, height);// 使用 Graphics2D 上下文的设置，填充 Shape 的内部区域。
                if (width == item.getWidth(null)) {
                    g.drawImage(item, 0, (height - item.getHeight(null)) / 2, item.getWidth(null), item.getHeight(null), Color.white, null);
                } else {
                    g.drawImage(item, (width - item.getWidth(null)) / 2, 0, item.getWidth(null), item.getHeight(null), Color.white, null);
                }
                g.dispose();
                item = image;
            }
            File outFile = new File(result);
            if (!outFile.exists() && !outFile.isDirectory()) {
                outFile.mkdir();
            }
            ImageIO.write((BufferedImage) item, "JPEG", outFile);
            //输出压缩图片
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}