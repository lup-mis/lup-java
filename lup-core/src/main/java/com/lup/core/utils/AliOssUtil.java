package com.lup.core.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.VoidResult;
import com.lup.core.config.LupConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public record AliOssUtil(LupConfig lupConfig) {

    //删除单个文件
    public boolean fileDelete(String objectName){
        String endpoint = lupConfig.getOssEndpoint();
        String accessKeyId = lupConfig.getOssAccessKeyId();
        String accessKeySecret = lupConfig.getOssAccessKeySecret();
        String bucketName = lupConfig.getOssBucketName();
        try {
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            VoidResult voidResult = ossClient.deleteObject(bucketName, objectName);
            ossClient.shutdown();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //上传文件
    public JSONObject fileUpload(MultipartFile file, String path,  String thumb) {
        String endpoint = lupConfig.getOssEndpoint();
        String accessKeyId = lupConfig.getOssAccessKeyId();
        String accessKeySecret = lupConfig.getOssAccessKeySecret();
        String bucketName = lupConfig.getOssBucketName();
        String folder = lupConfig.getOssFolder();

        String tempPath = folder;
        if (path != null && !path.equals("")) {
            String firstChar = String.valueOf(path.charAt(0));
            String endChar = path.substring(path.length() - 1);
            if (firstChar.equals("/")) {
                path = path.substring(1);
            }
            if (endChar.equals("/")) {
                path = path.substring(0, path.length() - 1);
            }
            tempPath = tempPath + path + "/";
        }
        if (null == file) {
            return ResultUtils.fail("获取上传文件失败,请检查file上传组件的名称是否正确");
        } else if (file.isEmpty()) {
            return ResultUtils.fail("没有选择文件");
        } else {
            SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("yyyy");
            String year = simpleDateFormatYear.format(new Date());
            SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MM");
            String month = simpleDateFormatMonth.format(new Date());
            if (path == null || path.equals("")) {
                tempPath = tempPath + "" + year + "" + month + "/";
            }
            int randNum = new Random().nextInt(999999);

            String fileOriName = file.getOriginalFilename();
            assert fileOriName != null;
            int lastIndexOf = fileOriName.lastIndexOf(".");
            //获取文件的后缀名 .xxx
            String suffix = fileOriName.substring(lastIndexOf).toLowerCase(Locale.ROOT);
            String[] uploadAllowTypeArray = lupConfig.getUploadAllowType().split(",");
            if (!Arrays.asList(uploadAllowTypeArray).contains(suffix)) {
                return ResultUtils.fail("不支持该文件类型！");
            }

            String filePathNoSuffix = DateUtils.getNowDateString() + "_" + randNum;
            String fileServerName = filePathNoSuffix + suffix;
            String filePath = tempPath + fileServerName;
            try {
                // 创建OSSClient实例。
                OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
                InputStream inputStream = file.getInputStream();
                //oss方法实现上传
                //第一个参数 bucket名称
                //第二个参数 上传到oss文件路径和名称 fileName
                //第三个参数 上传文件输入流
                ossClient.putObject(bucketName, filePath, inputStream);
                ossClient.shutdown();
                //把上传之后文件路径返回
                //需要把上传到阿里云oss路径手动拼接出来
                String host = "https://" + bucketName + "." + endpoint;
                String fileUrl = host + "/" + filePath;
                JSONObject retJson = new JSONObject(true);
                retJson.put("suffix", suffix);
                retJson.put("oriName", fileOriName);
                retJson.put("serverName", fileServerName);
                retJson.put("fileSize", new Formatter().format("%.2f", (double) file.getSize() / 1024) + "kb");
                retJson.put("filePath", filePath);
                retJson.put("fileUrl", fileUrl);
                retJson.put("savePath", filePath);
                retJson.put("host", host);
                retJson.put("thumb", "");
                return ResultUtils.success("上传成功", retJson);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultUtils.fail("文件上传发生异常");
            }
        }
    }
}
