/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-15 22:28:19;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {


    /**
     * 获取精确到秒的时间戳
     */
    public static Integer getTimeStampSecond() {
        return Integer.parseInt(String.valueOf((System.currentTimeMillis() / 1000)));
    }

    /**
     * 获取时间戳-毫秒
     */
    public static Long getTimestamp() {
        return System.currentTimeMillis();

    }

    /**
     * 获取当前Date型日期
     *
     * @return Date() 当前日期
     */
    public static Date getNowDate() {
        return new Date();
    }

    public static String timeStamp2DateTime(String timeStamp) {
        long time = Long.parseLong(timeStamp + "000");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
        Date date = new Date(time);
        String res;
        res = simpleDateFormat.format(date);

        return res;
    }

    public static String timeStamp2DateTimeStandard(String timeStamp) {
        long time = Long.parseLong(timeStamp + "000");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(time);
        String res;
        res = simpleDateFormat.format(date);
        return res;
    }

    public static String getNowDateString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSS");
        Date date = new Date();
        String res;
        res = simpleDateFormat.format(date);
        return res;
    }

    public static String getNowDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

    public static String getNowDateTimeMillisecond() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }
}