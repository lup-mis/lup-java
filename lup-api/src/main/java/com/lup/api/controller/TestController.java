package com.lup.api.controller;


import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.AliOssUtil;
import com.lup.core.utils.Db;
import com.lup.core.utils.Snowflake;
import com.lup.core.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
@AllArgsConstructor
public class TestController {
    private final Db db;
    final AliOssUtil aliOssUtil;
    final Snowflake snowflake;
    @RequestMapping(value = "/test")
    public String test() {
        List<Map<String, Object>> maps = db.fetchAll("select * from lup_test_user", null);
        System.out.println(maps);

        return Utils.getSign("1234561112122122","123");
    }

    @RequestMapping(value = "/ossUpload")
    public JSONObject ossUpload(@RequestParam("file") MultipartFile file) {
        JSONObject jsonObject = aliOssUtil.fileUpload(file, "", "");
        System.out.println(jsonObject);
        return jsonObject;
    }

    @RequestMapping(value = "/ossDel")
    public void ossDel() {
        aliOssUtil.fileDelete("2022-08-01/d88ea1a9cf21438580b976dcd59d5abd1首页1230.jpg");
    }

    @RequestMapping(value = "/p")
    public void p(){
        List<Map<String, Object>> maps = db.fetchAll("select * from sys_areas", null);
        for (int i=0;i<maps.size();i++){
            Object id = maps.get(i).get("id");
            String s = snowflake.nextId();
            db.exec("update sys_areas set id='"+s+"' where id="+id,null);
        }
    }
}
