/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.Utils;

import java.util.Objects;

public class BuildController {
    public String getController(JSONObject sysTable) {

        String tableName = sysTable.getString("tableName");
        String tableRemarks = sysTable.getString("tableRemarks");
        String smallHumpTableName = Utils.lineToHump(tableName);
        String domain = Utils.ucFirst(Utils.lineToHump(tableName));
        String domainName = Utils.lcFirst(Utils.lineToHump(tableName));
        String prefix = tableName.split("_")[0];
        String roleCodePrefix = prefix + ":" + Utils.lcFirst(smallHumpTableName.substring(prefix.length()));
        String routePrefix = prefix + "/" + Utils.lcFirst(smallHumpTableName.substring(prefix.length()));
        String generationTime = DateUtils.getNowDateTime();

        return Objects.requireNonNull(Utils.readResourcesFile("tpl/controller.java.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$domain}", domain)
                .replace("{$domainName}", domainName)
                .replace("{$prefix}", prefix)
                .replace("{$routePrefix}", routePrefix)
                .replace("{$roleCodePrefix}", roleCodePrefix)
                .replace("{$tableRemarks}", tableRemarks);
    }
}
