/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BuildXml {
    public String getXml(JSONObject sysTable, JSONArray sysFieldList){

        String tableName = sysTable.getString("tableName");
        String prefix = tableName.split("_")[0];
        String domain = Utils.ucFirst(Utils.lineToHump(tableName));
        String domainName = Utils.lcFirst(Utils.lineToHump(tableName));
        String generationTime = DateUtils.getNowDateTime();
        List<String> selectFieldLists = new ArrayList<>();
        List<String> fieldLists = new ArrayList<>();
        List<String> addFieldLists = new ArrayList<>();
        List<String> editFieldLists = new ArrayList<>();

        List<String> searchWhereArr = new ArrayList<>();
        List<String> searchLuActionWhereArr = new ArrayList<>();
        for (int i=0;i<sysFieldList.size();i++) {
            JSONObject item = sysFieldList.getJSONObject(i);
            String fieldName = item.getString("fieldName");
            String smallHumpFieldName = Utils.lineToHump(fieldName);
            String dataCheckType = item.getString("dataCheckType");
            String searchCondition = item.getString("searchCondition");
            //xml组装字段
            //fieldLists.add(fieldName);
            //addFieldLists.add("#{" + smallHumpFieldName + "}");
            selectFieldLists.add(fieldName);
            fieldLists.add("                <if test=\""+smallHumpFieldName+" != null\">"+fieldName+",</if>");
            addFieldLists.add("                <if test=\""+smallHumpFieldName+" != null\">#{"+smallHumpFieldName+"},</if>");
            if (!fieldName.equals("id")) {
                //设置密码为空不修改
                if (dataCheckType.equals("password")) {
                    editFieldLists.add("<if test=\"" + smallHumpFieldName + " != null and " + smallHumpFieldName + " != ''\">" + fieldName + " = #{" + smallHumpFieldName + "},</if>");
                } else {
                    editFieldLists.add("<if test=\"" + smallHumpFieldName + " != null\">" + fieldName + " = #{" + smallHumpFieldName + "},</if>");
                }
            }
            //xml 查询条件处理 $searchWhere start
            switch (searchCondition) {
                case "equals" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " = #{" + smallHumpFieldName + "}</if>");
                case "like" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " LIKE CONCAT('%',#{" + smallHumpFieldName + "},'%')</if>");
                case "likeLeft" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " LIKE CONCAT('%',#{" + smallHumpFieldName + "})</if>");
                case "likeRight" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " LIKE CONCAT(#{" + smallHumpFieldName + "},'%')</if>");
                case ">=" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " >= #{" + smallHumpFieldName + "}</if>");
                case "<=" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND " + fieldName + " <= #{" + smallHumpFieldName + "}</if>");
                case "between" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + "BetweenStart !=null and " + smallHumpFieldName + "BetweenStart !='' and " + smallHumpFieldName + "BetweenEnd !=null and " + smallHumpFieldName + "BetweenEnd !=''\"> AND (" + fieldName + " BETWEEN #{" + smallHumpFieldName + "BetweenStart} AND #{" + smallHumpFieldName + "BetweenEnd})</if>");
                case "find_in_set" -> searchWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> AND (<foreach item=\"item\" index=\"index\" collection=\"" + smallHumpFieldName + ".split(',')\" separator=\"or\"> FIND_IN_SET(#{item}," + fieldName + ") </foreach>)</if>");
            }

            switch (searchCondition) {
                case "equals" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " = #{" + smallHumpFieldName + "}</if>");
                case "like" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " LIKE CONCAT('%',#{" + smallHumpFieldName + "},'%')</if>");
                case "likeLeft" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " LIKE CONCAT('%',#{" + smallHumpFieldName + "})</if>");
                case "likeRight" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " LIKE CONCAT(#{" + smallHumpFieldName + "},'%')</if>");
                case ">=" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " >= #{" + smallHumpFieldName + "}</if>");
                case "<=" -> searchLuActionWhereArr.add("<if test=\"" + smallHumpFieldName + " !=null and " + smallHumpFieldName + " != ''\"> OR " + fieldName + " <= #{" + smallHumpFieldName + "}</if>");
            }
            //xml 查询条件处理 $searchWhere end
        }
        //组装mybatis 搜索条件
        String mybatisSearch = "";
        if (searchWhereArr.size() > 0 && searchLuActionWhereArr.size() > 0) {
            mybatisSearch += "<choose>\r\n";
            mybatisSearch += "                <when test=\"lupAction == 'search'\">\r\n                    ";
            mybatisSearch += StringUtils.join(searchLuActionWhereArr, "\r\n                    ")+"\r\n";
            mybatisSearch += "                </when>\r\n";
            mybatisSearch += "                <otherwise>\r\n                    ";
            mybatisSearch += StringUtils.join(searchWhereArr, "\r\n                    ")+"\r\n";
            mybatisSearch += "                </otherwise>\r\n";
            mybatisSearch += "            </choose>\r\n";
        } else {
            mybatisSearch += StringUtils.join(searchWhereArr, "\r\n            ");
        }
        return Objects.requireNonNull(Utils.readResourcesFile("tpl/mapper.xml.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$domain}", domain)
                .replace("{$domainName}", domainName)
                .replace("{$prefix}", prefix)
                .replace("{$table_name}", tableName)
                .replace("{$fields}", StringUtils.join(selectFieldLists, ","))
                .replace("{$addFieldKey}", StringUtils.join(fieldLists, "\r\n"))
                .replace("{$addFieldValue}", StringUtils.join(addFieldLists, "\r\n"))
                .replace("{$searchWhere}", mybatisSearch)
                .replace("{$editField}", StringUtils.join(editFieldLists, "\r\n            "));
    }
}
