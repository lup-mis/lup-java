/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.LupConfig;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Utils;
import lombok.AllArgsConstructor;

import java.io.File;
import java.util.Objects;

@AllArgsConstructor
public class BuildAddHtml {
    private final LupConfig lupConfig;
    public String getAddHtml(JSONObject sysTable, JSONArray sysFieldList){

        String addForm = "";
        String addInitDictData = "";
        String addInitDbTableData = "";
        String addInitTreeParentData = "";
        String initEditor = "";
        String wangEditorJs = "";

        String generationTime = DateUtils.getNowDateTime();
        String tableName = sysTable.getString("tableName");
        String prefix = tableName.split("_")[0];
        for (int i = 0; i < sysFieldList.size(); i++) {
            JSONObject item = sysFieldList.getJSONObject(i);
            String fieldName = item.getString("fieldName");
            String smallHumpFieldName = Utils.lineToHump(fieldName);
            String fieldRemarks = item.getString("fieldRemarks");
            String fieldType = item.getString("fieldType");
            String dataCheckType = item.getString("dataCheckType");
            String dataSrcType = item.getString("dataSrcType");
            String customSrc = item.getString("customSrc");
            String inputType = item.getString("inputType");
            String dictCode = item.getString("dictCode");
            int must = item.getIntValue("must");
            JSONObject customSrcJson = new JSONObject(true);
            if (!customSrc.equals("")) {
                customSrcJson = JSONObject.parseObject(customSrc);
            }
            String mustTag = "";
            String layVerify = "";
            if (must == 1) {
                if (inputType.equals("radio") || inputType.equals("checkbox")) {
                    layVerify = "otherReq";
                } else {
                    layVerify = "required";
                }
                mustTag = "<b style='color:red'>*</b>";
            }
            //处理表单内容 检索，添加，编辑 start
            switch (inputType) {
                case "input":
                    //处理input表单类型
                    //组装添加表单
                    if (!fieldName.equals("id") && !fieldName.equals("create_time")) {
                        String _inputType = "text";
                        String defaultDate = "";
                        String dateClass = "";
                        switch (dataCheckType) {
                            case "money" :
                            case "float" :
                            case "int" : {
                                _inputType = "number";
                                break;
                            }
                            case "ymd" : {
                                dateClass = "date";
                                defaultDate = "default-date=\"now\"";
                                break;
                            }
                            case "ymdhis" :
                            case "timeMs" :
                            case "time" : {
                                dateClass = "datetime";
                                defaultDate = "default-date=\"now\"";
                                break;
                            }
                            case "his" : {
                                dateClass = "time";
                                defaultDate = "default-date=\"now\"";
                                break;
                            }
                        }
                        addForm += "            <div class=\"layui-col-md6\">\r\n";
                        addForm += "                <div class=\"layui-form-item\">\r\n";
                        addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                        addForm += "                    <div class=\"layui-input-block\">\r\n";
                        addForm += "                       <input type=\"" + _inputType + "\" name=\"" + smallHumpFieldName + "\" " + defaultDate + " lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请输入" + fieldRemarks + "\" placeholder=\"请输入" + fieldRemarks + "\" autocomplete=\"off\" class=\"layui-input data-check-type-" + dataCheckType + " field-type-" + fieldType + " " + dateClass + "\">\r\n";
                        addForm += "                    </div>\r\n";
                        addForm += "                </div>\r\n";
                        addForm += "            </div>\r\n";
                    }
                    break;
                case "select": {
                    //处理select表单类型
                    String selectAttr = "";
                    String cssClass = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        //原生下拉数据字典
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClass = "class=\"dict-select\"";
                        addInitDictData = "initDictData('add', function (id, data) {\r\n    });";
                    } else if (dataSrcType.equals("db-table")) {
                        //原生下拉一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        cssClass = "class=\"db-table-select data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        addInitDbTableData = "initDbTableData('add', function (id, data) {\r\n    });";
                    }
                    //组装添加表单
                    addForm += "            <div class=\"layui-col-md6\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <select id=\""+smallHumpFieldName+"\" name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" tips=\"请选择" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " lay-verify=\"" + layVerify + "\" lay-reqtext=\"请选择" + fieldRemarks + "\" lay-search lay-filter=\"select-filter\">\r\n";
                    addForm += "                           <option value=\"\"> 请选择" + fieldRemarks + "</option>\r\n";
                    addForm += "                        </select>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "xmSelectNoPage":
                case "xmMultiSelectNoPage": {
                    //下拉选择无分页（下拉单选和下拉多选）
                    String selectAttr = "";
                    String cssClass = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        //数据字典
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClass = "class=\"dict-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDictData = "initDictData('add', function (id, data) {\r\n    });";
                    } else if (dataSrcType.equals("db-table")) {
                        //数据库
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        cssClass = "class=\"db-table-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDbTableData = "initDbTableData('add', function (id, data) {\r\n    });";
                    } else if (dataSrcType.equals("tree-parent")) {
                        //树表parent字段
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String pid = JsonUtils.getString("pid", customSrcJson, "0");
                            selectAttr = "api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\" pid=\"" + pid + "\"";
                        }
                        cssClass = "class=\"tree-parent-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitTreeParentData = "initTreeParentData('add');";
                    }
                    //组装添加表单
                    addForm += "            <div class=\"layui-col-md6\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <div style=\"min-width:200px;\" id=\"" + smallHumpFieldName + "\" name=\"" + smallHumpFieldName + "\" tips=\"请选择" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " lup-lay-verify=\"" + layVerify + "\" lay-reqText=\"请选择" + fieldRemarks + "\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "xmSelect":
                case "xmMultiSelect":
                    //下拉有分页(下拉有分页只能来源数据库)
                    if (dataSrcType.equals("db-table")) {
                        String search = "";
                        String selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            search = JsonUtils.getString("search", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        String cssClass = "class=\"db-table-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDbTableData = "initDbTableData('add', function (id, data) {\r\n    });";                        //组装添加表单
                        addForm += "            <div class=\"layui-col-md6\">\r\n";
                        addForm += "                <div class=\"layui-form-item\">\r\n";
                        addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                        addForm += "                    <div class=\"layui-input-block\">\r\n";
                        addForm += "                        <div style=\"min-width:200px;\" search=\"" + search + "\" id=\"" + smallHumpFieldName + "\" name=\"" + smallHumpFieldName + "\" tips=\"请选择" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " lup-lay-verify=\"" + layVerify + "\" lay-reqText=\"请选择" + fieldRemarks + "\"></div>\r\n";
                        addForm += "                    </div>\r\n";
                        addForm += "                </div>\r\n";
                        addForm += "            </div>\r\n";

                    }
                    break;
                case "textEditor":
                    //富文本编辑器，这里用wangEditor
                    wangEditorJs = "<script src=\"../../../js/wangEditor.min.js\"></script>\r\n";
                    initEditor += "editor(\"" + smallHumpFieldName + "\");\r\n";
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md12\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <textarea style=\"visibility: hidden; height: 0px !important;top: 0px;position:absolute;\" name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请输入" + fieldRemarks + "\" placeholder=\"请输入" + fieldRemarks + "\" autocomplete=\"off\"></textarea>\r\n";
                    addForm += "                        <div id=\"" + smallHumpFieldName + "\" style=\"min-width:400px;\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                case "textarea":
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md12\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <textarea  name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请输入" + fieldRemarks + "\" placeholder=\"请输入" + fieldRemarks + "\" autocomplete=\"off\" class=\"layui-textarea data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"></textarea>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                case "singleFile": {
                    //单附件信息
                    String exts = JsonUtils.getString("exts", customSrcJson, "");
                    String path = JsonUtils.getString("path", customSrcJson, "");
                    String thumb = JsonUtils.getString("thumb", customSrcJson, "");
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md6\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <input type=\"text\" onclick=\"openUploadFileBox(this,'')\" thumb=\"" + thumb + "\" path=\"" + path + "\" exts=\"" + exts + "\" name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请上传" + fieldRemarks + "\" placeholder=\"请上传" + fieldRemarks + "\" autocomplete=\"off\" class=\"layui-input upload data-check-type-" + dataCheckType + " field-type-" + fieldType + "\">\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "multiFile": {
                    //多附件上传
                    String exts = JsonUtils.getString("exts", customSrcJson, "");
                    String path = JsonUtils.getString("path", customSrcJson, "");
                    String thumb = JsonUtils.getString("thumb", customSrcJson, "");
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md12\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <div thumb=\"" + thumb + "\" path=\"" + path + "\" exts=\"" + exts + "\" name=\"" + smallHumpFieldName + "\" lup-lay-verify=\"" + layVerify + "\" lay-reqtext=\"请上传" + fieldRemarks + "\" placeholder=\"请上传" + fieldRemarks + "\" class=\"multiFile\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "singleFileRemarks": {
                    //单附件上传带描述
                    String exts = JsonUtils.getString("exts", customSrcJson, "");
                    String path = JsonUtils.getString("path", customSrcJson, "");
                    String thumb = JsonUtils.getString("thumb", customSrcJson, "");
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md12\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\" style=\"display: flex;\">\r\n";
                    addForm += "                        <textarea style=\"display:none\" name=\"" + smallHumpFieldName + "\" class=\"singleFileRemarksValue\"></textarea>\r\n";
                    addForm += "                        <div style=\"display: flex;width: 40%;min-width: 100px;position: relative;\">\r\n";
                    addForm += "                            <input type=\"text\" ori-name=name=\"" + smallHumpFieldName + "\" single-file-remarks-type=\"file\" onclick=\"openUploadFileBox(this,'')\" thumb=\"" + thumb + "\" path=\"" + path + "\" exts=\"" + exts + "\" name=\"" + smallHumpFieldName + "_file\" lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请上传" + fieldRemarks + "\" placeholder=\"请上传" + fieldRemarks + "\" autocomplete=\"off\" class=\"layui-input upload singleFileRemarks_file\">\r\n";
                    addForm += "                        </div>\r\n";
                    addForm += "                        <div style=\"display: flex;flex: 1;min-width: 100px;padding-left: 10px;\">\r\n";
                    addForm += "                            <input type=\"text\" ori-name=\"" + smallHumpFieldName + "\" single-file-remarks-type=\"remarks\" thumb=\"" + thumb + "\" path=\"" + path + "\" exts=\"" + exts + "\" name=\"" + smallHumpFieldName + "_remarks\" lay-verType=\"msg\" lay-verify=\"" + layVerify + "\" lay-reqtext=\"请输入" + fieldRemarks + "描述\" placeholder=\"请输入" + fieldRemarks + "描述\" autocomplete=\"off\" class=\"layui-input singleFileRemarks_remarks\">\r\n";
                    addForm += "                        </div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "multiFileRemarks": {
                    //多附件带描述上传
                    String exts = JsonUtils.getString("exts", customSrcJson, "");
                    String path = JsonUtils.getString("path", customSrcJson, "");
                    String thumb = JsonUtils.getString("thumb", customSrcJson, "");
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md12\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <div thumb=\"" + thumb + "\" path=\"" + path + "\" exts=\"" + exts + "\" name=\"" + smallHumpFieldName + "\" lup-lay-verify=\"" + layVerify + "\" lay-reqtext=\"请上传" + fieldRemarks + "\" placeholder=\"请上传" + fieldRemarks + "\" class=\"multiFileRemarks\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "radio": {
                    //单选按钮
                    //获取数据字典
                    String selectAttr = "";
                    String cssClassRadio = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClassRadio = "class=\"dict-radio layui-input data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDictData = "initDictData('add', function (id, data) {\r\n    });";
                    } else if (dataSrcType.equals("db-table")) {
                        //一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        cssClassRadio = "class=\"db-table-radio layui-input data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDbTableData = "initDbTableData('add', function (id, data) {\r\n    });";                    }
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md6\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <div name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" tips=\"请选择" + fieldRemarks + "\" " + cssClassRadio + " " + selectAttr + " lup-lay-verify=\"" + layVerify + "\" lay-reqtext=\"请选择" + fieldRemarks + "\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
                case "checkbox": {
                    String selectAttr = "";
                    String cssClassCheckbox = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClassCheckbox = "class=\"dict-checkbox layui-input data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDictData = "initDictData('add', function (id, data) {\r\n    });";
                    } else if (dataSrcType.equals("db-table")) {
                        //一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        cssClassCheckbox = "class=\"db-table-checkbox layui-input data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        addInitDbTableData = "initDbTableData('add', function (id, data) {\r\n    });";
                    }
                    //设置添加表单
                    addForm += "            <div class=\"layui-col-md6\">\r\n";
                    addForm += "                <div class=\"layui-form-item\">\r\n";
                    addForm += "                    <label class=\"layui-form-label\">" + mustTag + " " + fieldRemarks + "</label>\r\n";
                    addForm += "                    <div class=\"layui-input-block\">\r\n";
                    addForm += "                        <div name=\"" + smallHumpFieldName + "\" lay-verType=\"msg\" tips=\"请选择" + fieldRemarks + "\" " + cssClassCheckbox + " " + selectAttr + " lup-lay-verify=\"" + layVerify + "\" lay-reqtext=\"请选择" + fieldRemarks + "\"></div>\r\n";
                    addForm += "                    </div>\r\n";
                    addForm += "                </div>\r\n";
                    addForm += "            </div>\r\n";
                    break;
                }
            }
        }
        //判断html路径下是否有custom.js
        String htmlPath = Utils.lcFirst(Utils.lineToHump(tableName.substring(prefix.length())));
        String customJsPath = lupConfig.getDevHtmlWorkspace() + "pages/" + prefix + "/" + htmlPath + "/custom.js";
        String addStr = Objects.requireNonNull(Utils.readResourcesFile("tpl/add.html.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$addForm}", addForm)
                .replace("{$addInitTreeParentData}", addInitTreeParentData)
                .replace("{$addInitDbTableData}", addInitDbTableData)
                .replace("{$addInitDictData}", addInitDictData)
                .replace("{$initEditor}", initEditor)
                .replace("{$wangEditorJs}", wangEditorJs);
        if (new File(customJsPath).exists()) {
            addStr = addStr.replace("{$custom.js}", "<script src=\"./custom.js\"></script>");

        } else {
            addStr = addStr.replace("{$custom.js}", "");
        }
        return addStr;
    }
}
