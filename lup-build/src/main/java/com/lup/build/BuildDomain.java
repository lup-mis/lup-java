/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class BuildDomain {
    public String getDomain(JSONObject sysTable, JSONArray sysFieldList) {
        String generationTime = DateUtils.getNowDateTime();
        String fieldDomainStr = "";
        String tableName = sysTable.getString("tableName");
        String prefix = tableName.split("_")[0];
        String domain = Utils.ucFirst(Utils.lineToHump(tableName));
        Set<String> domainImport = new HashSet<>();
        for (int i = 0; i < sysFieldList.size(); i++) {
            JSONObject item = sysFieldList.getJSONObject(i);
            String fieldName = item.getString("fieldName");
            String smallHumpFieldName = Utils.lineToHump(fieldName);
            String fieldRemarks = item.getString("fieldRemarks");
            String fieldType = item.getString("fieldType");
            String dataType = item.getString("dataType");
            String dataCheckType = item.getString("dataCheckType");
            String dataSrcType = item.getString("dataSrcType");
            String customSrc = item.getString("customSrc");
            String inputType = item.getString("inputType");
            int must = item.getIntValue("must");
            String fieldLen = item.getString("fieldLen");
            JSONObject customSrcJson = new JSONObject(true);
            if (!customSrc.equals("")) {
                customSrcJson = JSONObject.parseObject(customSrc);
            }
            if (fieldName.equals("id") || fieldType.equals("varchar") || fieldType.equals("char") || fieldType.equals("text") || fieldType.equals("longtext") || fieldType.equals("date") || fieldType.equals("datetime") || fieldType.equals("time")) {
                dataType = "String";
            } else if (fieldType.equals("int") || fieldType.equals("tinyint")) {
                dataType = "Integer";
            } else if (fieldType.equals("bigint")) {
                dataType = "Long";
            } else if (fieldType.equals("decimal")) {
                dataType = "Double";
            }
            if (dataCheckType.equals("ymdhis")) {
                domainImport.add("import java.util.Date;");
                dataType = "Date";
            }
            if (dataCheckType.equals("ymd") || dataCheckType.equals("his")) {
                dataType = "String";
            }
            //判断是否id关联，如果是id关联则修改为string类型
            if (dataSrcType.equals("db-table") || dataSrcType.equals("tree-parent")) {
                if (dataType.equals("Long") || dataType.equals("Integer")) {
                    if (JsonUtils.getString("key", customSrcJson, "").equals("id")) {
                        dataType = "String";
                    }
                }
            }
            //组装domain
            fieldDomainStr += "\r\n    //" + fieldRemarks + "\r\n";
            if (!dataCheckType.equals("password") && must == 1 && !fieldName.equals("create_time") && !fieldName.equals("id")) {
                if (dataType.equals("Long") || dataType.equals("Integer") || dataType.equals("Double") || dataType.equals("Date")) {
                    domainImport.add("import javax.validation.constraints.NotNull;");
                    fieldDomainStr += "    @NotNull(message = \"" + fieldRemarks + "不能为空\")\r\n";
                } else {
                    domainImport.add("import javax.validation.constraints.NotEmpty;");

                    if (!fieldLen.equals("0") && !fieldLen.equals("")) {
                        domainImport.add("import javax.validation.constraints.Size;");
                        fieldDomainStr += "    @Size(max = " + fieldLen + ",message = \"" + fieldRemarks + "不能超过{max}个字符\")\n";
                    }
                    fieldDomainStr += "    @NotEmpty(message = \"" + fieldRemarks + "不能为空\")\r\n";
                }

            }
            //
            fieldDomainStr += "    private " + dataType + " " + smallHumpFieldName + ";\r\n";
            boolean isSingle = inputType.equals("select") || inputType.equals("xmSelectNoPage") || inputType.equals("xmSelect") || inputType.equals("radio");
            if (dataSrcType.equals("db-table") || dataSrcType.equals("tree-parent")) {
                //对应的是数据表
                String _table = JsonUtils.getString("table", customSrcJson, "");
                String _prefix = _table.split("_")[0];
                //判断是一对一还是一对多
                if (isSingle) {
                    fieldDomainStr += "    private " + Utils.ucFirst(Utils.lineToHump(_table)) + " " + smallHumpFieldName + "Res;\r\n";
                    domainImport.add("import java.util.List;");
                } else {
                    fieldDomainStr += "    private List<" + Utils.ucFirst(Utils.lineToHump(_table)) + "> " + smallHumpFieldName + "Res;\r\n";
                }
                //如果两个实体不在同一个空间则引入
                if (!_prefix.equals(prefix)) {
                    domainImport.add("import com.lup.system." + _prefix + ".domain." + Utils.ucFirst(Utils.lineToHump(_table)) + ";");
                }
            } else if (dataSrcType.equals("dict")) {
                //对应的是数据表
                String _table = "sys_dict_data";
                if (isSingle) {
                    fieldDomainStr += "    private " + Utils.ucFirst(Utils.lineToHump(_table)) + " " + smallHumpFieldName + "Res;\r\n";
                } else {
                    domainImport.add("import java.util.List;");
                    fieldDomainStr += "    private List<" + Utils.ucFirst(Utils.lineToHump(_table)) + "> " + smallHumpFieldName + "Res;\r\n";
                }
                if (!prefix.equals("sys")) {
                    domainImport.add("import com.lup.system.sys.domain.SysDictData;");
                }

            }
        }

        return Objects.requireNonNull(Utils.readResourcesFile("tpl/domain.java.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$field}", fieldDomainStr)
                .replace("{$prefix}", prefix)
                .replace("{$domain}", domain)
                .replace("{$domainImport}", StringUtils.join(domainImport, "\r\n"));
    }
}
