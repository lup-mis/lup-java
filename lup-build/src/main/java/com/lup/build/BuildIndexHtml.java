/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.LupConfig;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Utils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.*;
@AllArgsConstructor
public class BuildIndexHtml {
    private final LupConfig lupConfig;
    public String getIndexHtml(JSONObject sysTable, JSONArray sysFieldList) {
        String indexSearchForm = "";
        String indexInitDictData = "";
        String indexInitDbTableData = "";
        String indexInitTreeParentData = "";
        String generationTime = DateUtils.getNowDateTime();
        String tableName = sysTable.getString("tableName");
        String prefix = tableName.split("_")[0];
        String listSelectType = sysTable.getString("listSelectType");
        List<String> colsLists = new ArrayList<>();
        colsLists.add("                { type: \"" + listSelectType + "\" }");
        colsLists.add("                { field: \"systemSerialNumber\", title: \"序号\", width: 60, sort: true, align: \"center\", type: \"numbers\" }");
        for (int i = 0; i < sysFieldList.size(); i++) {
            JSONObject item = sysFieldList.getJSONObject(i);
            String fieldName = item.getString("fieldName");
            String smallHumpFieldName = Utils.lineToHump(fieldName);
            String fieldRemarks = item.getString("fieldRemarks");
            String fieldType = item.getString("fieldType");
            String dataCheckType = item.getString("dataCheckType");
            String dataSrcType = item.getString("dataSrcType");
            String customSrc = item.getString("customSrc");
            String inputType = item.getString("inputType");
            String dictCode = item.getString("dictCode");
            int colsWidth = item.getIntValue("colsWidth");
            int listShow = item.getIntValue("listShow");
            int listEdit = item.getIntValue("listEdit");
            JSONObject customSrcJson = new JSONObject(true);
            if (!customSrc.equals("")) {
                customSrcJson = JSONObject.parseObject(customSrc);
            }
            //组装index 的cols
            String searchCondition = item.getString("searchCondition");
            String selectAttr = "";
            String cssClass = "";
            //处理表单内容 检索，添加，编辑 start
            switch (inputType) {
                case "input":
                    //处理input表单类型
                    String searchDateClass = "";
                    String dateAttr = "range=\"\"";
                    String spStartEnd = "";
                    String readonly = "";
                    //组装index搜索表单
                    if (!searchCondition.equals("none")) {
                        String dateBetweenInput = "";
                        switch (dataCheckType) {
                            case "ymd":
                                searchDateClass = "date";
                                spStartEnd = "";
                                break;
                            case "ymdhis":
                            case "time":
                                searchDateClass = "datetime";
                                dateAttr = "range=\"~\"";
                                spStartEnd = "";
                                break;
                            case "timeMs" :
                                searchDateClass = "datetime";
                                dateAttr = "range=\"~\"";
                                spStartEnd = "000~999";
                                break;
                            case "his" :
                                searchDateClass = "time";
                                spStartEnd = "";
                                break;
                        }
                        if (searchCondition.equals("between")) {
                            readonly = "readonly";
                            dateBetweenInput += "                     <input type=\"hidden\" name=\"" + smallHumpFieldName + "BetweenStart\">\r\n";
                            dateBetweenInput += "                     <input type=\"hidden\" name=\"" + smallHumpFieldName + "BetweenEnd\">\r\n";
                        }
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                     <input name=\"" + smallHumpFieldName + "\" placeholder=\"" + fieldRemarks + "\" " + readonly + " data-check-type=\"" + dataCheckType + "\" " + dateAttr + " sp-start-end=\"" + spStartEnd + "\" class=\"layui-input " + searchDateClass + "\" autocomplete=\"off\">\r\n";
                        indexSearchForm += dateBetweenInput;
                        indexSearchForm += "                </div>\r\n";
                    }
                    break;
                case "select":
                    //处理select表单类型
                    selectAttr = "";
                    cssClass = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        //原生下拉数据字典
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClass = "class=\"dict-select\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDictData = "initDictData('search', function (id, data) {\r\n        });";
                        }
                    } else if (dataSrcType.equals("db-table")) {
                        //原生下拉一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        cssClass = "class=\"db-table-select data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!customSrc.equals("")) {

                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        if (!searchCondition.equals("none")) {
                            indexInitDbTableData = "initDbTableData('search', function (id, data) {\r\n        });";
                        }
                    }
                    //组装搜索表单
                    if (!searchCondition.equals("none")) {
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                    <select id=\""+smallHumpFieldName+"\" name=\"" + smallHumpFieldName + "\" tips=\"" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " lay-search lay-filter=\"select-filter\">\r\n";
                        indexSearchForm += "                        <option value=\"\">" + fieldRemarks + "</option>\r\n";
                        indexSearchForm += "                    </select>\r\n";
                        indexSearchForm += "                </div>\r\n";
                    }
                    break;

                case "xmSelectNoPage":
                case "xmMultiSelectNoPage":
                    //下拉选择无分页（下拉单选和下拉多选）
                    selectAttr = "";
                    cssClass = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        //数据字典
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClass = "class=\"dict-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!searchCondition.equals("'none'")) {
                            indexInitDictData = "initDictData('search', function (id, data) {\r\n        });";
                        }
                    } else if (dataSrcType.equals("db-table")) {
                        //数据库
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }

                        cssClass = "class=\"db-table-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDbTableData = "initDbTableData('search', function (id, data) {\r\n        });";
                        }
                    } else if (dataSrcType.equals("tree-parent")) {
                        //树表parent字段
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String pid = JsonUtils.getString("pid", customSrcJson, "0");

                            selectAttr = "api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\" pid=\"" + pid + "\"";
                        }
                        cssClass = "class=\"tree-parent-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!searchCondition.equals("none")) {
                            indexInitTreeParentData = "initTreeParentData('search');";
                        }
                    }
                    if (!searchCondition.equals("none")) {
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                    <div id=\"" + smallHumpFieldName + "\" name=\"" + smallHumpFieldName + "\" tips=\"" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " style=\"min-width:200px;max-width: 600px;\"></div>\r\n";
                        indexSearchForm += "                </div>\r\n";
                    }
                    break;
                case "xmSelect":
                case "xmMultiSelect":
                    //下拉有分页(下拉有分页只能来源数据库)
                    if (dataSrcType.equals("db-table")) {
                        String search = "";
                        selectAttr = "api=\"接口未定义\"";
                        cssClass = "";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            search = JsonUtils.getString("search", customSrcJson, "");

                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        cssClass = "class=\"db-table-" + inputType + " data-check-type-" + dataCheckType + " field-type-" + fieldType + "\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDbTableData = "initDbTableData('search', function (id, data) {\r\n        });";
                        }
                        if (!searchCondition.equals("none")) {
                            indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                            indexSearchForm += "                    <div id=\"" + smallHumpFieldName + "\" search=\"" + search + "\" name=\"" + smallHumpFieldName + "\" tips=\"" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " style=\"min-width:200px\"></div>\r\n";
                            indexSearchForm += "                </div>\r\n";
                        }
                    }
                    break;
                case "textEditor":
                case "textarea":
                    if (!searchCondition.equals("none")) {
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                    <input class=\"layui-input\" name=\"" + smallHumpFieldName + "\" placeholder=\"" + fieldRemarks + "\" autocomplete=\"off\">\r\n";
                        indexSearchForm += "                </div>\r\n";
                    }
                    break;
                case "singleFile":
                    //单附件信息
                    break;
                case "multiFile":
                    //多附件上传
                    break;
                case "singleFileRemarks":
                    //单附件上传带描述
                    break;
                case "multiFileRemarks":
                    //多附件带描述上传
                    break;
                case "radio":
                    //单选按钮
                    //获取数据字典
                    selectAttr = "";
                    cssClass = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        cssClass = "class=\"dict-select\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDictData = "initDictData('search', function (id, data) {\r\n        });";
                        }
                    } else if (dataSrcType.equals("db-table")) {
                        //一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        cssClass = "class=\"db-table-select\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDbTableData = "initDbTableData('search', function (id, data) {\r\n        });";
                        }
                    }
                    if (!searchCondition.equals("none")) {
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                    <select name=\"" + smallHumpFieldName + "\" tips=\"" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " lay-search lay-filter=\"select-filter\">\r\n";
                        indexSearchForm += "                        <option value=\"\">" + fieldRemarks + "</option>\r\n";
                        indexSearchForm += "                    </select>\r\n";
                        indexSearchForm += "                </div>\r\n";
                    }

                    break;

                case "checkbox":
                    cssClass = "";
                    selectAttr = "";
                    if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                        cssClass = "class=\"dict-xmMultiSelectNoPage\"";
                        selectAttr = "dict-code=\"" + dictCode + "\"";
                        if (!searchCondition.equals("none")) {
                            indexInitDictData = "initDictData('search', function (id, data) {\r\n        });";
                        }
                    } else if (dataSrcType.equals("db-table")) {
                        //一对一数据库
                        selectAttr = "api=\"接口未定义\"";
                        cssClass = "class=\"db-table-xmMultiSelectNoPage\"";
                        if (!customSrc.equals("")) {
                            String api = JsonUtils.getString("api", customSrcJson, "");
                            String key = JsonUtils.getString("key", customSrcJson, "");
                            String value = JsonUtils.getString("value", customSrcJson, "");
                            String cut = JsonUtils.getString("cut", customSrcJson, "");
                            String isParentRoot = JsonUtils.getString("isParentRoot", customSrcJson, "");
                            String parentId = JsonUtils.getString("parentId", customSrcJson, "");
                            String sonId = JsonUtils.getString("sonId", customSrcJson, "");
                            selectAttr = "is-parent-root=\""+isParentRoot+"\" parent-id=\""+parentId+"\" son-id=\""+sonId+"\" api=\"" + api + "\"  key=\"" + key + "\" val=\"" + value + "\" cut=\"" + cut + "\"";
                        }
                        if (!searchCondition.equals("none")) {
                            indexInitDbTableData = "initDbTableData('search', function (id, data) {\r\n        });";
                        }
                    }
                    if (!searchCondition.equals("none")) {
                        indexSearchForm += "                <div class=\"layui-inline\">\r\n";
                        indexSearchForm += "                    <div id=\"" + smallHumpFieldName + "\" name=\"" + smallHumpFieldName + "\" tips=\"" + fieldRemarks + "\" " + cssClass + " " + selectAttr + " style=\"min-width:200px\"></div>\r\n";
                        indexSearchForm += "                </div>\r\n";
                    }
                    break;
            }
            //处理表单内容 检索，添加，编辑 end
            //组装index字段列表start
            String widthStr = "";
            String listEditStr = "";
            if (colsWidth > 0) widthStr = "width: " + colsWidth + ", ";
            if (listEdit == 1) listEditStr = "edit: true, ";
            if (listShow == 1 || fieldName.equals("id")) {
                String templet = "";
                if (dataSrcType.equals("dict")) {
                    templet = ", templet: function(d) { return listDictData(\"" + dictCode + "\", d." + smallHumpFieldName + ") }";
                }
                if (dataSrcType.equals("db-table") || dataSrcType.equals("tree-parent")) {
                    String _customSrcValue = JsonUtils.getString("value", customSrcJson, "");
                    String _customSrcCut = JsonUtils.getString("cut", customSrcJson, "");
                    templet = ", templet: function(d) { return listResData(\"" + _customSrcValue + "\", \"" + _customSrcCut + "\", d." + smallHumpFieldName + "Res) }";
                }
                String hidden = "";
                if (fieldName.equals("id")) hidden = " hide: true, width: 100,";
                if (inputType.equals("singleFile") || inputType.equals("multiFile") || inputType.equals("singleFileRemarks") || inputType.equals("multiFileRemarks")) {
                    templet = ", templet: function(d) { return listUpload(d.id+\"_" + smallHumpFieldName + "\", d." + smallHumpFieldName + ", \"\") }";
                }
                if (dataCheckType.equals("time") || dataCheckType.equals("timeMs")) {
                    templet = ", templet: function(d) { return timeStampFmt(d." + smallHumpFieldName + ") }";
                }
                if (dataCheckType.equals("ymd") || dataCheckType.equals("ymdhis")) {
                    templet = ", templet: function(d) { return dateFormat(d." + smallHumpFieldName + ") }";
                }
                colsLists.add("                { field: \"" + smallHumpFieldName + "\", title: \"" + fieldRemarks + "\"," + hidden + " " + widthStr + "sort: true, " + listEditStr + "align: \"left\"" + templet + " }");
            }
            //组装index字段列表end
        }
        if (!indexSearchForm.equals("")) {
            indexSearchForm += "                <div class=\"layui-inline\">\r\n";
            indexSearchForm += "                    <button type=\"submit\" class=\"layui-btn\" lay-submit lay-filter=\"data-search-btn\">搜 索</button>\r\n";
            indexSearchForm += "                </div>\r\n";
        }
        //判断html路径下是否有custom.js
        String htmlPath = Utils.lcFirst(Utils.lineToHump(tableName.substring(prefix.length())));
        String customJsPath = lupConfig.getDevHtmlWorkspace() + "pages/" + prefix + "/" + htmlPath + "/custom.js";


        String indexStr = Objects.requireNonNull(Utils.readResourcesFile("tpl/index.html.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$indexSearchForm}", indexSearchForm)
                .replace("{$indexInitDictData}", indexInitDictData)
                .replace("{$indexInitTreeParentData}", indexInitTreeParentData)
                .replace("{$indexInitDbTableData}", indexInitDbTableData)
                .replace("{$cols}", StringUtils.join(colsLists, ", \r\n"));
        if (new File(customJsPath).exists()) {
            indexStr = indexStr.replace("{$custom.js}", "<script src=\"./custom.js\"></script>");
            if (Utils.readFile(customJsPath).contains("toolbarTapCustom")) {
                indexStr = indexStr.replace("{$toolbarTapCustom(data,pars,event)}", "toolbarTapCustom(data,pars,event);");
            } else {
                indexStr = indexStr.replace("{$toolbarTapCustom(data,pars,event)}", "");
            }
        } else {
            indexStr = indexStr.replace("{$custom.js}", "");
            indexStr = indexStr.replace("{$toolbarTapCustom(data,pars,event)}", "");
        }
        return indexStr;
    }
}
