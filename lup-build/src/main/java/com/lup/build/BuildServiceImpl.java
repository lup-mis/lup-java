/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.build;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class BuildServiceImpl {
    public String getServiceImpl(JSONObject sysTable, JSONArray sysFieldList) {
        String tableName = sysTable.getString("tableName");
        String prefix = tableName.split("_")[0];
        String domain = Utils.ucFirst(Utils.lineToHump(tableName));
        String domainName = Utils.lcFirst(Utils.lineToHump(tableName));
        String generationTime = DateUtils.getNowDateTime();
        Set<String> serviceImplImport = new HashSet<>();
        Set<String> serviceImplFinal = new HashSet<>();
        Integer isRedis = sysTable.getInteger("isRedis");
        Map<String, JSONObject> mappingMap = new HashMap<>();
        String redisUpdate = "";
        if (isRedis == 1) {
            serviceImplImport.add("import com.lup.system.component.SetCacheComponent;");
            serviceImplFinal.add("    private final SetCacheComponent setCacheComponent;");
            redisUpdate = "setCacheComponent.setCache(\"" + tableName + "\");";
        }
        for (int i = 0; i < sysFieldList.size(); i++) {
            JSONObject item = sysFieldList.getJSONObject(i);
            String fieldName = item.getString("fieldName");
            String smallHumpFieldName = Utils.lineToHump(fieldName);
            String fieldType = item.getString("fieldType");
            String dataSrcType = item.getString("dataSrcType");
            String customSrc = item.getString("customSrc");
            String inputType = item.getString("inputType");
            String dictCode = item.getString("dictCode");
            JSONObject customSrcJson = new JSONObject(true);
            if (!customSrc.equals("")) {
                customSrcJson = JSONObject.parseObject(customSrc);
            }
            //构建serviceImpl 查询 start
            //构建import
            if (dataSrcType.equals("db-table") || dataSrcType.equals("tree-parent")) {
                if (!customSrc.equals("")) {
                    String _table = JsonUtils.getString("table", customSrcJson, "");
                    String _key = JsonUtils.getString("key", customSrcJson, "");

                    String _domain = Utils.ucFirst(Utils.lineToHump(_table));
                    String _prefix = _table.split("_")[0];
                    serviceImplImport.add("import com.lup.system." + _prefix + ".domain." + _domain + ";");
                    if (!_domain.equals("SysCatalog")) {
                        serviceImplImport.add("import com.lup.system." + _prefix + ".mapper." + _domain + "Mapper;");
                    }
                    serviceImplImport.add("import com.lup.system.component.DataComponent;");
                    serviceImplImport.add("import com.lup.system.component.GetCacheComponent;");
                    if (dataSrcType.equals("db-table")) {
                        serviceImplImport.add("import java.util.Set;");
                        serviceImplImport.add("import java.util.HashSet;");
                        serviceImplImport.add("import org.apache.commons.lang3.StringUtils;");
                        serviceImplFinal.add("    private final " + _domain + "Mapper " + Utils.lcFirst(_domain) + "Mapper;");
                        JSONObject _json = new JSONObject();
                        _json.put("keysArray", smallHumpFieldName + "sArray");
                        _json.put("table", _table);
                        _json.put("key", _key);
                        _json.put("dictCode", dictCode);
                        _json.put("inputType", inputType);
                        _json.put("fieldType", fieldType);
                        _json.put("dataSrcType", dataSrcType);
                        _json.put("customSrcJson", customSrcJson);
                        mappingMap.put(smallHumpFieldName, _json);
                    } else {
                        serviceImplImport.add("import com.lup.system.component.DataComponent;");
                        serviceImplImport.add("import com.lup.system.component.GetCacheComponent;");
                        serviceImplFinal.add("    private final GetCacheComponent getCacheComponent;");
                        JSONObject _json = new JSONObject();
                        _json.put("keysArray", smallHumpFieldName + "sArray");
                        _json.put("table", _table);
                        _json.put("key", _key);
                        _json.put("dictCode", dictCode);
                        _json.put("inputType", inputType);
                        _json.put("fieldType", fieldType);
                        _json.put("dataSrcType", dataSrcType);
                        _json.put("customSrcJson", customSrcJson);
                        mappingMap.put(smallHumpFieldName, _json);
                    }
                }
            } else if (dataSrcType.equals("dict") && !dictCode.equals("")) {
                serviceImplImport.add("import com.lup.system.component.DataComponent;");
                serviceImplImport.add("import com.lup.system.component.GetCacheComponent;");
                serviceImplImport.add("import com.lup.system.sys.domain.SysDictData;");
                serviceImplFinal.add("    private final GetCacheComponent getCacheComponent;");
                JSONObject _json = new JSONObject();
                _json.put("keysArray", smallHumpFieldName + "sArray");
                _json.put("table", "sys_dict_data");
                _json.put("key", "");
                _json.put("dictCode", dictCode);
                _json.put("inputType", inputType);
                _json.put("fieldType", fieldType);
                _json.put("dataSrcType", dataSrcType);
                _json.put("customSrcJson", customSrcJson);
                mappingMap.put(smallHumpFieldName, _json);
            }
            //构建serviceImpl 查询 end
        }

        String foreachMapping = "";
        String foreachMappingQuery = "";
        if (mappingMap.size() > 0) {
            //开始组装businessCode
            foreachMapping += "        //取出数据库对应关系的字段的值（这里用逗号隔开）\r\n";
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                if (_json.getString("dataSrcType").equals("db-table")) {
                    foreachMapping += "        Set<String> " + _json.getString("keysArray") + " = new HashSet<>();\r\n";
                }
            }
            foreachMapping += "        for (" + domain + " item : list) {\r\n";
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                if (_json.getString("dataSrcType").equals("db-table")) {
                    foreachMapping += "            " + _json.getString("keysArray") + ".add(item.get" + Utils.ucFirst(entry.getKey()) + "());\r\n";
                }
//                String s = "key====>" + entry.getKey() + ",value===>" + entry.getValue();
//                System.out.println(s);
            }

            foreachMapping += "        }\r\n";
            //开始获取数据
            //这里多加一层循环，为了保证数据字典和目录分类缓存再上面显示
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                if (_json.getString("dataSrcType").equals("dict") && !_json.getString("dictCode").equals("")) {
                    //获取数据字典
                    if (!foreachMapping.contains("getCacheComponent.getCache(\"sys_dict_data\");")) {
                        foreachMapping += "        //从redis获取数据字典数据;\r\n";
                        foreachMapping += "        JSONArray sysDictData = getCacheComponent.getCache(\"sys_dict_data\");\r\n";
                        serviceImplImport.add("import com.alibaba.fastjson.JSONArray;");
                    }
                    if (!foreachMappingQuery.contains("getCacheComponent.getCache(\"sys_dict_data\");")) {
                        foreachMappingQuery += "        //从redis获取数据字典数据;\r\n";
                        foreachMappingQuery += "        JSONArray sysDictData = getCacheComponent.getCache(\"sys_dict_data\");\r\n";
                        serviceImplImport.add("import com.alibaba.fastjson.JSONArray;");
                    }
                } else if (_json.getString("dataSrcType").equals("tree-parent")) {
                    //获取数据字典
                    if (!foreachMapping.contains("getCacheComponent.getCache(\"sys_catalog\");")) {
                        foreachMapping += "        //从redis获取目录分类;\r\n";
                        foreachMapping += "        JSONArray sysCatalog = getCacheComponent.getCache(\"sys_catalog\");\r\n";
                        serviceImplImport.add("import com.alibaba.fastjson.JSONArray;");
                    }
                    if (!foreachMappingQuery.contains("getCacheComponent.getCache(\"sys_catalog\");")) {
                        foreachMappingQuery += "        //从redis获取目录分类;\r\n";
                        foreachMappingQuery += "        JSONArray sysCatalog = getCacheComponent.getCache(\"sys_catalog\");\r\n";
                        serviceImplImport.add("import com.alibaba.fastjson.JSONArray;");
                    }
                }
            }
            //获取数据表数据
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _table = _json.getString("table");
                JSONObject _customSrcJson = _json.getJSONObject("customSrcJson");
                String keyId = _customSrcJson.getString("key");
                if (_json.getString("dataSrcType").equals("db-table")) {
                    foreachMapping += "        //将数据表" + _table + "中根据id字段取出符合符合条件的数据\r\n";
                    foreachMapping += "        List<" + Utils.ucFirst(Utils.lineToHump(_table)) + "> " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey()) + "List = " + Utils.lcFirst(Utils.lineToHump(_table)) + "Mapper.listsIn(\""+Utils.humpToLine(keyId)+"\",!" + entry.getKey() + "sArray.isEmpty() ? StringUtils.join(" + entry.getKey() + "sArray, \",\") : \"\");\r\n";
                    foreachMappingQuery += "        //将数据表" + _table + "中根据id字段取出符合符合条件的数据\r\n";
                    foreachMappingQuery += "        List<" + Utils.ucFirst(Utils.lineToHump(_table)) + "> " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey())  + "List = " + Utils.lcFirst(Utils.lineToHump(_table)) + "Mapper.listsIn(\""+Utils.humpToLine(keyId)+"\",!" + Utils.lineToHump(tableName) + ".get" + Utils.ucFirst(entry.getKey()) + "().isEmpty() ? StringUtils.join(" + Utils.lineToHump(tableName) + ".get" + Utils.ucFirst(entry.getKey()) + "(), \",\") : \"\");\r\n";

                }
            }
            //组装查询query关系start
            //为了让字典在前面这里多一次for
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _inputType = _json.getString("inputType");
                String _dictCode = _json.getString("dictCode");
                String _fieldType = _json.getString("fieldType");
                String _key = _json.getString("key");
                String _toString = "";
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }
                if (_json.getString("dataSrcType").equals("dict")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayDictData(" + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", \"" + _dictCode + "\", sysDictData), SysDictData.class));\r\n";
                    } else {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(" + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", \"" + _dictCode + "\", sysDictData), SysDictData.class));\r\n";
                    }
                }
            }
            //tree-parent
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _inputType = _json.getString("inputType");
                String _fieldType = _json.getString("fieldType");
                String _toString = "";
                String _key = _json.getString("key");
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }
                if (_json.getString("dataSrcType").equals("tree-parent")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayCatalog(" + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", sysCatalog), SysCatalog.class));\r\n";
                    } else {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonCatalog(" + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", sysCatalog), SysCatalog.class));\r\n";
                    }
                }
            }
            //db-table
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _table = _json.getString("table");
                String _inputType = _json.getString("inputType");
                String _fieldType = _json.getString("fieldType");

                JSONObject _customSrcJson = _json.getJSONObject("customSrcJson");
                String keyId = _customSrcJson.getString("key");

                String _toString = "";
                String _key = _json.getString("key");
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }

                if (_json.getString("dataSrcType").equals("db-table")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayDbField(\""+keyId+"\", " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey())  + "List), " + Utils.ucFirst(Utils.lineToHump(_table)) + ".class));\r\n";
                    } else {
                        foreachMappingQuery += "        " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField(\""+keyId+"\", " + Utils.lcFirst(Utils.lineToHump(tableName)) + ".get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey())  + "List), " + Utils.ucFirst(Utils.lineToHump(_table)) + ".class));\r\n";
                    }
                }
            }
            //组装查询query关系end

            //开始组装for循环数据list start
            foreachMapping += "        for (" + domain + " item : list) {\r\n";
            //为了让字典在前面这里多一次for
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _inputType = _json.getString("inputType");
                String _dictCode = _json.getString("dictCode");
                String _fieldType = _json.getString("fieldType");
                String _key = _json.getString("key");
                String _toString = "";
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }
                if (_json.getString("dataSrcType").equals("dict")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayDictData(item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", \"" + _dictCode + "\", sysDictData), SysDictData.class));\r\n";
                    } else {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", \"" + _dictCode + "\", sysDictData), SysDictData.class));\r\n";
                    }
                }
            }
            //tree-parent
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _inputType = _json.getString("inputType");
                String _fieldType = _json.getString("fieldType");
                String _toString = "";
                String _key = _json.getString("key");
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }
                if (_json.getString("dataSrcType").equals("tree-parent")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayCatalog(item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", sysCatalog), SysCatalog.class));\r\n";
                    } else {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonCatalog(item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", sysCatalog), SysCatalog.class));\r\n";
                    }
                }
            }
            //db-table
            for (Map.Entry<String, JSONObject> entry : mappingMap.entrySet()) {
                JSONObject _json = entry.getValue();
                String _table = _json.getString("table");
                String _inputType = _json.getString("inputType");
                String _fieldType = _json.getString("fieldType");
                String _toString = "";
                String _key = _json.getString("key");
                if (!_key.equals("id")) {
                    if (_fieldType.equals("int") || _fieldType.equals("bigint") || _fieldType.equals("tinyint") || _fieldType.equals("decimal")) {
                        _toString = ".toString()";
                    }
                }

                JSONObject _customSrcJson = _json.getJSONObject("customSrcJson");

                String keyId = _customSrcJson.getString("key");

                if (_json.getString("dataSrcType").equals("db-table")) {
                    if (_inputType.equals("xmMultiSelectNoPage") || _inputType.equals("xmMultiSelect") || _inputType.equals("checkbox")) {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonArray2Domain(DataComponent.getArrayDbField(\""+keyId+"\", item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey())  +"List), " + Utils.ucFirst(Utils.lineToHump(_table)) + ".class));\r\n";
                    } else {
                        foreachMapping += "            item.set" + Utils.ucFirst(entry.getKey()) + "Res(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField(\""+keyId+"\", item.get" + Utils.ucFirst(entry.getKey()) + "()" + _toString + ", " + Utils.lcFirst(Utils.lineToHump(_table)) + Utils.ucFirst(entry.getKey()) + "List), " + Utils.ucFirst(Utils.lineToHump(_table)) + ".class));\r\n";
                    }
                }
            }
            foreachMapping += "        }\r\n";
            //组装for循环数据 list end

        }
        return Objects.requireNonNull(Utils.readResourcesFile("tpl/serviceImpl.java.template"))
                .replace("{$generationTime}", generationTime)
                .replace("{$domain}", domain)
                .replace("{$domainName}", domainName)
                .replace("{$prefix}", prefix)
                .replace("{$final}", StringUtils.join(serviceImplFinal, "\r\n"))
                .replace("{$serviceImplImport}", StringUtils.join(serviceImplImport, "\r\n"))
                .replace("{$domainName}", domainName)
                .replace("{$businessCode}", foreachMapping)
                .replace("{$businessCodeQuery}", foreachMappingQuery)
                .replace("{$redisUpdate} ", redisUpdate);
    }
}
