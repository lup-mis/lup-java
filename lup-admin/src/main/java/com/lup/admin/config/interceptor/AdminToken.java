package com.lup.admin.config.interceptor;

import lombok.Data;

@Data
public class AdminToken {
    private String accessUid;
    private String accessToken;
    private String accountName;
    private String terminal;

}
