/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.config;

import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.core.config.LupConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    private final TokenInterceptor tokenInterceptor;
    private final LupConfig lupConfig;
    private static final List<String> EXCLUDE_PATH = Arrays.asList("/", "index.html", "/lup-mis/**", "/static/**", "/css/**", "/js/**", "/uploads/**", "/upload/**");

    public WebMvcConfig(TokenInterceptor tokenInterceptor, LupConfig lupConfig) {
        this.tokenInterceptor = tokenInterceptor;
        this.lupConfig = lupConfig;
    }


    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(lupConfig.getAdminStaticPath(), "file:" + lupConfig.getUploadPath());
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        WebMvcConfigurer.super.addViewControllers(registry);
        //主页
        registry.addViewController("/lup-mis/").setViewName("forward:/lup-mis/index.html");
        registry.addViewController("/lup-mis").setViewName("forward:index.html");
    }

    /**
     * 注册拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //配置token 以及权限校验拦截器
//        String[] split = lupConfig.getTokenInterceptorPath().split(",");
//"/sys/**", "/lup/**", "/api/**", "/admin/**"
        registry.addInterceptor(tokenInterceptor).addPathPatterns(lupConfig.getTokenInterceptorPath().split(",")).excludePathPatterns(EXCLUDE_PATH);
    }
}

