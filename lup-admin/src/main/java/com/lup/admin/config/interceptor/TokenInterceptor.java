/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-22 14:32:37;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.config.interceptor;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.NoCheckToken;
import com.lup.core.config.LupConfig;
import com.lup.core.utils.*;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.component.RoleComponent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.concurrent.TimeUnit;


@Slf4j
@Component
@AllArgsConstructor
public class TokenInterceptor implements HandlerInterceptor {

    private static final ThreadLocal<AdminToken> adminTokenThreadLocal = new ThreadLocal<>();

    private final RedisUtils redisUtils;
    private final LupConfig lupConfig;
    private final RoleComponent roleComponent;
    private final GetCacheComponent getCacheComponent;




    /**
     * 在请求到达Controller控制器之前 通过拦截器执行一段代码
     * 如果方法返回true,继续执行后续操作
     * 如果返回false，执行中断请求处理，请求不会发送到Controller
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        AdminToken adminToken = new AdminToken();

//        log.info("拦截器1 在控制器执行之前执行");
        if (response.getStatus() == 500) {
            this.outPutCode(response, 500, "服务器内部错误！");
            return false;
        } else if (response.getStatus() == 404) {
            this.outPutCode(response, 404, "资源不存在！");
            return false;
        }

        Method method;
        String accessToken;
        String accessUid;
        String terminal = "web";
        try {
            //判断是否接口黑名单
//            String ipAddress = IpUtils.getIPAddress();
//            JSONObject sysConfig = getCacheComponent.config();
//            String apiBlacklist = JsonUtils.getString("apiBlacklist",sysConfig,"");
//            if (!Utils.empty(apiBlacklist)){
//                String[] split = apiBlacklist.split(",");
//                if (Utils.arrayContains(split,ipAddress)){
//                    log.info("非法访问拦截,ip:{}",ipAddress);
//                    this.outPutCode(response, -200, "非法访问！！！");
//                    return false;
//                }
//            }
            assert handler instanceof HandlerMethod;
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            method = handlerMethod.getMethod();
            //检查是否需要验证token，如果不需要校验则直接return
            if (method.isAnnotationPresent(NoCheckToken.class)) {
                return true;
            }
            accessUid = request.getHeader("accessuid");
            accessToken = request.getHeader("accesstoken");
            terminal = request.getHeader("terminal");
            if (Utils.empty(terminal)) terminal = "web";
            //TokenInterceptor.terminal = terminal;
        } catch (Exception e) {
            this.outPutCode(response, 404, "资源不存在.");
            return false;
        }

        //判断token是否为空
        if (accessToken == null || accessToken.length() != 32) {
            this.outPutCode(response, 401, "您还未登录，请登录!");
            return false;
        }
        if (terminal.equals("app")){
            if (!this.redisUtils.hasKey("token:admin:app:" + accessUid)) {
                this.outPutCode(response, 401, "登录信息失效，请重新登录.");
                return false;
            }
        }else {
            if (!this.redisUtils.hasKey("token:admin:web:" + accessUid)) {
                this.outPutCode(response, 401, "登录信息失效，请重新登录.");
                return false;
            }
        }
        String accountJsonStr;
        //从redis获取用户信息
        if (terminal.equals("app")) {
            accountJsonStr = this.redisUtils.get("token:admin:app:" + accessUid);
        } else {
            accountJsonStr = this.redisUtils.get("token:admin:web:" + accessUid);
        }

        if (StringUtils.isEmpty(accountJsonStr)) {
            this.outPutCode(response, 401, "登录信息失效，请重新登录.");
            return false;
        }
        JSONObject accountJson = JSON.parseObject(accountJsonStr);
        if (StringUtils.isEmpty(JsonUtils.getString("accountName", accountJson, null))) {
            this.outPutCode(response, 401, "登录信息失效，请重新登录!");
            return false;
        }
        if (!JsonUtils.getString("token", accountJson, "").equals(accessToken)) {
            this.outPutCode(response, 401, "登录信息失效，请重新登录。");
            return false;
        }

        //判断ip是否变化，如果ip变化则需要重新登录
//        String ipAddress = IpUtils.getIPAddress();
//        if (!ipAddress.equals(JsonUtils.getString("ip", accountJson, null))) {
//            this.outPutCode(response, 401, "登录信息失效，请重新登录~");
//            return false;
//        }
//        TokenInterceptor.accessUid = accessUid;
//        TokenInterceptor.getToken().getAccessToken() = accessToken;
//        TokenInterceptor.accountName = JsonUtils.getString("accountName", accountJson, null);
        adminToken.setTerminal(terminal);
        adminToken.setAccessUid(accessUid);
        adminToken.setAccessToken(accessToken);
        adminToken.setAccountName(JsonUtils.getString("accountName", accountJson, ""));
        adminTokenThreadLocal.set(adminToken);
        //判断token是否需要续期
        int expireTime = JsonUtils.getInt("expireTime", accountJson, 0);
        int now = DateUtils.getTimeStampSecond();
        if ((expireTime - now) <= (lupConfig.getTokenExpireTime() - lupConfig.getTokenNeedCreateTime())) {
            //续期token
            accountJson.put("expireTime", now + lupConfig.getTokenExpireTime());
            if (terminal.equals("app")) {
                redisUtils.add("token:admin:app:" + accessUid, JSON.toJSONString(accountJson), 3600 * 24 * 15L, TimeUnit.SECONDS);
            } else {
                redisUtils.add("token:admin:web:" + accessUid, JSON.toJSONString(accountJson), lupConfig.getTokenExpireTime(), TimeUnit.SECONDS);
            }
        }
        //权限校验
        try {
            //先判断redis是否有数据，如果没有数据则初始化cache
            //检查是否需要验证权限，如果不需要校验则直接return
            CheckRole checkRoleAnnotation = method.getAnnotation(CheckRole.class);
            //如果没有注解则不验证权限
            if (checkRoleAnnotation == null) return true;
            //code为空也不验证权限
            String code = checkRoleAnnotation.code();

            if (code.equals("")) return true;
            JSONObject roleActionIdsArray = roleComponent.getRolePageAction(accessUid);

            @SuppressWarnings("unchecked")
            Set<String> pageCodeArray = (Set<String>) roleActionIdsArray.get("pageCode");

            @SuppressWarnings("unchecked")
            Set<String> actionCodeArray = (Set<String>) roleActionIdsArray.get("actionCode");

            if (!pageCodeArray.contains(code) && !actionCodeArray.contains(code)) {
                this.outPutCode(response, 403, "暂无权限！");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 控制器之后，跳转前
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
//        log.info("拦截器1 在控制器执行之后执行");
        adminTokenThreadLocal.remove();
    }
    public static AdminToken getToken () {
        return adminTokenThreadLocal.get();
    }
    /**
     * 跳转之后执行
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
//        log.info("拦截器1 最后执行");
    }

    private void outPutCode(HttpServletResponse response, int code, String msg) {

        response.setCharacterEncoding("UTF-8");
        if (code != -200) {
            response.setStatus(code);
        }
        response.setContentType("application/json; charset=utf-8");
        JSONObject res = new JSONObject();
        res.put("code", code);
        res.put("msg", msg);
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (IOException e) {
            try {
                response.sendError(500);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } finally {
            assert out != null;
            out.write(res.toString());
            out.flush();
            out.close();
        }

    }
}

