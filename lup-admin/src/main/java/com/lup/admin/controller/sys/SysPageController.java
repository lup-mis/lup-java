/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-07-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysPage;
import com.lup.system.sys.service.SysPageService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sys/page")
@AllArgsConstructor
public class SysPageController extends LupBaseController {
    private final SysPageService service;

    @CheckRole(code = "sys:page:lists")
    @RequestMapping(value = "/lists", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public JSONObject list(@RequestBody(required = false) JSONObject data) {
        startPage(data);
        return getDataListPage(service.lists(data));
    }

    @OperationLog("删除页面接口")
    @CheckRole(code = "sys:page:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject idsJson) {
        service.del(idsJson);
        return success("删除成功");
    }

    @OperationLog("添加页面接口")
    @CheckRole(code = "sys:page:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody(required = false) SysPage sysPage) {
        try {
            return service.add(sysPage) == 0 ? fail("添加失败") : success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑页面接口")
    @CheckRole(code = "sys:page:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody(required = false) SysPage sysPage) {
        try {
            return service.edit(sysPage) == 0 ? fail("修改失败") : success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:page:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }

    @CheckRole(code = "sys:page:initBtn")
    @RequestMapping(value = "/initBtn")
    public JSONObject initBtn(@RequestBody(required = false) JSONObject dataJson) {
        return success(service.initBtn(dataJson));
    }
}