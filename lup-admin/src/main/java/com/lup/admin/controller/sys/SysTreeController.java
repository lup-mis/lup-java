/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-07-11 15:12:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysTree;
import com.lup.system.sys.service.SysTreeService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/tree")
@AllArgsConstructor
public class SysTreeController extends LupBaseController {
    private final SysTreeService service;

    @CheckRole(code = "sys:tree:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) {
        return getDataList(service.lists(dataJson));
    }

    @OperationLog("删除菜单")
    @CheckRole(code = "sys:tree:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject idsJson) {
        try {
            service.del(idsJson);
            return success("删除成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("添加菜单")
    @CheckRole(code = "sys:tree:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody(required = false) SysTree sysTree) {
        try {
            service.add(sysTree);
            return success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑菜单")
    @CheckRole(code = "sys:tree:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody(required = false) SysTree sysTree) {
        try {
            service.edit(sysTree);
            return success("编辑成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:tree:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }
}