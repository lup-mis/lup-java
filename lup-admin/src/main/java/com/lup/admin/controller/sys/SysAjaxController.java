/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-17 14:39:17;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.core.utils.Utils;
import com.lup.system.sys.mapper.MySqlMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/sys/ajax")
@AllArgsConstructor
public class SysAjaxController {
    private final MySqlMapper mySqlMapper;

    @OperationLog("双击修改数据")
    //这里不进行权限校验
    @CheckRole()
    @RequestMapping(value = "/tableEdit")
    public JSONObject list(@RequestBody JSONObject dataJson) {

        String id = JsonUtils.getString("id", dataJson);
        String tableName = JsonUtils.getString("tableName", dataJson);
        String fieldName = JsonUtils.getString("fieldName", dataJson);
        if (StringUtils.isEmpty(id)) return ResultUtils.fail("参数不正确");
        if (StringUtils.isEmpty(tableName)) return ResultUtils.fail("参数不正确");
        if (StringUtils.isEmpty(fieldName)) return ResultUtils.fail("参数不正确");

        //把驼峰转换下划线格式
        fieldName = Utils.humpToLine(fieldName);
        dataJson.put("fieldName", fieldName);
        try {
            if (this.mySqlMapper.tableEdit(dataJson) == 0) {
                return ResultUtils.fail("数据未更改");
            }
            return ResultUtils.success("修改成功");
        } catch (Exception e) {
            return ResultUtils.fail("修改失败");
        }
    }
}
