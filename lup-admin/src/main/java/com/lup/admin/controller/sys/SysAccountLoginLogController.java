/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:29:18;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysAccountLoginLog;
import com.lup.system.sys.service.SysAccountLoginLogService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/accountLoginLog")
@AllArgsConstructor
public class SysAccountLoginLogController extends LupBaseController {
    final SysAccountLoginLogService service;

    @CheckRole(code = "sys:accountLoginLog:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject data) {
        startPage(data);
        return getDataListPage(service.lists(data));
    }

    @OperationLog("删除登录日志")
    @CheckRole(code = "sys:accountLoginLog:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject idsJson) {
        service.del(idsJson);
        return success("删除成功");
    }

    @OperationLog("添加登录日志")
    @CheckRole(code = "sys:accountLoginLog:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody SysAccountLoginLog sysAccountLoginLog) {
        try {
            return service.add(sysAccountLoginLog) == 0 ? fail("添加失败") : success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑登录日志")
    @CheckRole(code = "sys:accountLoginLog:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody SysAccountLoginLog sysAccountLoginLog) {
        try {
            return service.edit(sysAccountLoginLog) == 0 ? fail("修改失败") : success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:accountLoginLog:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }
} 
