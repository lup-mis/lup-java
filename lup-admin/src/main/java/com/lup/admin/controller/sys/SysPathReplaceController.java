/**
 * [LupMisNotAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-09 09:08:07;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;


import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.system.sys.service.SysPathReplaceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/sys")
@CrossOrigin
@AllArgsConstructor
public class SysPathReplaceController {
    private final SysPathReplaceService service;

    @OperationLog("附件路径替换")
    @CheckRole(code = "sys:pathReplace:doReplace")
    @RequestMapping(value = "/pathReplace/doReplace")
    public JSONObject replacePath(@RequestBody(required = false) JSONObject dataJson) {
        return this.service.doReplace(dataJson);
    }

    @CheckRole(code = "sys:pathReplace:lists")
    @RequestMapping(value = "/pathReplace/lists")
    public JSONObject replacePathList() {
        return this.service.lists();
    }
}