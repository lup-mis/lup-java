/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-07-12 11:12:11;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.system.sys.domain.SysOrg;
import com.lup.system.sys.service.SysOrgService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping(value = "/sys/org")
@AllArgsConstructor
public class SysOrgController {
    private final SysOrgService service;

    @CheckRole(code = "sys:org:lists")
    @RequestMapping(value = "/lists")
    public List<SysOrg> list() {
        return service.lists();
    }

    @OperationLog("添加组织机构")
    @CheckRole(code = "sys:org:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@RequestBody(required = false) JSONObject dataJson) {
        return service.add(dataJson);
    }


    @CheckRole(code = "sys:org:lists")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return service.query(id);
    }

    @OperationLog("删除组织机构")
    @CheckRole(code = "sys:org:edit")
    @RequestMapping(value = "/del/{id}")
    public JSONObject del(@PathVariable(value = "id") String id) {
        return service.del(id);
    }

    @OperationLog("编辑组织机构")
    @CheckRole(code = "sys:org:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@RequestBody(required = false) JSONObject dataJson) {
        return service.edit(dataJson);
    }

    @CheckRole(code = "sys:org:edit")
    //上移 下移
    @RequestMapping(value = "/move")
    public JSONObject move(@RequestBody(required = false) JSONObject dataJson) {
        return service.move(dataJson);
    }

}