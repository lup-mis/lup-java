/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-09 09:08:07;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysConfig;
import com.lup.system.sys.service.SysConfigService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/sys/config")
@CrossOrigin
@AllArgsConstructor
public class SysConfigController extends LupBaseController {
    private final SysConfigService service;

    @CheckRole(code = "sys:config:query")
    @RequestMapping(value = "/query")
    public JSONObject query() {
        return getDataOne(service.query());
    }

    @OperationLog("编辑系统配置信息")
    @CheckRole(code = "sys:config:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@RequestBody(required = false) SysConfig sysConfig) {
        try {
            service.edit(sysConfig);
            return success();
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

}