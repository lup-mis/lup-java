/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:55:31;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysFiles;
import com.lup.system.sys.service.SysFilesService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/files")
@AllArgsConstructor
public class SysFilesController extends LupBaseController {
    private final SysFilesService service;

    @CheckRole(code = "sys:files:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) {
        startPage(dataJson);
        return getDataListPage(service.lists(dataJson));
    }

    @OperationLog("删除附件")
    @CheckRole(code = "sys:files:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject idsJson) {
        service.del(idsJson);
        return success();
    }

    @OperationLog("添加附件")
    @CheckRole(code = "sys:files:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody SysFiles sysFiles) {
        service.add(sysFiles);
        return success();
    }

    @OperationLog("编辑附件")
    @CheckRole(code = "sys:files:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody SysFiles sysFiles) {
        service.edit(sysFiles);
        return success();
    }

    @CheckRole(code = "sys:files:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }
} 
