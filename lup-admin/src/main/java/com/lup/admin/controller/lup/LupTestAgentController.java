/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-02 14:49:15; 
 * 版权所有 2020-2021 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
 
package com.lup.admin.controller.lup; 
 
import com.lup.core.config.exception.LupException; 
import com.lup.core.controller.LupBaseController; 
import com.alibaba.fastjson.JSONObject; 
import com.lup.admin.config.annotation.CheckRole; 
import com.lup.admin.config.annotation.OperationLog; 
import com.lup.system.lup.domain.LupTestAgent; 
import com.lup.system.lup.service.LupTestAgentService; 
import org.springframework.validation.annotation.Validated; 
import org.springframework.web.bind.annotation.PathVariable; 
import org.springframework.web.bind.annotation.RequestBody; 
import org.springframework.web.bind.annotation.RequestMapping; 
import org.springframework.web.bind.annotation.RestController; 
import lombok.AllArgsConstructor; 
 
@RestController 
@RequestMapping(value = "/lup/testAgent") 
@AllArgsConstructor 
public class LupTestAgentController extends LupBaseController { 
    private final LupTestAgentService service; 
 
    @CheckRole(code = "lup:testAgent:lists") 
    @RequestMapping(value = "/lists") 
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) { 
        startPage(dataJson); 
        return getDataListPage(service.lists(dataJson)); 
    } 
 
    @OperationLog("删除代理商（DEMO）") 
    @CheckRole(code = "lup:testAgent:del") 
    @RequestMapping(value = "/del") 
    public JSONObject del(@RequestBody(required = false) JSONObject ids) { 
        service.del(ids); 
        return success("删除成功"); 
    } 
 
    @OperationLog("添加代理商（DEMO）") 
    @CheckRole(code = "lup:testAgent:add") 
    @RequestMapping(value = "/add") 
    public JSONObject add(@Validated @RequestBody LupTestAgent lupTestAgent) { 
        try { 
            service.add(lupTestAgent); 
            return success("添加成功"); 
        } catch (LupException e) { 
            return fail(e.getMsg()); 
        } 
    } 
 
    @OperationLog("编辑代理商（DEMO）") 
    @CheckRole(code = "lup:testAgent:edit") 
    @RequestMapping(value = "/edit") 
    public JSONObject edit(@Validated @RequestBody LupTestAgent lupTestAgent) { 
        try { 
            service.edit(lupTestAgent); 
            return success("修改成功"); 
        } catch (LupException e) { 
            return fail(e.getMsg()); 
        } 
    } 
 
    @CheckRole(code = "lup:testAgent:edit") 
    @RequestMapping(value = "/query/{id}") 
    public JSONObject query(@PathVariable(value = "id") String id) { 
        return getDataOne(service.query(id)); 
    } 
} 
