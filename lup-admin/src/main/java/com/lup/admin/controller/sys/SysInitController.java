/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-12 14:56:12;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.lup.admin.config.annotation.NoCheckToken;
import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.service.SysInitService;
import com.lup.system.sys.service.SysRolePageActionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/init")
@AllArgsConstructor
public class SysInitController extends LupBaseController {
    private final SysInitService service;
    private final SysRolePageActionService sysRolePageActionService;

    //登录页面不校验token
    @NoCheckToken
    @RequestMapping(value = "/loginInfo")
    public JSONObject loginInfo() {
        JSONObject loginInfo = this.service.loginInfo();
        //这里需要判断一下，如果是未登录状态把下面两个参数给remove掉，防止任何人都可以看到敏感信息
        JSONPath.remove(loginInfo, "$.data.safeIps");
        JSONPath.remove(loginInfo, "$.data.secretKey");
        return loginInfo;
    }

    //初始化菜单,这里不校验权限，只校验token
    @RequestMapping(value = "/tree")
    public JSONObject tree() {
        return success(this.service.tree(TokenInterceptor.getToken().getAccessUid(), TokenInterceptor.getToken().getAccessToken(), TokenInterceptor.getToken().getAccountName()));
    }

    //欢迎页（控制台首页）
    @RequestMapping(value = "/welcome")
    public JSONObject welcome() {
        return success(this.service.welcome());
    }

    //获取访问页面的操作按钮(from redis)
    @RequestMapping(value = "/getPageAction/{code}")
    public JSONObject getPageAction(@PathVariable(value = "code") String code) {
        return this.sysRolePageActionService.lists(code, TokenInterceptor.getToken().getAccessUid());
    }

    //更新redis缓存
    @RequestMapping(value = "/cache")
    public JSONObject cache() {
        this.service.cache();
        return success("更新成功");
    }
}
