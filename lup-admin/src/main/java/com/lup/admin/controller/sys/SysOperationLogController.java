/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:35:31;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.system.sys.domain.SysOperationLog;
import com.lup.system.sys.service.SysOperationLogService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/operationLog")
@AllArgsConstructor
public class SysOperationLogController {
    final SysOperationLogService service;

    @CheckRole(code = "sys:operationLog:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) {
        return service.lists(dataJson);
    }

    @OperationLog("删除操作日志")
    @CheckRole(code = "sys:operationLog:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject idsJson) {
        return service.del(idsJson);
    }

    @OperationLog("添加操作日志")
    @CheckRole(code = "sys:operationLog:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody SysOperationLog sysOperationLog) {
        return service.add(sysOperationLog);
    }

    @OperationLog("编辑操作日志")
    @CheckRole(code = "sys:operationLog:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody SysOperationLog sysOperationLog) {
        return service.edit(sysOperationLog);
    }

    @CheckRole(code = "sys:operationLog:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return service.query(id);
    }
} 
