package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.system.sys.service.SysUploadService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-18 12:34:11;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
@RestController
@RequestMapping(value = "/sys")
@AllArgsConstructor
public class SysUploadController {
    private final SysUploadService sysUploadService;

    @RequestMapping(value = "/upload/uploadFile")
    @ResponseBody
    public JSONObject upload(@RequestParam("file") MultipartFile file,
                             @RequestParam(value = "path", required = false) String path,
                             @RequestParam(value = "thumb", required = false) String thumb,
                             @RequestParam(value = "remarks", required = false, defaultValue = "后台上传") String remarks) {
        // 文件上传
        return sysUploadService.upload(file, path, thumb, remarks, TokenInterceptor.getToken().getAccountName(), "admin");
    }
}