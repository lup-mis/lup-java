/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-23 23:59:22;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysAccount;
import com.lup.system.sys.service.SysAccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/sys/account")
@AllArgsConstructor
public class SysAccountController extends LupBaseController {
    private final SysAccountService service;

    @CheckRole(code = "sys:account:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject data) {
        startPage(data);
        return getDataListPage(service.lists(data));
    }

    @OperationLog("删除账号")
    @CheckRole(code = "sys:account:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject ids) {
        service.del(ids);
        return success("删除成功");
    }

    @OperationLog("添加账号")
    @CheckRole(code = "sys:account:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@RequestBody(required = false) SysAccount sysAccount) {
        try {
            return service.add(sysAccount) == 0 ? fail("添加失败") : success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑账号")
    @CheckRole(code = "sys:account:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@RequestBody(required = false) SysAccount sysAccount) {
        try {
            return service.edit(sysAccount) == 0 ? fail("修改失败") : success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    //只有拥有修改的权限才拥有单条查询的权限
    @CheckRole(code = "sys:account:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }

    @OperationLog("修改密码")
    @RequestMapping(value = "/updPwd")
    public JSONObject updPwd(@RequestBody(required = false) JSONObject data) {
        data.put("accountId", TokenInterceptor.getToken().getAccessUid());
        try {
            return service.updPwd(data) == 0 ? fail("修改失败") : success("密码修改成功，请牢记您的新密码");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

}