/**
 * [LupMisNotAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-08-01 16:35:22;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.admin.controller.sys;

import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.system.sys.domain.SysCities;
import com.lup.system.sys.domain.SysProvinces;
import com.lup.system.sys.mapper.SysCitiesMapper;
import com.lup.system.sys.service.SysProvincesService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.AllArgsConstructor;

import java.util.List;

@RestController
@RequestMapping(value = "/sys/provinces")
@AllArgsConstructor
public class SysProvincesController extends LupBaseController {
    private final SysProvincesService service;
    final SysCitiesMapper sysCitiesMapper;
    @CheckRole(code = "sys:provinces:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) {
        dataJson.put("sortField","systemSerialNumber");
        dataJson.put("sortOrder","asc");
        startPage(dataJson);
        return getDataListPage(service.lists(dataJson));
    }

    @OperationLog("删除省份")
    @CheckRole(code = "sys:provinces:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject ids) {
        String _ids = ids.getString("ids");
        //判断是否还有下级城市
        String[] strArray = _ids.split(",");
        for (String id : strArray) {
            System.out.println(id);
            JSONObject jsonObject = new JSONObject();
            SysProvinces query = service.query(id);
            jsonObject.put("provinceCode",query.getProvinceCode());
            List<SysCities> lists = sysCitiesMapper.lists(jsonObject);
            if(lists.size()>0){
                return fail("删除失败，请先删除下级城市");
            }
        }
        service.del(ids);
        return success("删除成功");
    }

    @OperationLog("添加省份")
    @CheckRole(code = "sys:provinces:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody SysProvinces sysProvinces) {
        try {
            service.add(sysProvinces);
            return success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑省份")
    @CheckRole(code = "sys:provinces:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody SysProvinces sysProvinces) {
        try {
            service.edit(sysProvinces);
            return success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:provinces:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }
}
