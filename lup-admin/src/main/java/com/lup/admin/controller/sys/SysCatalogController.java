/**
 * [LupMisAllowedSync-lock]
 * 生成时间 2021-07-11 15:12:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysCatalog;
import com.lup.system.sys.service.SysCatalogService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/sys/catalog")
@AllArgsConstructor
public class SysCatalogController extends LupBaseController {
    private final SysCatalogService service;

    @CheckRole(code = "sys:catalog:lists")
    @RequestMapping(value = "/lists")
    public JSONObject list(@RequestBody(required = false) JSONObject dataJson) {
        return getDataList(service.lists(dataJson));
    }


    @RequestMapping(value = "/listByPid/{pid}")
    public JSONObject list(@PathVariable(value = "pid") String pid) {
        return getDataList(service.lists(pid));
    }

    @OperationLog("删除目录")
    @CheckRole(code = "sys:catalog:del")
    @RequestMapping(value = "/del/{id}")
    public JSONObject del(@PathVariable(value = "id") String id) {
        try {
            service.del(id);
            return success();
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("添加目录")
    @CheckRole(code = "sys:catalog:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody(required = false) JSONObject dataJson) {
        return success(service.add(dataJson));
    }

    @OperationLog("编辑目录")
    @CheckRole(code = "sys:catalog:edit")
    @RequestMapping(value = "/editName")
    public JSONObject editName(@Validated @RequestBody(required = false) SysCatalog sysCatalog) {
        try {
            return service.editName(sysCatalog) == 0 ? fail("修改失败") : success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑目录")
    @CheckRole(code = "sys:catalog:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody(required = false) SysCatalog sysCatalog) {
        try {
            service.edit(sysCatalog);
            return success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:catalog:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }


    //上移 下移
    @RequestMapping(value = "/move")
    public JSONObject move(@RequestBody(required = false) JSONObject dataJson) {
        service.move(dataJson);
        return success();
    }

}