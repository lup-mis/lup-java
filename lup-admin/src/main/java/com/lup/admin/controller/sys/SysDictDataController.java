/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-23 34:56:13;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.CheckRole;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.core.config.exception.LupException;
import com.lup.core.controller.LupBaseController;
import com.lup.system.sys.domain.SysDictData;
import com.lup.system.sys.service.SysDictDataService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/sys/dictData")
@AllArgsConstructor
public class SysDictDataController extends LupBaseController {
    private final SysDictDataService service;

    @CheckRole(code = "sys:dictData:listsByDictCode")
    @RequestMapping(value = "/listsByDictCode/{dictCode}")
    public JSONObject listsByDictCode(@PathVariable(value = "dictCode") String dictCode) {
        return getDataList(service.listsByDictCode(dictCode));
    }

    @OperationLog("删除字典数据")
    @CheckRole(code = "sys:dictData:del")
    @RequestMapping(value = "/del")
    public JSONObject del(@RequestBody(required = false) JSONObject ids) {
        try {
            service.del(ids);
            return success("删除成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("添加字典数据")
    @CheckRole(code = "sys:dictData:add")
    @RequestMapping(value = "/add")
    public JSONObject add(@Validated @RequestBody(required = false) SysDictData sysDictData) {
        try {
            service.add(sysDictData);
            return success("添加成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @OperationLog("编辑字典数据")
    @CheckRole(code = "sys:dictData:edit")
    @RequestMapping(value = "/edit")
    public JSONObject edit(@Validated @RequestBody(required = false) SysDictData sysDictData) {
        try {
            service.edit(sysDictData);
            return success("修改成功");
        } catch (LupException e) {
            return fail(e.getMsg());
        }
    }

    @CheckRole(code = "sys:dictData:edit")
    @RequestMapping(value = "/query/{id}")
    public JSONObject query(@PathVariable(value = "id") String id) {
        return getDataOne(service.query(id));
    }

    @CheckRole()
    @RequestMapping(value = "/getAll")
    public JSONObject lists() {
        return getDataList(service.getAll());
    }


}