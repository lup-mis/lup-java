package com.lup.admin.controller.sys;


import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.NoCheckToken;
import com.lup.core.utils.Db;
import com.lup.core.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
@AllArgsConstructor
public class TestController {
    private final Db db;

    @RequestMapping(value = "/test")
    @NoCheckToken
    public String test() {
        JSONObject pars = new JSONObject(true);
        pars.put("id", "20652912451518464");
        List<Map<String, Object>> maps = db.fetchAll("select id,user_name from lup_test_user where id=:id", pars);
        System.out.println(maps);

        return Utils.getSign("1234561112122122", "123");
    }

}
