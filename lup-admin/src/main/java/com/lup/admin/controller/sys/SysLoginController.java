/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-12 14:56:12;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.admin.controller.sys;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.NoCheckToken;
import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.system.sys.service.SysLoginService;
import com.lup.system.sys.service.SysVerifyCodeService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
@RequestMapping(value = "/sys")
@AllArgsConstructor
public class SysLoginController {
    private final SysVerifyCodeService sysVerifyCodeService;
    private final SysLoginService service;

    //登录验证码
    @NoCheckToken
    @RequestMapping(value = "/login/getVerifyCode")
    public JSONObject getVerifyCode(@RequestBody(required = false) JSONObject dataJson) {
        String verifyCodeToken = null;
        if (dataJson != null) {
            verifyCodeToken = dataJson.getOrDefault("verifyCodeToken", null).toString();
        }
        return this.sysVerifyCodeService.getVerifyCode(verifyCodeToken);
    }

    //登录验证
    @NoCheckToken
    @RequestMapping(value = "/login/login")
    public JSONObject login(@RequestBody(required = false) JSONObject dataJson) {
        return this.service.login(dataJson);
    }

    //登录验证
    @NoCheckToken
    @RequestMapping(value = "/login/logout")
    public JSONObject logout() {
        return this.service.logout(TokenInterceptor.getToken().getAccessUid(),TokenInterceptor.getToken().getTerminal());
    }
}
