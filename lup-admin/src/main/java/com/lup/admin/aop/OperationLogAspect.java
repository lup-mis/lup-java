package com.lup.admin.aop;

import com.alibaba.fastjson.JSONObject;
import com.lup.admin.config.annotation.OperationLog;
import com.lup.admin.config.interceptor.TokenInterceptor;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.IpUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.sys.domain.SysOperationLog;
import com.lup.system.sys.mapper.SysOperationLogMapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public record OperationLogAspect(SysOperationLogMapper sysOperationLogMapper, Snowflake snowflake) {

    @Pointcut("@annotation(com.lup.admin.config.annotation.OperationLog)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint point) {
        long beginTime = System.currentTimeMillis();
        Object result = null;
        try {
            result = point.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        long time = System.currentTimeMillis() - beginTime;
        saveLog(point, time);
        return result;
    }

    private void saveLog(ProceedingJoinPoint point, long time) {
        SysOperationLog sysOperationLog = new SysOperationLog();
        sysOperationLog.setId(this.snowflake.nextId());
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        OperationLog annotation = method.getAnnotation(OperationLog.class);
        //注解上的描述 用户操作
        if (annotation != null) {
            sysOperationLog.setOperation(annotation.value());
        }else{
            sysOperationLog.setOperation("controller缺少注解");
        }
        //请求方法名
        String className = point.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysOperationLog.setMethod(className + "." + methodName + "()");
        //请求参数
        Object[] args = point.getArgs();
        LocalVariableTableParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();
        String[] parameterNames = discoverer.getParameterNames(method);
        if (args != null && parameterNames != null) {
            StringBuilder pars = new StringBuilder();
            for (int i = 0; i < args.length; i++) {
//                if (parameterNames[i] instanceof Object) {
                if (parameterNames[i] != null) {
                    pars.append(" ").append(parameterNames[i]).append(":").append(JSONObject.toJSON(args[i]).toString());
                } else {
                    pars.append(" ").append(parameterNames[i]).append(":").append(args[i]);
                }
            }
            sysOperationLog.setPars(pars.toString());
        }
        sysOperationLog.setIp(IpUtils.getIPAddress());
        sysOperationLog.setAccountName(TokenInterceptor.getToken().getAccountName());
        sysOperationLog.setTime((int) time);
        sysOperationLog.setCreateTime(DateUtils.getNowDate());
        // 保存系统日志
        sysOperationLogMapper.add(sysOperationLog);
    }
}