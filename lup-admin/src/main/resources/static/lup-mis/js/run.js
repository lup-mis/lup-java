/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

var debug = false;
var projectPath = "/lup-mis/";
var staticVersion = localStorage.getItem("systemVersion");
if (debug) {
    staticVersion = "debug_" + new Date().getTime();
} else {
    if (localStorage.getItem("systemVersion")) {
        staticVersion = localStorage.getItem("systemVersion");
    } else {
        staticVersion = '1.0.0';
    }
}
localStorage.setItem("systemVersion", staticVersion);
var $run = {
    initRes: function(path) {
        document.write('<script type="text/javascript" src="' + path + 'js/config.js?v=' + new Date().getTime() + '"><\/script>');
        document.write('<script type="text/javascript" src="' + path + 'lib/layui-v2.6.3/layui.js?v=' + staticVersion + '"><\/script>');
        document.write('<script type="text/javascript" src="' + path + 'js/lay-config.js?v=' + staticVersion + '"><\/script>');
        document.write('<script type="text/javascript" src="' + path + 'js/lib.js?v=' + staticVersion + '"><\/script>');
        document.write('<script type="text/javascript" src="' + path + 'js/baiduTemplate.js?v=' + staticVersion + '"><\/script>');
        document.write('<script type="text/javascript" src="' + path + 'js/xm-select.js?v=' + staticVersion + '"><\/script>');

    }
}
$run.initRes(projectPath);