/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    //查询数据
    var query = getQueryString("query");
    var id = getQueryString("id");
    layui.jquery("input[name='id']").val(id);
    var url = baseGateway + query + "/" + id;
    request(url, {}, function(ret) {
        var code = ret.code;
        if (code == 200) {
            document.getElementById('form-list').innerHTML = baidu.template('tpl-list', ret);
            getTableList(ret.data.tableName);
            layui.form.render(null, null);
        }

    });
    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
        // layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
    });
    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});

function getTableList(tableName) {
    var url = baseGateway + 'sys/table/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var str = '<option value="">请选择数据表</option>';
            if (tableName == '#') {
                str += '<option value="#" selected>#</option>';
            } else {
                str += '<option value="#" >#</option>';
            }
            for (var i = 0; i < ret.data.rows.length; i++) {
                var selected = "";
                if (ret.data.rows[i].tableName == tableName) {
                    selected = "selected";
                }
                str += '<option value="' + ret.data.rows[i].tableName + '" ' + selected + '>' + ret.data.rows[i].tableName + "【" + ret.data.rows[i].tableRemarks + '】</option>';
            }
            $("select[name='tableName']").html(str);

            layui.form.render('select', null);

        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}