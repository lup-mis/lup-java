/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    //表单赋值
    layui.colorpicker.render({
        elem: '#bgColor-form',
        color: '',
        done: function(color) {
            $('#bgColor').val(color);
        }
    });
    getPageList();
    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
    });

    layui.form.on('select(pageId)', function(data) {
        //获取table名称
        var tableName = $("select[name='pageId']").find('option:selected').attr("table-name");
        getTableList(tableName);
        var code = tableName.replace("_", ":") + ":";
        $("input[name='code']").val(code);
    });

    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});

function getPageList() {
    var url = baseGateway + 'sys/page/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var str = '<option value="">请选择页面接口</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {
                str += '<option value="' + ret.data.rows[i].id + '" table-name="' + ret.data.rows[i].tableName + '">' + ret.data.rows[i].pageName + "【" + ret.data.rows[i].code + '】</option>';
            }
            $("select[name='pageId']").html(str);

            layui.form.render('select', null);

        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}

function getTableList(_tableName) {
    var url = baseGateway + 'sys/table/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var str = '<option value="">请选择数据表</option>';
            str += '<option value="#">#</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {
                var selected = "";
                if (_tableName == ret.data.rows[i].tableName) {
                    selected = "selected";
                }
                str += '<option value="' + ret.data.rows[i].tableName + '" ' + selected + '>' + ret.data.rows[i].tableName + "【" + ret.data.rows[i].tableRemarks + '】</option>';
            }
            $("select[name='tableName']").html(str);
            layui.form.render('select', null);
        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}

function setColor(color) {
    layui.colorpicker.render({
        elem: '#bgColor-form',
        color: color,
        done: function(color) {
            $('#bgColor').val(color);
        }
    });
    $('#bgColor').val(color);
}