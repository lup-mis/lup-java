/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {

    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
    });
    //查询数据
    var query = getQueryString("query");
    var url = baseGateway + query + "/" + getQueryString("id");
    request(url, {}, function(ret) {
        var code = ret.code;
        if (code == 200) {
            if (isEmpty(ret.data.orgRes)) {
                ret.data.orgRes = { name: "" };
            }
            document.getElementById('form-list').innerHTML = baidu.template('tpl-list', ret);
            getRoleList(ret.data.roleIds);

            layui.form.render(null, null);

        }
    });


    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});

function selectOrg() {
    openFrame('选择机构', 'select_org.html', ['400px', '80%']);
}

function doSelect(orgId, orgName) {
    layui.jquery("input[name='orgId']").val(orgId);
    layui.jquery("input[name='orgName']").val(orgName);
}

function getRoleList(roleIds) {
    var data = {
        "page": 1,
        "limit": 10000
    };
    request(baseGateway + 'sys/role/lists', data, function(ret) {
        var code = ret.code;
        if (code == 200 && ret.data.rows.length > 0) {
            var selectData = [];
            for (var i = 0; i < ret.data.rows.length; i++) {
                var item = { name: ret.data.rows[i].roleName, value: ret.data.rows[i].id };
                selectData.push(item);
            }
            var roleSelectBox = xmSelect.render({
                el: '#roleSelectBox',
                toolbar: {
                    show: true,
                },
                autoRow: true,
                filterable: true,
                tips: '请选择角色',
                name: 'roleIds',
                layVerify: 'required',
                layVerType: 'msg',
                on: function(data) {
                    //arr:  当前多选已选中的数据
                    // var arr = data.arr;
                    // var roleIds = [];
                    // for (var i = 0; i < arr.length; i++) {
                    //     roleIds[i] = arr[i].value;
                    // }
                    // console.log(roleIds);
                    // $("input[name='roleId']").val(roleIds);
                },
                data: selectData
            });

            roleSelectBox.setValue(roleIds.split(","));
        }

    });
}