/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    //查询数据
    var query = getQueryString("query");
    var url = baseGateway + query;
    request(url + "/" + getQueryString("id"), {}, function(ret) {
        var code = ret.code;
        if (code == 200) {
            document.getElementById('form-list').innerHTML = baidu.template('tpl-list', ret);
            getPageList(ret.data.pageId);
            getTableList(ret.data.tableName);
            //表单赋值
            layui.colorpicker.render({
                elem: '#bg-color-form',
                color: '',
                done: function(color) {
                    $('#bgColor').val(color);
                }
            });
            layui.form.on('switch(status)', function(data) {
                if (this.checked) {
                    layui.jquery("input[name='status']").val(1);
                } else {
                    layui.jquery("input[name='status']").val(-1);
                }
            });
            layui.form.on('select(pageId)', function(data) {
                //获取table名称
                var tableName = $("select[name='pageId']").find('option:selected').attr("table-name");
                getTableList(tableName);
            });
            layui.form.render(null, null);
        }
    });
    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
    });
    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});

function getPageList(pageId) {
    var url = baseGateway + 'sys/page/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var str = '<option value="">请选择页面接口</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {
                var selected = "";
                if (pageId == ret.data.rows[i].id) {
                    selected = "selected";
                }
                str += '<option value="' + ret.data.rows[i].id + '" ' + selected + ' table-name="' + ret.data.rows[i].tableName + '">' + ret.data.rows[i].pageName + "【" + ret.data.rows[i].code + '】 </option>';
            }
            $("select[name='pageId']").html(str);

            layui.form.render('select', null);

        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}

function getTableList(tableName) {
    var url = baseGateway + 'sys/table/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var str = '<option value="">请选择数据表</option>';
            str += '<option value="#">#</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {
                var selected = "";
                if (ret.data.rows[i].tableName == tableName) {
                    selected = "selected";
                }
                str += '<option value="' + ret.data.rows[i].tableName + '" ' + selected + '>' + ret.data.rows[i].tableName + "【" + ret.data.rows[i].tableRemarks + '】</option>';
            }
            $("select[name='tableName']").html(str);
            layui.form.render('select', null);
        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}


function setColor(color) {
    layui.colorpicker.render({
        elem: '#bgColor-form',
        color: color,
        done: function(color) {
            $('#bgColor').val(color);
        }
    });
    $('#bgColor').val(color);
}