/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
            type: 'checkbox'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 65,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'accountName',
            title: '登录账号',
            width: 100,
            sort: true
        }, {
            field: 'token',
            title: '令牌',
            width: 250,
            sort: false
        }, {
            field: 'expire_time',
            title: '到期时间',
            width: 140,
            sort: true,
            templet: function(d) {
                return timeStampFmt(d.expire_time)
            },
        }, {
            field: 'expire_time',
            title: '状态',
            width: 80,
            sort: false,
            templet: function(d) {
                var data = new Date().getTime();
                if (Math.floor(data / 1000) > d.expire_time) {
                    return "<span style='color:red'>已过期</span>";
                } else {
                    return "<span style='color:green'>未过期</span>";
                }
                // return timeStampFmt(d.expireTime)
            },
        }, {
            field: 'ip',
            title: 'IP',
            width: 100,
            sort: true
        }, {
            field: 'user_agent',
            title: '用户代理',
            minWidth: 100,
            sort: true
        }];
        loadData(baseGateway + queryUrl, cols, toolbarHtml);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data) {
            // console.log(JSON.stringify(data));


        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});