/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    //加载代码雨
    //获取画布对象
    var canvas = document.getElementById("mom");
    //获取画布的上下文
    //getContext() 方法返回一个用于在画布上绘图的环境。
    var context = canvas.getContext("2d");
    //获取浏览器屏幕的宽度和高度
    var W = window.innerWidth;
    var H = window.innerHeight;
    //设置canvas的宽度和高度
    canvas.width = W;
    canvas.height = H;
    //每个文字的字体大小
    var fontSize = 16;
    //计算列
    var colunms = Math.floor(W / fontSize);
    //记录每列文字的y轴坐标
    var drops = [];
    //给每一个文字初始化一个起始点的位置
    //计算每一个文字所谓坐标 存储y轴的坐标
    for (var i = 0; i < colunms; i++) {
        drops[i] = 1;
    }
    //运动的文字JavaScript function(){}
    var str = "0101";
    //4:fillText(str,x,y);原理就是去更改y的坐标位置
    //绘画的函数
    function draw() {
        context.fillStyle = "rgba(0,0,0,0.05)";
        //fillRect() 方法绘制“已填色”的矩形。默认的填充颜色是黑色。
        context.fillRect(0, 0, W, H);
        //给字体设置样式
        context.font = "700 " + fontSize + "px  微软雅黑";
        //给字体添加颜色
        context.fillStyle = "#00cc33"; //可以rgb,hsl, 标准色，十六进制颜色
        //写入画布中
        for (var i = 0; i < colunms; i++) {
            var index = Math.floor(Math.random() * str.length); //设置文字出发时间随机 Math.floor(Math.random()*str.length)让数组里面的文字索引随机出现
            var x = i * fontSize;
            var y = drops[i] * fontSize; //也让y轴方向也向下掉一个文字的距离
            context.fillText(str[index], x, y);
            // //如果要改变时间，肯定就是改变每次他的起点
            if (y >= canvas.height && Math.random() > 0.99) {
                drops[i] = 0;
            }
            drops[i]++; //让数组里面的值每次加一，用于上面的y轴下掉
        }
    }

    function randColor() {
        var r = Math.floor(Math.random() * 256);
        var g = Math.floor(Math.random() * 256);
        var b = Math.floor(Math.random() * 256);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
    // draw();
    var timer;
    var startTimer = function() {
        timer = setInterval(function() {
            draw();
        }, 20)
    };
    var clearTimer = function() {
        clearInterval(timer);
    }
    startTimer();
    var windowH = $(window).height();
    if (windowH <= 300) {
        windowH = 300;
    }
    $("#right").css("height", (windowH - 125) + "px");
    $("#left").css("height", (windowH - 125) + "px");
    $(window).resize(function() { //当浏览器大小变化时
        var windowH = $(window).height();
        if (windowH <= 300) {
            windowH = 300;
        }
        $("#right").css("height", (windowH - 125) + "px");
        $("#left").css("height", (windowH - 125) + "px");
    });
    getTableList();
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
                type: 'radio',
                width: 40
            }, {
                field: 'id',
                title: 'ID',
                width: 65,
                hide: true
            }, {
                field: 'systemSerialNumber',
                title: 'No',
                width: 45,
                align: 'center',
                type: 'numbers'
            }, {
                field: 'fieldName',
                title: '字段名称',
                minWidth: 80,
                align: 'left'
            }, {
                field: 'fieldRemarks',
                title: '字段说明',
                minWidth: 80
            },
            {
                field: 'fieldType',
                title: '类型',
                width: 65,
            }, {
                field: 'fieldLen',
                title: '长度',
                width: 45,
            }, {
                field: 'dataSrcType',
                title: '来源',
                width: 65,
                templet: function name(d) {
                    if (d.dataSrcType == 'dict') {
                        return d.dataSrcType + "【" + d.dictCode + "】";
                    } else {
                        return d.dataSrcType;
                    }
                }
            }, {
                field: 'colsWidth',
                title: '列宽',
                width: 50,
                edit: 'text'
            }, {
                field: 'dataCheckType',
                title: '校验',
                width: 50
            }, {
                field: 'inputType',
                title: '输入',
                width: 50
            }, {
                field: 'searchCondition',
                title: '检索',
                width: 50
            }, {
                field: 'must',
                title: '必填',
                width: 50,
                templet: function(d) {
                    if (d.must == 1) {
                        return '<span style="color:green">是</span>';
                    } else {
                        return '<span style="color:red">否</span>';
                    }
                }
            }, {
                field: 'listEdit',
                title: '双击',
                width: 50,
                templet: function(d) {
                    if (d.listEdit == 1) {
                        return '<span style="color:green">是</span>';
                    } else {
                        return '<span style="color:red">否</span>';
                    }
                }
            }, {
                field: 'sort',
                title: '排序',
                width: 50,
                edit: 'text'
            }, {
                field: 'listShow',
                title: '列表',
                width: 45,
                templet: function(d) {
                    if (d.listShow == 1) {
                        return '<span style="color:green">是</span>';
                    } else {
                        return '<span style="color:red">否</span>';
                    }
                }
            }, {
                field: 'remarks',
                title: '字段备注描述',
                minWidth: 120
            }, {
                field: 'isAllowReplace',
                title: '替换',
                width: 45,
                templet: function(d) {
                    if (d.isAllowReplace == 1) {
                        return '<span style="color:green">允许</span>';
                    } else {
                        return '<span style="color:red">禁止</span>';
                    }
                }
            }
        ];


        selfLoadData(baseGateway + queryUrl, cols, toolbarHtml, function(res) {

        }, (windowH - 128));
        // 监听搜索操作
        doSearch(function(data) {
            var fieldName = data.fieldName;
            var tableName = data.tableName;
            var fieldRemarks = data.field_remarks;
            if (!isEmpty(fieldName) || !isEmpty(tableName) || !isEmpty(fieldRemarks)) {
                $("#mom").hide();
                $("#content").show();
                setTimeout(function() {
                    clearTimer();
                }, 1000);
            } else {
                $("#mom").show();
                $("#content").hide();
                clearTimer();
                startTimer();
            }
        });
        //监听操作事件(添加 删除 编辑)
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data, pars, event) {
            if (event == 'addPars') {
                var jump = pars.jump;
                var gateway = pars.gateway;
                var query = pars.query;
                // console.log(query);
                var tips = pars.tips;
                var width = pars.width;
                var height = pars.height;
                var area = [width, height];

                if (isEmpty(tips)) {
                    tips = "<font style='color:red'>参数未定义</font>";
                }
                if (isEmpty(width)) {
                    width = "100%";
                }
                if (isEmpty(height)) {
                    height = "100%";
                }
                var _tableName = $("select[name='tableName']").val();
                if (_tableName == '') {
                    layer.msg("请选择一张数据表");
                    return false;
                }
                openFrame(tips, jump + '?tableName=' + _tableName + "&gateway=" + gateway + "&query=" + query, area);
            }
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});

function showTable(tableType) {
    if (tableType == '') {
        $(".tableBusiness").show();
        $(".tableSystem").show();
    }
    if (tableType == 'tableBusiness') {
        $(".tableBusiness").show();
        $(".tableSystem").hide();
    }
    if (tableType == 'tableSystem') {
        $(".tableBusiness").hide();
        $(".tableSystem").show();
    }
}

function getTableList() {
    request(baseGateway + "sys/table/lists", { page: 1, limit: 10000 }, function(ret) {
        var _tableName = $("select[name='tableName']").val();
        tableNameRes = ret.data.rows || ret.data;
        var str = '<option value="">数据表</option>';
        var htmlStr = "";
        for (var i = 0; i < tableNameRes.length; i++) {
            var selected = "";
            css = "tableBusiness";
            if (tableNameRes[i].tableType == 1) {
                css = "tableSystem";
            }
            if (_tableName == tableNameRes[i].tableName) {
                selected = "selected";
                css += " selected";
            }
            var no = i + 1;
            if (no <= 9) {
                no = "0" + no;
            }
            htmlStr += '<div style="position: relative;font-size:10px" class="' + css + '" onclick=searchTable(this,"' + tableNameRes[i].tableName + '")>' + tableNameRes[i].tableName + ' <span style="font-size:9px;display:block"> ' + tableNameRes[i].tableRemarks + '</span><span class="tableNo">' + no + '</span></div>';
            str += '<option value="' + tableNameRes[i].tableName + '" ' + selected + '>' + tableNameRes[i].tableRemarks + " / " + tableNameRes[i].tableName + '</option>';
        }
        $("#tableList").html(htmlStr);
        $("select[name='tableName']").html(str);
        form.render('select', null);

    });

}
//监听搜索，重写方法
function doSearch(callback) {
    layui.form.on('submit(data-search-btn)', function(data) {
        //执行搜索重载
        layui.table.reload('data-list', {
            page: false,
            where: data.field
        });
        if (callback) {
            callback(data.field);
        }
        return false;
    });
}

function selfLoadData(url, cols, toolbar, callback, height = "full-110") {
    if (!toolbar) {
        toolbar = false;
    }
    if (toolbar == '<div class="layui-btn-container"></div>') {
        toolbar = false;
    }
    if (height == "full-110") {
        var searchBoxHeight = $(".search-box").height();
        // var height = $(window).height() - 60 - searchBoxHeight;
        var height = $(window).height() - 22 - searchBoxHeight;

        if (height <= 400) {
            height = 400;
        }
    }
    if (isMobile()) {
        height = "";
    }
    var accesstoken = localStorage.getItem("accesstoken");
    layui.table.render({
        elem: '#data-list',
        url: url,
        method: 'post',
        contentType: 'application/json',
        headers: {
            'accessuid': localStorage.getItem("accessuid"),
            'accesstoken': accesstoken
        },
        toolbar: toolbar,
        cellMinWidth: 80,
        loading: false,
        even: true,
        height: height,
        autoSort: false,
        defaultToolbar: false,
        cols: [cols],
        done: function(res, curr, count) {

            layui.jquery(".layui-table-tool").css("background", "#f8f8f8");
        },
        error: function(obj, content) {
            // 
        },
        // skin:'row',
        // size: 'sm',
        page: 1,
        limit: 1000000,
        limits: false,
        response: {
            statusCode: 200 //重新规定成功的状态码为 200，table 组件默认为 0
        },
        parseData: function(res) { //将原始数据解析成 table 组件所规定的数据
            if (callback) {
                callback(res);
            }
            if (res.code == 401) {
                localStorage.setItem("accesstoken", null);
                layer.alert(res.msg, {
                    icon: 1,
                    closeBtn: 0
                }, function(index) {
                    //关闭弹窗
                    layer.close(index);
                    window.location.href = projectPath + "login.html";
                });
            } else if (res.code == 403) {
                layer.msg(res.msg);
                return false;
            } else {
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.data.total || res.data.length || 0, //解析数据长度
                    "data": res.data.rows || res.data || [] //解析数据列表
                };
            }

        }
    });
    // dataSort();
}

function searchTable(obj, tableName) {
    $("select[name='tableName']").val(tableName);

    var prefix = tableName.substring(0, 3);
    if (prefix == 'sys') {
        $("#syncCode").addClass("layui-btn-disabled");
    } else {
        $("#syncCode").removeClass("layui-btn-disabled");
    }

    $(".sysTable div").removeClass("selected");
    $(obj).addClass("selected");


    form.render('select', null);
    $("button[type='submit']").click();
}

function showCode() {
    var tableName = $("select[name='tableName']").val();
    if (tableName == "") {
        layer.msg("请选择一张表");
        return false;
    }
    openFrame(tableName, 'show_code.html?tableName=' + tableName, ["100%", "100%"]);
}

function syncCode() {
    var tableName = $("select[name='tableName']").val();
    if (tableName == "") {
        layer.msg("请选择一张表");
        return false;
    }

    var prefix = tableName.substring(0, 3);

    if ($("#syncCode").hasClass("layui-btn-disabled")) {
        return false;
    }
    if (prefix == 'system') {
        layer.msg("系统表不允许同步");
        return false;
    }
    //syncCode
    openFrame("同步代码", './sync.html?tableName=' + tableName, ['640px', '480px']);


}
//位置移动
function moveTr(obj, type, orgId, courseId) {

    var tableName = ("select[name='tableName']").val();
    if (type == 'up') {
        var objParentTR = $(obj).parent().parent();
        var prevTR = objParentTR.prev();
        if (prevTR.length > 0) {
            var sort1 = objParentTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val();
            var sort2 = prevTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val();
            objParentTR.hide();
            objParentTR.show(800);

            prevTR.hide();
            prevTR.show(800);

            objParentTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val(sort2);
            prevTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val(sort1);
            prevTR.insertAfter(objParentTR);
        }
    } else {
        var objParentTR = $(obj).parent().parent();
        var nextTR = objParentTR.next();
        if (nextTR.length > 0) {
            var sort1 = objParentTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val();
            var sort2 = nextTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val();

            objParentTR.hide();
            objParentTR.show(800);

            nextTR.hide();
            nextTR.show(800);

            objParentTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val(sort2);
            nextTR.find("input[name='sort[" + orgId + "][" + courseId + "][]']").val(sort1);

            nextTR.insertBefore(objParentTR);
        }
    }
}