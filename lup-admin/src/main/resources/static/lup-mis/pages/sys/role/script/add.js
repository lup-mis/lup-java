/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

var setting = {
    check: {
        enable: true
    },
    data: {
        simpleData: {
            enable: true
        }
    }
};
var treeObj;
var pageActionObj;

function getTreeListNodes() {
    var treeIdsArray = new Array();
    var nodes = treeObj.getCheckedNodes();
    for (var i = 0; i < nodes.length; i++) {
        var type = nodes[i].type;
        treeIdsArray.push(nodes[i].id);
    }
    var treeIds = treeIdsArray.join(',');
    $("#treeIds").val(treeIds);
}

function getPageRoleListNodes() {
    var pageIdArray = new Array();
    var actionIdArray = new Array();
    var nodes = pageActionObj.getCheckedNodes();
    if (nodes.length == 0) {
        $("#pageIds").val("");
        return false;
    }
    for (var i = 0; i < nodes.length; i++) {
        var type = nodes[i].type;
        if (type == 'page') {
            pageIdArray.push(nodes[i].id); //页面id
        } else if (type == 'action') {
            actionIdArray.push(nodes[i].id); //页面id
        }
    }
    var pageIds = pageIdArray.join(',');
    var actionIds = actionIdArray.join(',');


    var arr = actionIds.split(",");
    var json = {};
    for (var i = 0; i < arr.length; i++) {
        var arr1 = arr[i].split("@");
        var pageId = arr1[0];
        var actionId = arr1[1];
        json[pageId] = [arr1];
    }

    for (var val in json) {
        var actionIdArr = new Array();
        for (var j = 0; j < arr.length; j++) {
            var arr1 = arr[j].split("@");
            var pageId = arr1[0];
            var actionId = arr1[1];
            if (val == pageId && actionIdArr.indexOf(actionId) == -1) {
                actionIdArr.push(actionId);
            }
        }
        json[val] = actionIdArr;
    }
    $("#pageIds").val(JSON.stringify(json));
}

function setData() {
    getTreeListNodes();
    getPageRoleListNodes();
}

layui.use(['layer', 'table', 'form'], function() {
    ajaxStart();
    //统一给ajax请求加header参数
    ajaxSend();
    //截获ajax参数
    ajaxSetup();
    table = layui.table;
    form = layui.form;
    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
        // layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
    });


    request(baseGateway + "sys/tree/lists", {}, function(ret) {
        if (ret.code == 200) {
            var zNodes = [];
            var rows = ret.data.rows || ret.data;
            for (var i = 0; i < rows.length; i++) {
                var open = false;
                if (rows[i].pid == "0") {
                    open = true;
                }
                var row = { id: rows[i].id, name: rows[i].treeName, pId: rows[i].pid, checked: false, open: open };
                zNodes.push(row);
            }
            setting.check.chkboxType = { "Y": "ps", "N": "ps" };
            treeObj = $.fn.zTree.init($("#treeList"), setting, zNodes);
        } else {
            layer.msg(ret.msg, {
                shade: [0.1, '#393D49'],
                time: 1500,
                icon: 5
            });
        }
    });

    request(baseGateway + "sys/page/lists", { 'page': 1, 'limit': 9999 }, function(ret) {
        if (ret.code == 200) {
            var zNodes = [];
            var rows = ret.data.rows || ret.data;
            for (var i = 0; i < rows.length; i++) {
                var row = {
                    id: rows[i].id,
                    type: "page",
                    name: rows[i].pageName + "【" + rows[i].code + "】",
                    pId: "0",
                    checked: false,
                    open: false
                };
                zNodes.push(row);
                for (var j = 0; j < rows[i].sysAction.length; j++) {
                    var rowAction = {
                        id: rows[i].sysAction[j].id,
                        type: "action",
                        name: rows[i].sysAction[j].actionName + "【" + rows[i].sysAction[j].code + "】",
                        pId: rows[i].id,
                        checked: false,
                        open: false
                    };
                    zNodes.push(rowAction);
                }
            }
            setting.check.chkboxType = { "Y": "ps", "N": "ps" };
            pageActionObj = $.fn.zTree.init($("#pageActionList"), setting, zNodes);
        } else {
            layer.msg(ret.msg, {
                shade: [0.1, '#393D49'],
                time: 1500,
                icon: 5
            });
        }
    });

    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});