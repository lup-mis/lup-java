/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

function getTreeOption(data) {
    var treeList = [{ name: "顶级菜单", value: "0" }];
    for (var i = 0; i < data.length; i++) {
        if (data[i].pid == '0') {
            treeList[(i + 1)] = { name: data[i].treeName, value: data[i].id, children: [] };
            for (var j = 0; j < data.length; j++) {
                if (data[j].pid == data[i].id) {
                    treeList[(i + 1)].children[j] = { name: data[j].treeName, value: data[j].id, children: [] };
                    for (var k = 0; k < data.length; k++) {
                        if (data[k].pid == data[j].id) {
                            treeList[(i + 1)].children[j].children[k] = { name: data[k].treeName, value: data[k].id, children: [] };
                            for (var l = 0; l < data.length; l++) {
                                if (data[l].pid == data[k].id) {
                                    treeList[(i + 1)].children[j].children[k].children[l] = { name: data[l].treeName, value: data[l].id, disabled: true, children: [] };
                                }
                            }
                        }
                    }

                }
            }
        }
    }
    var demo1 = xmSelect.render({
        el: '#pid',
        autoRow: true,
        filterable: true,
        radio: true,
        clickClose: true,
        filterable: true,
        tips: '请选上级菜单',
        name: 'pid',
        layVerify: 'required',
        layVerType: 'msg',
        height: '300px',
        tree: {
            show: true,
            showFolderIcon: true,
            showLine: true,
            indent: 20,
            strict: false,
            //是否开启极简模式
            simple: false,
            expandedKeys: false,
        },
        toolbar: {
            show: false,
            list: ['ALL', 'REVERSE', 'CLEAR']
        },
        data: function() {
            return treeList;
        }
    })
}


function getTreeList(callback) {
    var url = baseGateway + 'sys/tree/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var rows = ret.data.rows || ret.data;
            getTreeOption(rows);
        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}

function getPageList() {
    var url = baseGateway + 'sys/page/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        if (code == 200) {
            var rows = ret.data.rows || ret.data;
            var str = '<option value="">请选择页面接口</option>';
            for (var i = 0; i < rows.length; i++) {
                str += '<option value="' + rows[i].id + '">' + rows[i].pageName + "【" + rows[i].code + '】</option>';
            }
            $("select[name='pageId']").html(str);
            layui.form.render('select', null);

        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}
$init(function() {
    getPageList();
    getTreeList();
    layui.form.on('switch(status)', function(data) {
        if (this.checked) {
            layui.jquery("input[name='status']").val(1);
        } else {
            layui.jquery("input[name='status']").val(-1);
        }
        // layer.tips('温馨提示：请注意开关状态的文字可以随意定义，而不仅仅是ON|OFF', data.othis)
    });
    var gateway = getQueryString("gateway");
    frameSubmit(baseGateway + gateway, true, function(ret) {
        setTimeout(function() {
            parent.window.location.reload();
        }, 1000);
    });
});