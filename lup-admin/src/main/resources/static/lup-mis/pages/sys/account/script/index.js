/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

//这里引用一下，否则不生效
layui.use(['layer', 'table', 'form'], function() {
    ajaxStart();
    //统一给ajax请求加header参数
    ajaxSend();
    //截获ajax参数
    ajaxSetup();
});
$(document).ajaxSend(function(event, jqxhr, settings) {
    jqxhr.setRequestHeader("accesstoken", localStorage.getItem("accesstoken"));
    jqxhr.setRequestHeader("accessuid", localStorage.getItem("accessuid"));
});
var accessuid = localStorage.getItem("accessuid");
var accesstoken = localStorage.getItem("accesstoken");
var setting = {
    async: {
        enable: true,
        contentType: "application/json",
        headers: {
            'accessuid': accessuid,
            'accesstoken': accesstoken
        },
        type: "post",
        dataType: "json",
        autoParam: ["id", "name"],
        url: baseGateway + "sys/org/lists"
    },
    view: {
        expandSpeed: "1",
        showLine: true,
        showIcon: true,
        showTitle: true,
        selectedMulti: false
    },
    edit: {
        enable: false
    },
    data: {
        simpleData: {
            enable: true,
            idKey: "id",
            pIdKey: "pid"
        }
    },
    callback: {
        onClick: onClick,
        onAsyncSuccess: zTreeOnAsyncSuccess, // 异步加载正常结束的事件回调函数
    }
};

//用于捕获异步加载正常结束的事件回调函数
function zTreeOnAsyncSuccess(event, treeId, treeNode, msg) {
    var treeObj = $.fn.zTree.getZTreeObj(treeId);
    var nodes = treeObj.getNodes();
    //默认展开前3个
    treeObj.expandAll(true);
    for (var i = 0; i < nodes.length; i++) {
        // treeObj.expandNode(nodes[i], true, false, false); //默认展开第一级节点
    }

}

function getChildren(ids, treeNode) {
    ids.push(treeNode.id);
    if (treeNode.isParent) {
        for (var obj in treeNode.children) {
            getChildren(ids, treeNode.children[obj]);
        }
    }
    return ids;
}

//目录点击事件
function onClick(event, treeId, treeNode, clickFlag) {
    var ids = [];
    ids = getChildren(ids, treeNode);
    $("input[name='orgId']").val(ids);
    $("button[type='submit']").click();
    //调用搜索
}

var treeDemoObj;
$(function() {
    treeDemoObj = $.fn.zTree.init($("#treeDemo"), setting);

    var windowH = $(window).height();
    if (windowH <= 300) {
        windowH = 300;
    }
    $("#right").css("height", (windowH - 125) + "px");
    $("#left").css("height", (windowH - 125) + "px");
    $("#treeDemo").css("height", (windowH - 135) + "px");
    $(window).resize(function() { //当浏览器大小变化时
        var windowH = $(window).height();
        if (windowH <= 300) {
            windowH = 300;
        }
        $("#right").css("height", (windowH - 125) + "px");
        $("#left").css("height", (windowH - 125) + "px");
        $("#treeDemo").css("height", (windowH - 135) + "px");
    });
});


function getRoleList() {
    var data = {
        "page": 1,
        "limit": 10000
    };

    request(baseGateway + 'sys/role/lists', data, function(ret) {
        var code = ret.code;
        var rows = ret.data.rows || ret.data;
        if (code == 200 && rows.length > 0) {
            var selectData = [];
            for (var i = 0; i < rows.length; i++) {
                var item = { name: rows[i].roleName, value: rows[i].id };
                selectData.push(item);
            }
            var roleSelectBox = xmSelect.render({
                el: '#roleSelectBox',
                filterable: true,
                tips: '用户角色',
                name: 'roleIds',
                on: function(data) {},
                data: selectData
            });
            layer.closeAll("loading");
        }
    });
}

function showAccountToken(accountToken) {
    var accountTokenObj = eval("(" + decodeURIComponent(accountToken) + ")");
    var expireTime = timestampToDate(accountTokenObj.expireTime);
    var userAgent = accountTokenObj.userAgent;
    var ip = accountTokenObj.ip;

    layer.open({
        type: 1,
        skin: 'layui-layer-demo', //样式类名
        closeBtn: 0, //不显示关闭按钮
        anim: 2,
        shadeClose: true, //开启遮罩关闭
        content: '<div style="line-height:40px;padding:10px;width:480px;"><div>过期时间：' + expireTime + '</div><div>IP地址：' + ip + '</div><div>浏览器信息：' + userAgent + '</div></div>'
    });


}
layui.use(['layer', 'table', 'form'], function() {
    table = layui.table;
    form = layui.form;
    layui.$(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function(e) {
        e.stopPropagation();
    });
    //单击行勾选radio事件
    layui.$(document).on("click", ".layui-table-body table.layui-table tbody tr", function() {
        var index = layui.$(this).attr('data-index');
        var tableBox = layui.$(this).parents('.layui-table-box');
        //存在固定列
        var tableDiv;
        if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length > 0) {
            tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
        } else {
            tableDiv = tableBox.find(".layui-table-body.layui-table-main");
        }
        var checkCell = tableDiv.find("tr[data-index=" + index + "]").find("td div.laytable-cell-radio div.layui-form-radio I");
        if (checkCell.length > 0) {
            checkCell.click();
        }
    });

    layui.$(document).on("click", "td div.laytable-cell-radio div.layui-form-radio", function(e) {
        e.stopPropagation();
    });

    pageInit(function(pageInfo, toolbarHtml) {
        getRoleList();
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;
        var cols = [{
            type: 'radio'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 60,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'accountName',
            title: '账号',
            minWidth: 80,
            sort: true
        }, {
            field: 'roleId',
            title: '角色',
            minWidth: 70,
            templet: function(d) {
                if (!isEmpty(d.roleRes)) {
                    var _arr = [];
                    for (var i = 0; i < d.roleRes.length; i++) {
                        _arr[i] = d.roleRes[i].roleName
                    }
                    return _arr;
                } else {
                    return "";
                }

            },
            sort: false
        }, {
            field: 'fullName',
            title: '姓名',
            width: 80,
            sort: true
        }, {
            field: 'orgId',
            title: '机构',
            minWidth: 70,
            templet: function(d) {
                if (!isEmpty(d.orgRes) && !isEmpty(d.orgRes.name)) {
                    return d.orgRes.name
                } else {
                    return "";
                }

            },
            sort: true
        }, {
            field: 'sex',
            title: '性别',
            width: 70,
            templet: function(d) {
                if (d.sex == "m") {
                    return "<span style='color:green'>男</span>"
                } else if (d.sex == "w") {
                    return "<span style='color:red'>女</span>";
                } else {
                    return "未知";
                }
            },
            sort: true
        }, {
            field: 'phone',
            title: '联系电话',
            width: 100,
            sort: true
        }, {
            field: 'accountToken',
            title: '在线状态',
            width: 100,
            sort: false,
            templet: function(d) {
                if (!isEmpty(d.accountToken)) {
                    return "<span style='color:green' onclick=showAccountToken('" + encodeURIComponent(JSON.stringify(d.accountToken)) + "')>在线</span>"
                } else {
                    return "<span style='color:#999999'>离线</span>";
                }

            },
        }];
        var rightH = $("#right").height();
        var h = rightH - 50;

        loadData(baseGateway + queryUrl, cols, toolbarHtml, null, h + "px");
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data) {
            // console.log(JSON.stringify(data));
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});