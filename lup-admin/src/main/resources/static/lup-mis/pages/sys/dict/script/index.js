/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
            type: 'radio'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true,
            sort: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 60,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'dictName',
            title: '字典名称',
            minWidth: 100,
            align: 'center',
            sort: true,
            exit: 'text'
        }, {
            field: 'dictCode',
            title: '字典编码',
            minWidth: 100,
            sort: true,
            exit: 'text'
        }, {
            field: 'remarks',
            title: '备注说明',
            minWidth: 100,
            sort: true,
            exit: 'text'
        }, {
            field: 'status',
            title: '状态',
            width: 80,
            sort: true,
            templet: function(d) {
                if (d.status == 1) {
                    return '<span style="color:green">正常</span>';
                } else {
                    return '<span style="color:red">禁用</span>';
                }
            }
        }];
        loadData(baseGateway + queryUrl, cols, toolbarHtml);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data, pars, event) {
            if (event == 'dictDataList') {
                if (data.length == 0) {
                    layer.msg('请选择一条记录');
                } else if (data.length > 1) {
                    layer.msg('只能选择一条记录');
                } else {
                    var jump = pars.jump;
                    var gateway = pars.gateway;
                    var query = pars.query;
                    var tips = pars.tips;
                    var width = pars.width;
                    var height = pars.height;
                    if (isEmpty(width)) {
                        width = "100%";
                    }
                    if (isEmpty(height)) {
                        height = "100%";
                    }
                    var area = [width, height];
                    if (isEmpty(tips)) {
                        tips = "<span style='color:red'>参数未定义</span>";
                    }
                    console.log(jump + '&dictCode=' + data[0].dictCode + "&gateway=" + gateway + "&query=" + query);
                    openFrame(tips, jump + '&dictCode=' + data[0].dictCode + "&gateway=" + gateway + "&query=" + query, area);
                }
            }
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});