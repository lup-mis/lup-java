/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    var windowH = $(window).height();
    if (windowH <= 300) {
        windowH = 300;
    }
    $("#right").css("height", (windowH - 25) + "px");
    $("#left").css("height", (windowH - 25) + "px");
    $(window).resize(function() { //当浏览器大小变化时
        var windowH = $(window).height();
        if (windowH <= 300) {
            windowH = 300;
        }
        $("#right").css("height", (windowH - 25) + "px");
        $("#left").css("height", (windowH - 25) + "px");
    });


    getSearchPageList();
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
            type: 'radio'
        }, {
            field: 'id',
            title: 'ID',
            width: 140,
            hide: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 50,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'actionName',
            title: '功能(按钮)',
            width: 100,
            sort: true,
            edit: 'text'
        }, {
            field: 'code',
            title: '功能编码',
            minWidth: 150,
            sort: true,
            edit: 'text'
        }, {
            field: 'bindEvent',
            title: '事件',
            width: 80,
            sort: true,
            edit: 'text'
        }, {
            field: 'tips',
            title: '提示标题',
            minWidth: 100,
            sort: true,
            edit: 'text'
        }, {
            field: 'tableName',
            title: '映射表',
            width: 100,
            sort: true
        }, {
            field: 'bgColor',
            title: '颜色',
            width: 75,
            align: 'center',
            templet: function(d) {
                return '<span style="background:' + d.bgColor + ';text-align:center; display:block;color:#ffffff;">' + d.bgColor + '</span>';
            },
        }, {
            field: 'targetType',
            title: '请求',
            width: 80,
            sort: true
        }, {
            field: 'width',
            title: '宽',
            width: 50,
            edit: 'text'
        }, {
            field: 'height',
            title: '高',
            width: 50,
            edit: 'text'
        }, {
            field: 'jump',
            title: '前端地址',
            minWidth: 100,
            sort: true,
            edit: 'text'
        }, {
            field: 'queryUrl',
            title: '查询接口',
            minWidth: 100,
            sort: true,
            edit: 'text'
        }, {
            field: 'apiUrl',
            title: '数据提交接口',
            minWidth: 100,
            sort: true,
            edit: 'text'
        }, {
            field: 'status',
            title: '状态',
            width: 55,
            sort: true,
            templet: function(d) {
                if (d.status == 1) {
                    return '<span style="color:green">正常</span>';
                } else {
                    return '<span style="color:red">禁用</span>';
                }
            },
        }, {
            field: 'sort',
            title: '排序',
            width: 55,
            sort: true,
            edit: 'text'
        }];
        loadData(baseGateway + queryUrl, cols, toolbarHtml);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data) {
            // console.log(JSON.stringify(data));
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});

function searchAction(obj, pageId) {
    $(".leftMenu div").removeClass("selected");
    $(obj).addClass("selected");
    $("select[name='pageId']").val(pageId);
    form.render('select', null);
    $("button[type='submit']").click();
}

function _showPage(type) {
    if (type == '') {
        $(".pageBusiness").show();
        $(".pageSystem").show();
    }
    if (type == 'pageBusiness') {
        $(".pageBusiness").show();
        $(".pageSystem").hide();
    }
    if (type == 'pageSystem') {
        $(".pageBusiness").hide();
        $(".pageSystem").show();
    }
}

function getSearchPageList() {
    var url = baseGateway + 'sys/page/lists';
    request(url, { page: 1, limit: 10000 }, function(ret) {
        var code = ret.code;
        var _pageId = $("select[name='pageId']").val();
        var css;
        if (code == 200) {
            var str = '<option value="">页面接口</option>';
            var leftHtml = "";
            for (var i = 0; i < ret.data.rows.length; i++) {
                css = "pageBusiness";
                if (ret.data.rows[i].pageType == 1) {
                    css = "pageSystem";
                }

                if (_pageId == ret.data.rows[i].id) {
                    css += " selected";
                }
                var no = i + 1;
                if (no <= 9) {
                    no = "0" + no;
                }
                str += '<option value="' + ret.data.rows[i].id + '">' + ret.data.rows[i].pageName + "【" + ret.data.rows[i].code + "】 | 【 " + ret.data.rows[i].queryUrl + '】</option>';
                leftHtml += '<div style="position: relative;font-size:10px" class="' + css + '" onclick=searchAction(this,"' + ret.data.rows[i].id + '")>' + ret.data.rows[i].pageName + ' <span style="font-size:9px;display:block"> ' + ret.data.rows[i].code + '</span><span class="leftMenuNo">' + no + '</span></div>';
            }
            $("#pageList").html(leftHtml);
            $("select[name='pageId']").html(str);
            layui.form.render('select', null);
        } else {
            layer.msg(ret.msg, {
                icon: 5
            });
        }
    });
}