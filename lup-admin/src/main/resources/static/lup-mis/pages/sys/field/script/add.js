/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    var tableName = getQueryString("tableName");
    getTableList(tableName);
    getDictList();
    var gateway = getQueryString("gateway");

    form.on('select(dataSrcType)', function(data) {
        var fieldSrc = $("textarea[name='customSrc']").val();
        var str = "";
        if (fieldSrc == "") {
            if (data.value == 'db-table') {
                var json = {
                    "api": "",
                    "table": "",
                    "key": "id",
                    "value": "id,xxx",
                    "search": "xxx",
                    "cut": "-",
                    "isParentRoot": "false",
                    "parentId":"",
                    "sonId":""
                }
                str = JSON.stringify(json);
            } else if (data.value == 'tree-parent') {
                var json = {
                    "api": "sys/catalog/lists",
                    "pid": "0",
                    "table": "sys_catalog",
                    "key": "id",
                    "value": "name",
                    "cut": "-",
                    "isParentRoot": "false",
                    "parentId":"",
                    "sonId":""
                }
                str = JSON.stringify(json);
            }
            $("textarea[name='customSrc']").val(str);
            formatJson(str, -1);
        }
    });
    form.on('select(inputType)', function(data) {
        var inputType = $("select[name='inputType']").val();
        var fieldSrc = $("textarea[name='customSrc']").val();
        var str = "";
        if (fieldSrc == "") {
            if (data.value == 'singleFile' || data.value == 'multiFile' || data.value == 'singleFileRemarks' || data.value == 'multiFileRemarks') {
                var json = {
                    "exts": "",
                    "path": "",
                    "thumb": ""
                }
                str = JSON.stringify(json);
                $("textarea[name='customSrc']").val(str);
                formatJson(str, -1);

            }
            $("textarea[name='customSrc']").val(str);
            formatJson(str, -1);
        }
        if (inputType == 'textEditor' || inputType == 'singleFile' || inputType == 'multiFile' || inputType == 'singleFileRemarks' || inputType == 'multiFileRemarks') {
            $("select[name='isAllowReplace']").val(1);
        } else {
            $("select[name='isAllowReplace']").val(-1);
        }
        form.render('select', null);
    });


    form.on('select(fieldType)', function(data) {
        if (data.value == 'int') {
            $("input[name='fieldLen']").val(10);
        } else if (data.value == 'bigint') {
            $("input[name='fieldLen']").val(19);
        } else if (data.value == 'tinyint') {
            $("input[name='fieldLen']").val(3);
        } else if (data.value == 'decimal') {
            $("input[name='fieldLen']").val('10,2');
        } else if (data.value == 'char') {
            $("input[name='fieldLen']").val(20);
        } else if (data.value == 'varchar') {
            $("input[name='fieldLen']").val(50);
        } else {
            $("input[name='fieldLen']").val(0);
        }

        if (data.value == 'int' || data.value == 'bigint' || data.value == 'tinyint') {
            if ($("input[name='fieldDefaultValue']").val() == '') {
                $("input[name='fieldDefaultValue']").val(0);
            }
        } else if (data.value == 'decimal') {
            if ($("input[name='fieldDefaultValue']").val() == '') {
                $("input[name='fieldDefaultValue']").val('0.00');
            }
        } else {
            if ($("input[name='fieldDefaultValue']").val() == 0 || $("input[name='fieldDefaultValue']").val() == 0.00) {
                $("input[name='fieldDefaultValue']").val('');
            }
        }
        if (data.value == "datetime") {
            $("input[name='fieldDefaultValue']").val('1970-01-01 08:00:00');
        }
        if (data.value == "date") {
            $("input[name='fieldDefaultValue']").val('1970-01-01');
        }
        if (data.value == "time") {
            $("input[name='fieldDefaultValue']").val('00:00:00');
        }
    });
    frameSubmit(baseGateway + gateway, true, function(ret) {

    });
});

function getDictList() {
    var data = {
        "page": 1,
        "limit": 10000
    };
    request(baseGateway + 'sys/dict/lists', data, function(ret) {
        var code = ret.code;
        if (code == 200 && ret.data.rows.length > 0) {
            var str = '<option value="">数据字典</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {

                str += '<option value="' + ret.data.rows[i].dictCode + '">' + ret.data.rows[i].dictName + " / " + ret.data.rows[i].dictCode + '</option>';
            }
            $("select[name='dictCode']").html(str);
            form.render('select', null);
        }

    });
}

function getTableList(_tableName) {
    var data = {
        "page": 1,
        "limit": 10000
    };
    request(baseGateway + 'sys/table/lists', data, function(ret) {
        var code = ret.code;
        if (code == 200 && ret.data.rows.length > 0) {
            var str = '<option value="">数据表</option>';
            for (var i = 0; i < ret.data.rows.length; i++) {
                var select = "";
                if (_tableName == ret.data.rows[i].tableName) {
                    select = "selected";
                }
                str += '<option value="' + ret.data.rows[i].tableName + '" ' + select + '>' + ret.data.rows[i].tableRemarks + " / " + ret.data.rows[i].tableName + '</option>';
            }
            $("select[name='tableName']").html(str);
            form.render('select', null);
        }

    });
}

function formatJson(json, tag) {
    try {
        if (typeof JSON.parse(json) == "object") {
            if (tag == 1) {
                layer.msg('校验通过', {
                    shade: [0.1, '#393D49'],
                    time: 2000,
                    icon: 6
                });
            }
            $("textarea[name='customSrc']").val(JSON.stringify(JSON.parse(json), null, 4));
            return true;
        }
    } catch (e) {}
    if (tag == 1) {
        layer.msg('JSON格式不正确', {
            shade: [0.1, '#393D49'],
            time: 2000,
            icon: 5
        });
    }
    return false;
}