/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var apiUrl = pageInfo.apiUrl;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
            type: 'radio'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 65,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'tableName',
            title: '表名称',
            minWidth: 150,
            sort: true
        }, {
            field: 'tableRemarks',
            title: '表说明',
            minWidth: 120,
            sort: true
        }, {
            field: 'orderBy',
            title: '缓存排序',
            minWidth: 120,
            sort: true
        }, {
            field: 'tableType',
            title: '表类型',
            width: 100,
            templet: function(d) {
                if (d.tableType == 2) {
                    return '<span style="color:green">业务表</span>';
                } else {
                    return '<span style="color:red">系统表</span>';
                }
            }
        }, {
            field: 'listSelectType',
            title: '生成列表选择类型',
            minWidth: 100,
            templet: function(d) {
                if (d.listSelectType == 'radio') {
                    return '<span style="color:green">单选按钮</span>';
                } else {
                    return '<span style="color:red">复选框</span>';
                }
            }
        }, {
            field: 'isRedis',
            title: '缓存',
            width: 100,
            templet: function(d) {
                if (d.isRedis == 1) {
                    return '<span style="color:green">是</span>';
                } else {
                    return '<span style="color:red">否</span>';
                }
            }
        }];


        loadData(baseGateway + queryUrl, cols, toolbarHtml);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data) {
            // console.log(JSON.stringify(data));
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});