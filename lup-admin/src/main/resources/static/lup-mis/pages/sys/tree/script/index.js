/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

function loadTree(treetable, pageInfo, toolbarHtml) {
    // 渲染表格
    layer.load(2);
    // getTreeList();
    if (!toolbarHtml) {
        toolbarHtml = false;
    }
    if (toolbarHtml == '<div class="layui-btn-container"></div>') {
        toolbarHtml = false;
    }
    var queryUrl = pageInfo.queryUrl;
    var height = $(window).height() - 20;
    treetable.render({
        treeColIndex: 1,
        treeSpid: '0',
        treeIdName: 'id',
        treePidName: 'pid',
        toolbar: toolbarHtml,
        elem: '#data-list',
        url: baseGateway + queryUrl,
        defaultToolbar: ['filter', 'print'],
        page: false,
        height: height,
        cols: [
            [{
                    type: 'radio',
                }, {
                    field: 'treeName',
                    minWidth: 250,
                    title: '菜单名称',
                    edit: 'text'
                }, {
                    field: 'treeAlias',
                    align: 'center',
                    edit: 'text',
                    title: '菜单别名'
                }, {
                    field: 'icon',
                    align: 'center',
                    title: '菜单图标',
                    edit: 'text'
                }, {
                    field: 'pageId',
                    minWidth: 250,
                    align: 'center',
                    title: '页面接口',
                    templet: function(d) {
                        if (d.sysPageRes && d.sysPageRes.pageName) {
                            return d.sysPageRes.pageName + " " + d.sysPageRes.code;
                        } else {
                            return "";
                        }
                    },
                }, {
                    field: 'status',
                    width: 100,
                    align: 'center',
                    templet: function(d) {
                        if (d.status == 1) {
                            return '<span style="color:green">正常</span>';
                        } else {
                            return '<span style="color:red">禁用</span>';
                        }
                    },
                    title: '状态'
                }, {
                    field: 'treeType',
                    width: 100,
                    align: 'center',
                    templet: function(d) {
                        if (d.treeType == 1) {
                            return '<span style="color:red">系统菜单</span>';
                        } else {
                            return '<span style="color:green">业务菜单</span>';
                        }
                    },
                    title: '菜单类型'
                }, {
                    field: 'sort',
                    width: 70,
                    align: 'center',
                    title: '排序',
                    edit: 'text'
                }, {
                    field: 'id',
                    title: 'ID',
                    hide: true
                }
                // , {
                //     templet: '#auth-state',
                //     width: 120,
                //     align: 'center',
                //     title: '操作'
                // }
            ]
        ],
        done: function() {
            layer.closeAll('loading');
        }
    });

}
ajaxStart();
//统一给ajax请求加header参数
ajaxSend();
//截获ajax参数
ajaxSetup();
$init(function() {
    toolbarTap(function(data, pars, event) {

    });
});

function reloadTree() {
    pageInit(function(pageInfo, toolbarHtml) {
        loadTree(treetable, pageInfo, toolbarHtml);
    });
}
var treetable;
layui.use(['table', 'treetable'], function() {
    var table = layui.table;
    treetable = layui.treetable;
    //这里引用一下，否则不生效
    pageInit(function(pageInfo, toolbarHtml) {
        loadTree(treetable, pageInfo, toolbarHtml);
        //监听单元格编辑
        layui.table.on('edit(data-list)', function(obj) {
            var value = obj.value; //得到修改后的值
            var data = obj.data //得到所在行所有键值
            var fieldName = obj.field; //得到字段
            var that = this;
            var oldText = layui.jquery(that).prev().text();
            var url = baseGateway + 'sys/ajax/tableEdit';
            request(url, { "tableName": "sys_tree", "id": data.id, "fieldName": fieldName, "value": value }, function(ret) {
                var code = ret.code;
                if (code == 200) {
                    layer.msg(ret.msg, {
                        icon: 6
                    });
                } else {
                    var oldData = JSON.parse('{"' + fieldName + '":"' + oldText + '"}');
                    //重新赋值
                    obj.update(oldData);
                    layer.msg(ret.msg, {
                        icon: 5
                    });
                }
            });

        });
    });
    toolbarTap(function(data, pars, event) {

    }, function(data, ret, event) {
        if (event == 'del') {
            reloadTree();
        }
    });
});