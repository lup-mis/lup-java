/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    pageInit(function(pageInfo, toolbarHtml) {
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;

        var cols = [{
            type: 'checkbox'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 65,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'accountName',
            title: '登录账号',
            width: 100,
            sort: true
        }, {
            field: 'password',
            title: '密码',
            width: 70,
            sort: false
        }, {
            field: 'ip',
            title: '登录IP',
            width: 100,
            sort: true
        }, {
            field: 'browser',
            title: '浏览器',
            minWidth: 100,
            sort: true
        }, {
            field: 'status',
            title: '状态',
            width: 80,
            sort: true,
            templet: function(d) {
                if (d.status == 1) {
                    return '<span style="color:green">成功</span>';
                } else {
                    return '<span style="color:red">失败</span>';
                }
            },
        }, {
            field: 'remarks',
            title: '备注',
            minWidth: 100,
            sort: true
        }, {
            field: 'login_time',
            title: '登录时间',
            width: 140,
            sort: true,
            templet: function(d) {
                return timeStampFmt(d.login_time)
            },
        }, {
            field: 'user_agent',
            title: '用户代理',
            minWidth: 100,
            sort: true
        }];
        loadData(baseGateway + queryUrl, cols, toolbarHtml);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data) {});
        //监听单元格编辑
        tableEdit(tableName);
    });
});