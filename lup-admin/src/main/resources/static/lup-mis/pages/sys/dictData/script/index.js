/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-02-02 11:18:21;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

$init(function() {
    pageInit(function(pageInfo, toolbarHtml) {
        var dictCode = getQueryString("dictCode");
        var jump = pageInfo.jump;
        var queryUrl = pageInfo.queryUrl;
        var tableName = pageInfo.tableName;
        var cols = [{
            type: 'radio'
        }, {
            field: 'id',
            title: 'ID',
            width: 265,
            hide: true,
            sort: false
        }, {
            field: 'systemSerialNumber',
            title: '序号',
            width: 60,
            sort: true,
            align: 'center',
            type: 'numbers'
        }, {
            field: 'dictLabel',
            title: '字典标签',
            minWidth: 100,
            align: 'center',
            sort: false,
            edit: 'text'
        }, {
            field: 'dictValue',
            title: '字典键值',
            minWidth: 100,
            sort: false,
            edit: 'text'
        }, {
            field: 'sort',
            title: '顺序',
            width: 80,
            sort: false,
            edit: 'text'
        }, {
            field: 'isDefault',
            title: '默认',
            width: 80,
            sort: false,
            templet: function(d) {
                if (d.is_default == 1) {
                    return '<span style="color:green">是</span>';
                } else {
                    return '<span style="color:red">否</span>';
                }
            }
        }, {
            field: 'status',
            title: '状态',
            width: 80,
            sort: false,
            templet: function(d) {
                if (d.status == 1) {
                    return '<span style="color:green">正常</span>';
                } else {
                    return '<span style="color:red">禁用</span>';
                }
            }
        }, {
            field: 'remarks',
            title: '备注说明',
            minWidth: 100,
            sort: false,
            edit: 'text'
        }, {
            field: 'cssStyle',
            title: 'CSS样式',
            minWidth: 100,
            sort: false,
            edit: 'text'
        }];
        var height = $(window).height() - 20 + "px";
        loadData(baseGateway + queryUrl + "/" + dictCode, cols, toolbarHtml, function() {}, height);
        // 监听搜索操作
        doSearch();
        //监听操作事件(添加 删除 编辑)
        toolbarTap(function(data, pars, event) {
            //addDictData
            if (event == 'addDictData') {
                var jump = pars.jump;
                var gateway = pars.gateway;
                var query = pars.query;
                var tips = pars.tips;
                var width = pars.width;
                var height = pars.height;
                var area = [width, height];
                openFrame(tips, jump + "?gateway=" + gateway + "&dictCode=" + dictCode, area);
            } else if (event == 'dictDataRefresh') {
                window.location.reload();
            }
        });
        //监听单元格编辑
        tableEdit(tableName);
    });
});