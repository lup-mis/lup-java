/**
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-01 16:35:22; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */

package com.lup.system.sys.domain;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class SysProvinces {
    //ID
    private String id;
    //省份代码
    @Size(max = 20,message = "省份代码不能超过{max}个字符")
    @NotEmpty(message = "省份代码不能为空")
    private String provinceCode;
    //省份名称
    @Size(max = 50,message = "省份名称不能超过{max}个字符")
    @NotEmpty(message = "省份名称不能为空")
    private String provinceName;

} 