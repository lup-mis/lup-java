/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.domain;

import lombok.Data;

@Data
public class SysConfig {
    private String id;
    private String loginName;
    private String copyrightInfo;
    private String systemName;
    private String logo;
    private String author;
    private String contact;
    private String systemVersion;
    private String email;
    private String website;
    private String qq;
    private String wechat;
    private String safeIps;
    private Integer loginErrorLock;
    private Integer loginErrorNum;
    private Integer lockTime;
    private Integer leftTreeShow;
    private String secretKey;
    private String uploadChannel;
}
