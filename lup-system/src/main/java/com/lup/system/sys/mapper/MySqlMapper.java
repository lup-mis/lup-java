/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface MySqlMapper {
    void createTable(@Param("tableName") String tableName, @Param("tableRemarks") String tableRemarks);

    void editTable(@Param("oldTableName") String oldTableName, @Param("tableName") String tableName, @Param("tableRemarks") String tableRemarks);

    void addColumn(JSONObject json);

    void editColumn(JSONObject json);

    int tableEdit(JSONObject json);

    Map<String, Object> getTableAttr(String tableName);

    List<Map<String, Object>> getListsByTable(@Param("tableName") String tableName,@Param("orderBy") String orderBy);

    List<Map<String, Object>> getRedisTable();
}