/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.component.RoleComponent;
import com.lup.system.sys.service.SysRolePageActionService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Set;

@AllArgsConstructor
@Service
public class SysRolePageActionServiceImpl implements SysRolePageActionService {
    private final GetCacheComponent getCacheComponent;
    private final RoleComponent roleComponent;

    @Override
    public JSONObject lists(String code, String accountId) {
        JSONObject roleActionIdsArray = this.roleComponent.getRolePageAction(accountId);
        @SuppressWarnings("unchecked")
        Set<String> actionIdsArray = (Set<String>) roleActionIdsArray.get("actionId");
        if (StringUtils.isEmpty(code)) {
            return ResultUtils.fail("code不能为空.");
        }
        JSONObject retData = new JSONObject(true);
        //获取页面
        JSONArray pageArray = this.getCacheComponent.getCache("sys_page");
        JSONArray actionArray = this.getCacheComponent.getCache("sys_action");
        JSONObject pageRs = new JSONObject(true);
        int i = 0;
        while (i < pageArray.size()) {
            JSONObject json = pageArray.getJSONObject(i);
            if (JsonUtils.getString("code", json, "").equals(code)) {
                pageRs = json;
                break;
            }
            i++;
        }
        if (pageRs.size() == 0) {
            retData.put("page", new JSONObject());
            retData.put("action", new JSONArray());
        } else {
            String pageId = JsonUtils.getString("id", pageRs);
            retData.put("page", pageRs);
            //获取操作功能
            JSONArray _actionArray = new JSONArray();
            i = 0;
            while (i < actionArray.size()) {
                JSONObject json = actionArray.getJSONObject(i);
                //判断操作功能权限(如果没有操作权限则不追加)
                if (JsonUtils.getString("pageId", json, "").equals(pageId) && actionIdsArray.contains(json.getString("id"))) {
                    _actionArray.add(json);
                }
                i++;
            }
            retData.put("action", _actionArray);
        }

        return ResultUtils.success(retData);
    }
}
