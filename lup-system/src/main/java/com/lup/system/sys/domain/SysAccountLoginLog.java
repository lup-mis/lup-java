/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-03-27 15:29:18; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
package com.lup.system.sys.domain; 
import javax.validation.constraints.NotEmpty;
import java.util.Date; 
import lombok.Data; 
 
@Data 
public class SysAccountLoginLog { 

    //ID
    @NotEmpty(message = "ID不能为空")
    private String id;

    //登录名
    private String accountName;

    //登录IP
    private String ip;

    //浏览器
    private String browser;

    //状态
    private Integer status;
    private SysDictData statusRes;

    //备注
    private String remarks;

    //登录时间
    private Date loginTime;

    //代理信息
    private String userAgent;

    //密码
    private String password;
 
} 
