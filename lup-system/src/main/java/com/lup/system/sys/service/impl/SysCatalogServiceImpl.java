/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.CatalogComponent;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysCatalog;
import com.lup.system.sys.mapper.SysCatalogMapper;
import com.lup.system.sys.service.SysCatalogService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysCatalogServiceImpl implements SysCatalogService {
    private final SysCatalogMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysCatalog> lists(JSONObject dataJson) {
        // 使用mapper.xml中的sql进行排序
        List<SysCatalog> list = mapper.lists();
        String pid = JsonUtils.getString("pid", dataJson, "");
        if (StringUtils.isNotEmpty(pid)) {
            CatalogComponent catalogComponent = new CatalogComponent();
            catalogComponent.catalogList(list, pid);
            return JsonUtils.jsonArray2Domain(catalogComponent.catalogList, SysCatalog.class);
        }
        return list;
    }

    @Override
    public List<SysCatalog> lists(String pid) {
        return mapper.lists();
    }

    @Override
    public SysCatalog query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(String id) {
        // 判断是否还有下级菜单
        List<SysCatalog> treeList = mapper.getByPid(id);
        if (treeList.size() > 0) throw new LupException("该目录下还有下级目录，请先删除下级目录");
        int num = mapper.del(id);
        if (num == 0) throw new LupException("数据不存在，请刷新");
        setCacheComponent.setCache("sys_catalog");
        return num;
    }

    @Override
    public int edit(SysCatalog dataJson) {
        String pid = dataJson.getPid();
        String code = dataJson.getCode();
        if (pid.equals("0")) {
            if (code.equals("")) throw new LupException("根目录必须填写目录编码！");
        }
        int num = mapper.edit(dataJson);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_catalog");
        return num;
    }

    @Override
    public int editName(SysCatalog sysCatalog) {
        int num = mapper.editName(sysCatalog);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_catalog");
        return num;
    }

    @Override
    public JSONObject add(JSONObject dataJson) {
        String id = this.snowflake.nextId();
        dataJson.put("id", id);

        dataJson.putIfAbsent("name", "");
        dataJson.putIfAbsent("pid", "0");
        dataJson.putIfAbsent("status", 1);
        dataJson.putIfAbsent("code", "");
        dataJson.putIfAbsent("fileUrl", "");
        dataJson.putIfAbsent("remarks", "");
        dataJson.putIfAbsent("aliasName", "");
        int position = dataJson.getIntValue("position");

        SysCatalog sysCatalog = JSONObject.parseObject(dataJson.toJSONString(), SysCatalog.class);

        Integer maxSort = mapper.getMaxSortByPid(dataJson.getString("pid"));
        Integer minSort = mapper.getMinSortByPid(dataJson.getString("pid"));
        //开头创建
        int currentSort;
        if (position == 0) {
            currentSort = (minSort == null || minSort == 0) ? 1000000000 : minSort - 1;
        } else {
            currentSort = (maxSort == null || maxSort == 0) ? 1000000000 : maxSort + 1;
        }

        sysCatalog.setSort(currentSort);

        JSONObject json = new JSONObject(true);
        json.put("id", id);
        json.put("name", dataJson.get("name"));
        json.put("pid", dataJson.get("pid"));
        if (mapper.add(sysCatalog) == 0) throw new LupException("添加失败");
        return json;
    }

    @Override
    public int move(JSONObject dataJson) {
        String id = dataJson.getString("id");
        String targetId = dataJson.getString("targetId");
        int sort = mapper.query(id).getSort();
        int targetSort = mapper.query(targetId).getSort();
        int num = mapper.sort(id, targetSort);
        int num1 = mapper.sort(targetId, sort);
        return num + num1;
    }
}
