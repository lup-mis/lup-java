/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.IpUtils;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.component.RoleComponent;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.component.SysTreeComponent;
import com.lup.system.sys.service.SysInitService;
import lombok.AllArgsConstructor;
import org.apache.catalina.util.ServerInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
@Service
public class SysInitServiceImpl implements SysInitService {
    private final SysTreeComponent sysTreeComponent;
    private final GetCacheComponent getCacheComponent;
    private final SetCacheComponent setCacheComponent;
    private final RoleComponent roleComponent;

    //获取菜单，并且初始化数据字典等
    @Override
    public JSONObject tree(String accessUid, String accessToken, String accountName) {
        //获取菜单
        JSONArray sysTreeArray = this.getCacheComponent.getCache("sys_tree");
        //获取页面
        JSONArray sysPageArray = this.getCacheComponent.getCache("sys_page");
        Map<String, JSONObject> sysPageMap = new HashMap<>();
        for (int i = 0; i < sysPageArray.size(); i++) {
            JSONObject pageJson = sysPageArray.getJSONObject(i);
            sysPageMap.put(pageJson.getString("id"), pageJson);
        }
        JSONObject retData = new JSONObject(true);
        //获取配置文件
        JSONObject configJson = this.loginInfo();
        //组装树形菜单（这里需要检查是否有权限，如果没有权限则不显示给前端）
        JSONArray _treeArray = new JSONArray();
        Set<String> treeIds = roleComponent.getTreeIds(accessUid);
        for (int i = 0; i < sysTreeArray.size(); i++) {
            JSONObject treeRes = sysTreeArray.getJSONObject(i);
            if (treeIds.contains(treeRes.getString("id"))) {
                _treeArray.add(treeRes);
            }
        }
        JSONArray tree = this.sysTreeComponent.getTree(_treeArray, "0", sysPageMap);
        //初始化数据字典
        JSONArray dictData = this.getCacheComponent.getCache("sys_dict_data");
        JSONArray dictDataArray = new JSONArray();
        for (Object dictDatum : dictData) {
            JSONObject _json = (JSONObject) dictDatum;
            _json.remove("id");
            _json.remove("remarks");
            dictDataArray.add(_json);
        }
        JSONObject homeInfo = new JSONObject(true);
        homeInfo.put("title", "控制台");
        homeInfo.put("href", "pages/welcome.html");
        retData.put("homeInfo", homeInfo);
        retData.put("systemVersion", JsonUtils.getString("$.data.systemVersion", configJson, "1.0.0"));
        JSONObject logoInfo = new JSONObject(true);
        logoInfo.put("image", JsonUtils.getString("$.data.logo", configJson));
        logoInfo.put("href", "");
        logoInfo.put("title", JsonUtils.getString("$.data.systemName", configJson));
        retData.put("logoInfo", logoInfo);
        retData.put("leftTreeShow", JsonUtils.getString("$.data.leftTreeShow", configJson));
        retData.put("accountName", accountName);
        retData.put("menuInfo", tree);
        retData.put("dict", dictDataArray);
        return retData;
    }

    //登录信息
    @Override
    public JSONObject loginInfo() {
        return ResultUtils.success(this.getCacheComponent.config());
    }

    //欢迎页（控制台首页）
    @Override
    public JSONObject welcome() {
        //获取config信息
        JSONObject config = this.getCacheComponent.config();
        JSONObject retData = new JSONObject(true);
        retData.put("config", config);
        //获取系统信息
        JSONObject system = new JSONObject(true);
        system.put("serverInfo", ServerInfo.getServerInfo());
        system.put("jdkVersion", System.getProperty("java.version"));
        system.put("serverIp", IpUtils.getServerIP());
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = requestAttributes.getRequest();
        system.put("serverPort", request.getServerPort());
        system.put("serverDateTime", DateUtils.getNowDateTime());
        system.put("os", System.getProperty("os.name"));
        retData.put("system", system);
        return retData;
    }

    @Override
    public void cache() {
//        this.setCacheComponent.cacheAll();
        this.setCacheComponent.setCache();
    }

}
