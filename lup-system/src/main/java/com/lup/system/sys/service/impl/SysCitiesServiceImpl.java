/**
 * [LupMisAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-08-01 17:00:35;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.system.sys.service.impl;

import java.util.List;
import com.lup.core.utils.*;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import lombok.AllArgsConstructor;
import com.lup.system.sys.domain.SysCities;
import com.lup.system.sys.mapper.SysCitiesMapper;
import com.lup.system.sys.service.SysCitiesService;
import com.lup.system.component.SetCacheComponent;
import org.apache.commons.lang3.StringUtils;
import com.lup.system.component.GetCacheComponent;
import java.util.HashSet;
import com.lup.system.sys.domain.SysProvinces;
import com.lup.system.sys.mapper.SysProvincesMapper;
import com.lup.system.component.DataComponent;
import java.util.Set;

@AllArgsConstructor
@Service
public class SysCitiesServiceImpl implements SysCitiesService {
    private final SysCitiesMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;
    private final SysProvincesMapper sysProvincesMapper;

    @Override
    public List<SysCities> lists(JSONObject dataJson) {
        List<SysCities> list = this.mapper.lists(dataJson);
        //取出数据库对应关系的字段的值（这里用逗号隔开）
        Set<String> provinceCodesArray = new HashSet<>();
        for (SysCities item : list) {
            provinceCodesArray.add(item.getProvinceCode());
        }
        //将数据表sys_provinces中根据id字段取出符合符合条件的数据
        List<SysProvinces> sysProvinces = sysProvincesMapper.listsInByCode(StringUtils.join(provinceCodesArray, ","));
        for (SysCities item : list) {
            item.setProvinceCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("provinceCode", item.getProvinceCode(), sysProvinces), SysProvinces.class));
        }

        return list;
    }

    @Override
    public SysCities query(String id) {
        SysCities sysCities = mapper.query(id);
        //将数据表sys_provinces中根据id字段取出符合符合条件的数据
        List<SysProvinces> sysProvinces = sysProvincesMapper.listsInByCode(StringUtils.join(sysCities.getProvinceCode(), ","));
        sysCities.setProvinceCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("provinceCode", sysCities.getProvinceCode(), sysProvinces), SysProvinces.class));

        return sysCities;
    }

    @Override
    public int del(JSONObject idsJson) {
        String ids = idsJson.getString("ids");
        String[] strArray = ids.split(",");
        int delTotalNum = 0;
        for (String id : strArray) {
            int num = this.mapper.del(id);
            delTotalNum += num;
        }
        setCacheComponent.setCache("sys_cities");
        return delTotalNum;
    }

    @Override
    public int edit(SysCities sysCities) {
        int editNum = this.mapper.edit(sysCities);
        if(editNum == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_cities");
        return editNum;
    }

    @Override
    public int add(SysCities sysCities) {
        sysCities.setId(this.snowflake.nextId());
        int addNum = this.mapper.add(sysCities);
        if(addNum == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_cities");
        return addNum;
    }

}
