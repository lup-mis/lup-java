/**
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-01 16:35:22; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */

package com.lup.system.sys.service.impl;

import java.util.List;
import com.lup.core.utils.*;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import lombok.AllArgsConstructor;
import com.lup.system.sys.domain.SysProvinces;
import com.lup.system.sys.mapper.SysProvincesMapper;
import com.lup.system.sys.service.SysProvincesService;
import com.lup.system.component.SetCacheComponent;

@AllArgsConstructor
@Service
public class SysProvincesServiceImpl implements SysProvincesService {
    private final SysProvincesMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysProvinces> lists(JSONObject dataJson) {
        List<SysProvinces> list = this.mapper.lists(dataJson);

        return list;
    }

    @Override
    public SysProvinces query(String id) {
        SysProvinces sysProvinces = mapper.query(id);

        return sysProvinces;
    }

    @Override
    public int del(JSONObject idsJson) {
        String ids = idsJson.getString("ids");
        String[] strArray = ids.split(",");
        int delTotalNum = 0;
        for (String id : strArray) {
            int num = this.mapper.del(id);
            delTotalNum += num;
        }
        setCacheComponent.setCache("sys_provinces");
        return delTotalNum;
    }

    @Override
    public int edit(SysProvinces sysProvinces) {
        int editNum = this.mapper.edit(sysProvinces);
        if(editNum == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_provinces");
        return editNum;
    }

    @Override
    public int add(SysProvinces sysProvinces) {
        sysProvinces.setId(this.snowflake.nextId());
        int addNum = this.mapper.add(sysProvinces);
        if(addNum == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_provinces");
        return addNum;
    }

} 