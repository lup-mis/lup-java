/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.RedisUtils;
import com.lup.core.utils.Snowflake;
import com.lup.core.utils.Utils;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysAccount;
import com.lup.system.sys.domain.SysOrg;
import com.lup.system.sys.domain.SysRole;
import com.lup.system.sys.mapper.SysAccountMapper;
import com.lup.system.sys.mapper.SysOrgMapper;
import com.lup.system.sys.mapper.SysRoleMapper;
import com.lup.system.sys.service.SysAccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Service
public class SysAccountServiceImpl implements SysAccountService {
    private final SysAccountMapper mapper;
    private final SysOrgMapper sysOrgMapper;
    private final Snowflake snowflake;
    private final SysRoleMapper sysRoleMapper;
    private final SetCacheComponent setCacheComponent;
    private final RedisUtils redisUtils;

    @Override
    public List<SysAccount> lists(JSONObject dataJson) {

        String roleId = dataJson.getString("roleId");
        String orgId = dataJson.getString("orgId");
        if (roleId != null && !roleId.equals("")) {
            List<String> roleIdList = Arrays.asList(roleId.split(","));
            dataJson.put("roleId", roleIdList);
        }
        if (orgId != null && !orgId.equals("")) {
            List<String> orgIdList = Arrays.asList(orgId.split(","));
            dataJson.put("orgId", orgIdList);
        }

        List<SysAccount> list = this.mapper.lists(dataJson);
        int i = 0, n = list.size();
        while (i < n) {
            SysOrg sysOrgList = this.sysOrgMapper.query(list.get(i).getOrgId());
            list.get(i).setOrgRes(sysOrgList);
            //组装role
            String roleIds = list.get(i).getRoleIds();
            String[] roleIdsArray = roleIds.split(",");
            List<SysRole> roleRes = new LinkedList<>();
            for (String s : roleIdsArray) {
                //查询角色名称
                SysRole sysRole = this.sysRoleMapper.query(s);
                roleRes.add(sysRole);
            }
            list.get(i).setRoleRes(roleRes);

            JSONObject accountToken = JSONObject.parseObject(this.redisUtils.get("sys_account_token_" + list.get(i).getId()));
            list.get(i).setAccountToken(accountToken);
            i++;
        }
        return list;
    }

    @Override
    public SysAccount query(String id) {
        SysAccount sysAccountList = mapper.query(id);
        SysOrg sysOrgList = this.sysOrgMapper.query(sysAccountList.getOrgId());
        sysAccountList.setOrgRes(sysOrgList);
        return sysAccountList;
    }

    @Override
    public int del(JSONObject ids) {
        int num = mapper.del(ids.getString("ids"));
        if (num == 0) throw new LupException("数据不存在，请刷新");
        setCacheComponent.setCache("sys_account");
        return num;
    }

    @Transactional
    @Override
    public int edit(SysAccount sysAccount) {
        String password = sysAccount.getPassword();
        if (password != null && !password.equals("")) {
            sysAccount.setPassword(Utils.password(password));
        }
        int num = this.mapper.edit(sysAccount);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_account");
        return num;
    }

    /**
     * 修改密码
     */
    @Override
    public int updPwd(JSONObject data) {
        String accountId = data.get("accountId").toString();
        String password = data.getString("password");
        String oldPwd = data.getString("oldPwd");
        if (password != null && !password.equals("")) {
            data.put("password", Utils.password(password));
        }
        oldPwd = Utils.password(oldPwd);
        SysAccount _sysAccount = this.mapper.query(accountId);
        if (!_sysAccount.getPassword().equals(oldPwd)) throw new LupException("旧密码不正确");
        if (_sysAccount.getPassword().equals(Utils.password(password))) throw new LupException("新密码和旧密码不能一样");

        SysAccount sysAccount = JSONObject.parseObject(data.toJSONString(), SysAccount.class);
        sysAccount.setId(accountId);
        int num = mapper.updPwd(sysAccount);
        setCacheComponent.setCache("sys_account");
        return num;
    }

    @Transactional
    @Override
    public int add(SysAccount sysAccount) {
        sysAccount.setId(this.snowflake.nextId());
        sysAccount.setCreateTime(DateUtils.getNowDate());
        String password = sysAccount.getPassword();
        sysAccount.setPassword(Utils.password(password));
        //需要判断登录账号是否重复
        SysAccount sysAccountList = mapper.queryByAccountName(sysAccount.getAccountName());
        if (sysAccountList != null) throw new LupException("添加失败,登录账号重复");
        int addNun = mapper.add(sysAccount);
        if (addNun == 0) {
            throw new LupException("添加失败");
        }
        setCacheComponent.setCache("sys_account");
        return addNun;
    }
}
