/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.KaptchaUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.system.sys.service.SysVerifyCodeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@AllArgsConstructor
@Service
public class SysVerifyCodeServiceImpl implements SysVerifyCodeService {
    private final KaptchaUtils kaptchaUtils;

    @Override
    public JSONObject getVerifyCode(String verifyCodeToken) {
        JSONObject data = new JSONObject();
        JSONObject kaptcha;
        try {
            kaptcha = this.kaptchaUtils.code(verifyCodeToken);
            data.put("imgBase64", kaptcha.get("imgBase64"));
            data.put("verifyCodeToken", kaptcha.get("verifyCodeToken"));
        } catch (IOException e) {
            data.put("imgBase64", "");
            data.put("verifyCodeToken", verifyCodeToken);
            return ResultUtils.fail("", data);
        }

        return ResultUtils.success(data);
    }
}
