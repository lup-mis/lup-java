/**
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-01 17:17:02; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */

package com.lup.system.sys.service.impl;

import java.util.List;
import com.lup.core.utils.*;
import org.springframework.stereotype.Service;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import lombok.AllArgsConstructor;
import com.lup.system.sys.domain.SysAreas;
import com.lup.system.sys.mapper.SysAreasMapper;
import com.lup.system.sys.service.SysAreasService;
import com.lup.system.sys.domain.SysCities;
import com.lup.system.component.SetCacheComponent;
import org.apache.commons.lang3.StringUtils;
import com.lup.system.component.GetCacheComponent;
import java.util.HashSet;
import com.lup.system.component.DataComponent;
import java.util.Set;
import com.lup.system.sys.mapper.SysCitiesMapper;

@AllArgsConstructor
@Service
public class SysAreasServiceImpl implements SysAreasService {
    private final SysAreasMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;
    private final SysCitiesMapper sysCitiesMapper;

    @Override
    public List<SysAreas> lists(JSONObject dataJson) {
        List<SysAreas> list = this.mapper.lists(dataJson);
        //取出数据库对应关系的字段的值（这里用逗号隔开）
        Set<String> cityCodesArray = new HashSet<>();
        for (SysAreas item : list) {
            cityCodesArray.add(item.getCityCode());
        }
        //将数据表sys_cities中根据id字段取出符合符合条件的数据
        List<SysCities> sysCities = sysCitiesMapper.listsInByCode(StringUtils.join(cityCodesArray, ","));
        for (SysAreas item : list) {
            item.setCityCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("cityCode", item.getCityCode(), sysCities), SysCities.class));
        }

        return list;
    }

    @Override
    public SysAreas query(String id) {
        SysAreas sysAreas = mapper.query(id);
        //将数据表sys_cities中根据id字段取出符合符合条件的数据
        List<SysCities> sysCities = sysCitiesMapper.listsInByCode(StringUtils.join(sysAreas.getCityCode(), ","));
        sysAreas.setCityCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("cityCode", sysAreas.getCityCode(), sysCities), SysCities.class));

        return sysAreas;
    }

    @Override
    public int del(JSONObject idsJson) {
        String ids = idsJson.getString("ids");
        String[] strArray = ids.split(",");
        int delTotalNum = 0;
        for (String id : strArray) {
            int num = this.mapper.del(id);
            delTotalNum += num;
        }
        setCacheComponent.setCache("sys_areas");
        return delTotalNum;
    }

    @Override
    public int edit(SysAreas sysAreas) {
        int editNum = this.mapper.edit(sysAreas);
        if(editNum == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_areas");
        return editNum;
    }

    @Override
    public int add(SysAreas sysAreas) {
        sysAreas.setId(this.snowflake.nextId());
        int addNum = this.mapper.add(sysAreas);
        if(addNum == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_areas");
        return addNum;
    }

} 