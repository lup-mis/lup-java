/**
 * [LupMisAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-08-01 17:17:02;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.system.sys.domain;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
public class SysAreas {
    //ID
    private String id;
    //区域代码
    @Size(max = 20,message = "区域代码不能超过{max}个字符")
    @NotEmpty(message = "区域代码不能为空")
    private String areaCode;
    //区域名称
    @Size(max = 50,message = "区域名称不能超过{max}个字符")
    @NotEmpty(message = "区域名称不能为空")
    private String areaName;
    //城市代码
    @Size(max = 20,message = "城市代码不能超过{max}个字符")
    @NotEmpty(message = "城市代码不能为空")
    private String cityCode;
    private SysCities cityCodeRes;

}
