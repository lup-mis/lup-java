/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.*;
import com.lup.system.sys.domain.SysField;
import com.lup.system.sys.domain.SysTable;
import com.lup.system.sys.mapper.MySqlMapper;
import com.lup.system.sys.mapper.SysFieldMapper;
import com.lup.system.sys.mapper.SysTableMapper;
import com.lup.system.sys.service.SysTableService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@AllArgsConstructor
@Service
public class SysTableServiceImpl implements SysTableService {
    private final SysTableMapper mapper;
    private final MySqlMapper mySqlMapper;
    private final SysFieldMapper sysFieldMapper;
    private final Snowflake snowflake;

    @Override
    public JSONObject lists(JSONObject dataJson) {
        PageUtils.startPage(dataJson, true);
        List<SysTable> list = mapper.lists(dataJson);
        return ResultUtils.success(new PageBean<>(list));
    }

    @Override
    public JSONObject query(String id) {
        return ResultUtils.success(mapper.query(id));
    }

    @Override
    public JSONObject del(JSONObject idsJson) {
        // 删除前需要判断数据库是否还有字段
        String id = JsonUtils.getString("ids", idsJson, "0");
        SysTable tableList = mapper.query(id);
        String tableName = tableList.getTableName();
        List<SysField> fieldList = sysFieldMapper.getByTableName(tableName);
        if (fieldList.size() > 0) {
            return ResultUtils.fail("该数据表下还有字段，请先删除字段");
        }
        if (mapper.del(id) == 0) {
            return ResultUtils.fail("数据不存在，请刷新");
        }
        return ResultUtils.success("删除成功");
    }

    @Override
    public JSONObject edit(JSONObject dataJson) {
        SysTable sysTable = JSONObject.parseObject(dataJson.toJSONString(), SysTable.class);
        try {
            // 判断是否重复
            SysTable rows = mapper.getByTableName(JsonUtils.getString("tableName", dataJson), JsonUtils.getString("id", dataJson));
            if (rows != null) {
                return ResultUtils.fail("数据表名不能重复");
            }
            if (mapper.edit(sysTable) == 0) {
                return ResultUtils.fail("您未修改任何数据！");
            }
        } catch (Exception e) {
            return ResultUtils.fail("修改失败");
        }
        try {
            mySqlMapper.editTable(JsonUtils.getString("oldTableName", dataJson), JsonUtils.getString("tableName", dataJson), JsonUtils.getString("tableRemarks", dataJson));
            // 修改字段表数据
            sysFieldMapper.editByTableName(JsonUtils.getString("oldTableName", dataJson), JsonUtils.getString("tableName", dataJson));
            return ResultUtils.success("修改成功");
        } catch (Exception e) {
            return ResultUtils.success("请手动修改数据表");
        }

    }

    @Transactional
    @Override
    public JSONObject add(JSONObject dataJson) {

        dataJson.put("id", this.snowflake.nextId());
        SysTable sysTable = JSONObject.parseObject(dataJson.toJSONString(), SysTable.class);
        // 判断是否重复
        SysTable rows = mapper.getByTableName(JsonUtils.getString("tableName", dataJson), null);
        if (rows != null) {
            return ResultUtils.fail("数据表名不能重复");
        }
        try {
            if (mapper.add(sysTable) == 0) {
                return ResultUtils.fail("添加失败");
            }
        } catch (Exception e) {
            return ResultUtils.fail("添加失败");
        }
        // 开始创建数据表
        int status = 1;
        try {
            mySqlMapper.createTable(JsonUtils.getString("tableName", dataJson), JsonUtils.getString("tableRemarks", dataJson));
        } catch (Exception e) {
            status = 0;
        }
        // 给字段表初始化两个字段
        // 添加id字段
        JSONObject jsonId = new JSONObject(true);
        jsonId.put("id", this.snowflake.nextId());
        jsonId.put("tableName", dataJson.getString("tableName"));
        jsonId.put("fieldName", "id");
        jsonId.put("fieldRemarks", "ID");
        jsonId.put("fieldType", "bigint");
        jsonId.put("fieldLen", "19");
        jsonId.put("fieldDefaultValue", "");
        jsonId.put("dataSrcType", "none");
        jsonId.put("inputType", "input");
        jsonId.put("dataCheckType", "none");
        jsonId.put("must", 1);
        jsonId.put("colsWidth", 0);
        jsonId.put("listEdit", -1);
        jsonId.put("listShow", -1);
        jsonId.put("searchCondition", "none");
        jsonId.put("dictCode", "");
        jsonId.put("sort", 1);
        jsonId.put("customSrc", "");
        jsonId.put("isAllowReplace", -1);
        jsonId.put("remarks", "系统生成");

        sysFieldMapper.add(JSONObject.parseObject(jsonId.toJSONString(), SysField.class));

        // 添加数据生成时间
        JSONObject jsonTimeStamp = new JSONObject(true);
        jsonTimeStamp.put("id", this.snowflake.nextId());
        jsonTimeStamp.put("tableName", dataJson.getString("tableName"));
        jsonTimeStamp.put("fieldName", "create_time");
        jsonTimeStamp.put("fieldRemarks", "创建时间");
        jsonTimeStamp.put("fieldType", "datetime");
        jsonTimeStamp.put("fieldLen", "0");
        jsonTimeStamp.put("fieldDefaultValue", "1970-01-01 08:00:00");
        jsonTimeStamp.put("dataSrcType", "none");
        jsonTimeStamp.put("inputType", "input");
        jsonTimeStamp.put("dataCheckType", "ymdhis");
        jsonTimeStamp.put("must", 1);
        jsonTimeStamp.put("colsWidth", 0);
        jsonTimeStamp.put("listEdit", -1);
        jsonTimeStamp.put("listShow", -1);
        jsonTimeStamp.put("searchCondition", "none");
        jsonTimeStamp.put("dictCode", "");
        jsonTimeStamp.put("sort", 9999);
        jsonTimeStamp.put("customSrc", "");
        jsonTimeStamp.put("isAllowReplace", -1);
        jsonTimeStamp.put("remarks", "系统生成");

        sysFieldMapper.add(JSONObject.parseObject(jsonTimeStamp.toJSONString(), SysField.class));
        if (status == 1) {
            return ResultUtils.success("添加成功，数据表创建成功");
        }
        return ResultUtils.success("添加成功，数据表创建失败，请手动创建");
    }

}
