/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysAction;
import com.lup.system.sys.mapper.SysActionMapper;
import com.lup.system.sys.service.SysActionService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysActionServiceImpl implements SysActionService {
    private final SysActionMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysAction> lists(JSONObject data) {
        return mapper.lists(data);
    }

    @Override
    public SysAction query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(JSONObject ids) {
        int num = mapper.del(ids.getString("ids"));
        if (num == 0) throw new LupException("数据不存在，请刷新");
        setCacheComponent.setCache("sys_action");
        return num;
    }

    @Override
    public int edit(SysAction sysAction) {
        int num = this.mapper.edit(sysAction);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_action");
        return num;
    }

    @Override
    public int add(SysAction sysAction) {
        sysAction.setId(this.snowflake.nextId());
        int addNun = mapper.add(sysAction);
        if (addNun == 0) {
            throw new LupException("添加失败");
        }
        setCacheComponent.setCache("sys_action");
        return addNun;
    }
}
