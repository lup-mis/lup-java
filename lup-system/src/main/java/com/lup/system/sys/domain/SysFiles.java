/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-03-27 15:55:31; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
package com.lup.system.sys.domain; 
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date; 
import lombok.Data; 
 
@Data 
public class SysFiles { 

    //ID
    @NotEmpty(message = "ID不能为空")
    private String id;

    //原始名称
    @NotEmpty(message = "原始名称不能为空")
    private String oriName;

    //根路径
    private String filePath;

    //后缀
    private String suffix;

    //服务器端名
    @NotEmpty(message = "服务器端名不能为空")
    private String fileName;

    //访问路径
    @NotEmpty(message = "访问路径不能为空")
    private String fileUrl;

    //服务器保存路径
    @NotEmpty(message = "服务器保存路径不能为空")
    private String serverPath;

    //缩略图
    @NotEmpty(message = "缩略图不能为空")
    private String thumb;

    //大小
    @NotEmpty(message = "大小不能为空")
    private String fileSize;

    //上传时间
    @NotNull(message = "上传时间不能为空")
    private Date uploadTime;

    //上传者账号
    @NotEmpty(message = "上传者账号不能为空")
    private String account;

    //备注
    @NotEmpty(message = "备注不能为空")
    private String remarks;

    private String uploadChannel;
} 
