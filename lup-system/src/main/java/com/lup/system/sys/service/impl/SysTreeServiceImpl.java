/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.DataComponent;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysPage;
import com.lup.system.sys.domain.SysRoleTree;
import com.lup.system.sys.domain.SysTree;
import com.lup.system.sys.domain.vo.SysTreeVo;
import com.lup.system.sys.mapper.SysPageMapper;
import com.lup.system.sys.mapper.SysRoleTreeMapper;
import com.lup.system.sys.mapper.SysTreeMapper;
import com.lup.system.sys.service.SysTreeService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class SysTreeServiceImpl implements SysTreeService {
    private final SysTreeMapper mapper;
    private final SysPageMapper sysPageMapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;
    private final SysRoleTreeMapper sysRoleTreeMapper;

    @Override
    public List<SysTreeVo> lists(JSONObject dataJson) {
        //查询所有page
        JSONArray sysPageArray = JSONArray.parseArray(JSON.toJSONString(sysPageMapper.lists(null)));
        List<SysTreeVo> list = mapper.lists();
        //查询对应的page
        String roleId = JsonUtils.getString("roleId", dataJson, "");
        //查询该角色下所有菜单
        List<SysRoleTree> sysRoleTrees = sysRoleTreeMapper.queryByRoleId(roleId);
        List<String> treeIdArray = new ArrayList<>();
        for (SysRoleTree item : sysRoleTrees) {
            treeIdArray.add(item.getTreeId());
        }
        for (SysTreeVo item : list) {
            item.setChecked(treeIdArray.contains(item.getId()));
            item.setSysPageRes(JSONObject.parseObject(DataComponent.getJsonDbField("id", item.getPageId(), sysPageArray).toJSONString(), SysPage.class));
        }
        return list;
    }

    @Override
    public SysTree query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(JSONObject idsJson) {

        String id = JsonUtils.getString("ids", idsJson);
        // 判断是否还有下级菜单
        List<SysTree> treeList = mapper.getByPid(id);
        if (treeList.size() > 0) {
            throw new LupException("该菜单下还有下级菜单，请先删除下级菜单");
        }
        int num = mapper.del(id);
        if (num == 0) {
            throw new LupException("数据不存在，请刷新");
        }
        setCacheComponent.setCache("sys_tree");
        return num;
    }

    @Override
    public int edit(SysTree sysTree) {
        //SysTree p = JSONObject.parseObject(dataJson.toJSONString(), SysTree.class);
        int num = mapper.edit(sysTree);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_tree");
        return num;
    }

    @Override
    public int add(SysTree sysTree) {
        sysTree.setId(this.snowflake.nextId());
        int num = mapper.add(sysTree);
        if (num == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_tree");
        return num;
    }

}
