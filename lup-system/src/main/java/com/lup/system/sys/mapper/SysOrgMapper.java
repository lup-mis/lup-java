/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.mapper;

import com.lup.system.sys.domain.SysOrg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysOrgMapper {

    List<SysOrg> lists();

    List<SysOrg> queryByPid(String pid);

    SysOrg query(String id);

    int sort(@Param("id") String id, @Param("sort") int sort);

    int sortPrev(@Param("pid") String pid, @Param("sort") int sort);

    int sortNext(@Param("pid") String pid, @Param("sort") int sort);

    int del(String id);

    int edit(SysOrg sysOrg);

    int add(SysOrg sysOrg);
}