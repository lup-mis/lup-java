/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */

package com.lup.system.sys.mapper;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.system.sys.domain.SysField;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysFieldMapper {

    List<SysField> lists(JSONObject dataJson);

    List<Map<String, Object>> getReplaceField();

    SysField query(String id);

    List<SysField> getByTableName(String tableName);

    List<SysField> getByTableNameFieldName(@Param("tableName") String tableName, @Param("fieldName") String fieldName, @Param("id") String id);

    int del(String id);

    int edit(SysField p);

    int doReplaceField(JSONObject dataJson);

    int add(SysField p);

    int editByTableName(@Param("oldTableName") String oldTableName, @Param("tableName") String tableName);

}