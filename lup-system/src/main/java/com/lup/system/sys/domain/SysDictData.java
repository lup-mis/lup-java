/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SysDictData {
    private String id;
    @NotBlank(message = "字典标签不能为空!")
    private String dictLabel;
    @NotBlank(message = "字典键值!")
    private String dictValue;
    @NotBlank(message = "字典编码不能为空!")
    private String dictCode;
    private Integer isDefault;
    private String cssStyle;
    private Integer status;
    private String remarks;
    private Integer sort;
}
