/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:55:31;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.system.sys.mapper.SysFieldMapper;
import com.lup.system.sys.service.SysPathReplaceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Service
public class SysPathReplaceServiceImpl implements SysPathReplaceService {
    private final SysFieldMapper sysFieldMapper;

    @Override
    public JSONObject lists() {
        return ResultUtils.success(this.sysFieldMapper.getReplaceField());
    }

    @Override
    public JSONObject doReplace(JSONObject dataJson) {
        String ids = JsonUtils.getString("ids", dataJson, "0");
        String oriPath = JsonUtils.getString("oriPath", dataJson, "0");
        String newPath = JsonUtils.getString("newPath", dataJson, "0");
        if (!oriPath.startsWith("http://") && !oriPath.startsWith("https://")) {
            return ResultUtils.fail("原路径必须以http://或者https://开头");
        }
        if (!newPath.startsWith("http://") && !newPath.startsWith("https://")) {
            return ResultUtils.fail("新路径必须以http://或者https://开头");
        }
        if (newPath.equals(oriPath)) {
            return ResultUtils.fail("新路径和原路径不能一样！");
        }
        int totalReplace = 0;
        //获取字段
        List<Map<String, Object>> replaceFieldList = this.sysFieldMapper.getReplaceField();
        String[] idsArray = ids.split(",");
        for (String id : idsArray) {
            for (Map<String, Object> item : replaceFieldList) {
                if (item.get("id").toString().equals(id)) {
                    //开始替换
                    String fieldName = item.get("fieldName").toString();
                    String tableName = item.get("tableName").toString();
                    JSONObject pars = new JSONObject();
                    pars.put("tableName", tableName);
                    pars.put("fieldName", fieldName);
                    pars.put("oriPath", oriPath);
                    pars.put("newPath", newPath);
                    int num = this.sysFieldMapper.doReplaceField(pars);
                    totalReplace += num;
                }
            }
        }
        return ResultUtils.success("替换成功，本次共替换了【" + totalReplace + "】条数据");
    }
}
