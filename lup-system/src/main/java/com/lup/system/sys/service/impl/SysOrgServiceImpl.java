/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.ResultUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysOrg;
import com.lup.system.sys.mapper.SysOrgMapper;
import com.lup.system.sys.service.SysOrgService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysOrgServiceImpl implements SysOrgService {
    private final SysOrgMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysOrg> lists() {
        return mapper.lists();
    }

    @Override
    public JSONObject query(String id) {
        return ResultUtils.success(mapper.query(id));
    }

    @Override
    public JSONObject del(String id) {
        if (id.equals("1")) return ResultUtils.fail("根机构不能删除");
        List<SysOrg> list = mapper.queryByPid(id);
        if (list.size() > 0) return ResultUtils.fail("请先删除下级机构");
        String pid = mapper.query(id).getPid();
        if (mapper.del(id) == 0) return ResultUtils.fail("数据不存在，请刷新");
        //重新对数据排序
        List<SysOrg> sonList = mapper.queryByPid(pid);
        int i = 0, n = sonList.size();
        while (i < n) {
            mapper.sort(sonList.get(i).getId(), i + 1);
            i++;
        }
        setCacheComponent.setCache("sys_org");
        return ResultUtils.success("删除成功");
    }

    @Override
    public JSONObject edit(JSONObject dataJson) {
        SysOrg sysOrg = JSONObject.parseObject(dataJson.toJSONString(), SysOrg.class);
        if (mapper.edit(sysOrg) == 0) ResultUtils.fail("您未修改任何数据");
        setCacheComponent.setCache("sys_org");
        return ResultUtils.success("修改成功");
    }

    @Override
    public JSONObject add(JSONObject dataJson) {
        String id = this.snowflake.nextId();
        dataJson.put("id", id);
        dataJson.putIfAbsent("name", "");
        dataJson.putIfAbsent("pid", "0");
        dataJson.putIfAbsent("status", 1);
        dataJson.putIfAbsent("leader", "");
        dataJson.putIfAbsent("phone", "");
        dataJson.putIfAbsent("email", "");
        dataJson.putIfAbsent("address", "");
        dataJson.putIfAbsent("remarks", "");
        int position = dataJson.getIntValue("position");

        SysOrg sysOrg = JSONObject.parseObject(dataJson.toJSONString(), SysOrg.class);


        List<SysOrg> list = mapper.queryByPid(sysOrg.getPid());
        int i = 0, n = list.size();
        int maxSort = 0;
        if (n > 0) {
            maxSort = list.get(n - 1).getSort();
        }
        while (i < n) {
            if (position == 0) {
                mapper.sort(list.get(i).getId(), list.get(i).getSort() + 1);
            }
            i++;
        }
        int currentSort = (position == 0) ? 1 : maxSort + 1;
        sysOrg.setSort(currentSort);

        JSONObject json = new JSONObject(true);
        json.put("id", id);
        json.put("name", dataJson.get("name"));
        json.put("pid", dataJson.get("pid"));
        if (mapper.add(sysOrg) == 0) {
            return ResultUtils.fail("添加失败");
        }
        setCacheComponent.setCache("sys_org");
        return ResultUtils.success("添加成功", json);
    }

    @Override
    public JSONObject move(JSONObject dataJson) {
        String id = dataJson.getString("id");
        String targetId = dataJson.getString("targetId");
        int sort = mapper.query(id).getSort();
        int targetSort = mapper.query(targetId).getSort();
        mapper.sort(id, targetSort);
        mapper.sort(targetId, sort);
        setCacheComponent.setCache("sys_org");
        return ResultUtils.success("操作成功");
    }

}
