/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysDictData;
import com.lup.system.sys.mapper.SysDictDataMapper;
import com.lup.system.sys.service.SysDictDataService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysDictDataServiceImpl implements SysDictDataService {
    private final SysDictDataMapper mapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public JSONArray getAll() {
        List<SysDictData> list = this.mapper.getAll();
        JSONArray ret = new JSONArray();
        int i = 0;
        for (SysDictData item : list) {
            JSONObject data = new JSONObject(true);
            data.put("dictLabel", item.getDictLabel());
            data.put("dictValue", item.getDictValue());
            data.put("dictCode", item.getDictCode());
            data.put("isDefault", item.getIsDefault());
            data.put("cssStyle", item.getCssStyle());
            ret.add(i, data);
            i++;
        }
        return ret;
    }

    @Override
    public SysDictData query(String id) {
        return mapper.query(id);
    }


    @Override
    public int del(JSONObject ids) {
        int num = mapper.del(JsonUtils.getString("ids", ids));
        if (num == 0) throw new LupException("数据不存在，请刷新");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }

    @Override
    public int edit(SysDictData sysDictData) {
        int num = mapper.edit(sysDictData);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }

    @Override
    public int add(SysDictData sysDictData) {
        sysDictData.setId(this.snowflake.nextId());
        int num = mapper.add(sysDictData);
        if (num == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }

    @Override
    public List<SysDictData> listsByDictCode(String dictCode) {
        return this.mapper.listsByDictCode(dictCode);
    }
}
