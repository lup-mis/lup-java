/**
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.*;
import com.lup.system.component.DataComponent;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.sys.domain.SysFiles;
import com.lup.system.sys.mapper.SysFilesMapper;
import com.lup.system.sys.service.SysUploadService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@AllArgsConstructor
@Service
public class SysUploadServiceImpl implements SysUploadService {
    private final UpLoad upload;
    private final SysFilesMapper sysFilesMapper;
    private final Snowflake snowflake;
    private final GetCacheComponent getCacheComponent;
    private final AliOssUtil aliOssUtil;
    @Override
    public JSONObject upload(MultipartFile file, String path, String thumb, String remarks, String accountName, String userType) {
        //获取附件保存渠道
        JSONObject config = getCacheComponent.config();
        JSONObject uploadRes;
        String uploadChannel = "local";
        if(config.get("uploadChannel").equals("oss")){
            uploadChannel = "oss";
            uploadRes = aliOssUtil.fileUpload(file, path, thumb);
        }else{
            uploadRes = upload.fileUpload(file, path, thumb);
        }
        if (uploadRes.getIntValue("code") == 200) {
            JSONObject data = (JSONObject) uploadRes.get("data");
            String suffix = data.getString("suffix");
            String oriName = data.getString("oriName");
            String serverName = data.getString("serverName");
            String fileSize = data.getString("fileSize");
            String filePath = data.getString("filePath");
            String fileUrl = data.getString("fileUrl");
            String savePath = data.getString("savePath");
            String thumbStr = data.getString("thumb");

            JSONObject dataJson = new JSONObject(true);
            if (userType.equals("api-user")) {
                accountName = "api-user";
            }
            if (accountName == null) {
                accountName = "";
            }
            dataJson.put("id", this.snowflake.nextId());
            dataJson.put("oriName", oriName);
            dataJson.put("filePath", filePath);
            dataJson.put("suffix", suffix);
            dataJson.put("fileName", serverName);
            dataJson.put("fileUrl", fileUrl);
            dataJson.put("serverPath", savePath);
            dataJson.put("thumb", thumbStr);
            dataJson.put("fileSize", fileSize);
            dataJson.put("uploadTime", DateUtils.getNowDateTime());
            dataJson.put("account", accountName);
            dataJson.put("remarks", remarks);
            dataJson.put("uploadChannel", uploadChannel);
            SysFiles sysFiles = JsonUtils.jsonObject2Domain(dataJson, SysFiles.class);
            this.sysFilesMapper.add(sysFiles);
        }
        return uploadRes;
    }
}
