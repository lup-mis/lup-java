/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysDict;
import com.lup.system.sys.mapper.SysDictDataMapper;
import com.lup.system.sys.mapper.SysDictMapper;
import com.lup.system.sys.service.SysDictService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@AllArgsConstructor
@Service
public class SysDictServiceImpl implements SysDictService {
    private final SysDictMapper mapper;
    private final SysDictDataMapper sysDictDataMapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysDict> lists(JSONObject dataJson) {
        return mapper.lists(dataJson);
    }

    @Override
    public SysDict query(String id) {
        return mapper.query(id);
    }

    @Transactional
    @Override
    public int del(JSONObject dataJson) {
        String id = JsonUtils.getString("ids", dataJson);
        String dictCode = this.mapper.query(id).getDictCode();
        this.sysDictDataMapper.delByDictCode(dictCode);
        int num = mapper.del(id);
        setCacheComponent.setCache("sys_dict");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }

    @Override
    public int edit(SysDict sysDict) {
        String oldDictCode = this.mapper.query(sysDict.getId()).getDictCode();
        String dictCode = sysDict.getDictCode();
        int num = mapper.edit(sysDict);
        if (num == 0) throw new LupException("您未修改任何数据");
        //修改数据字典里面的数据（根据code）
        sysDictDataMapper.editDictDataCode(dictCode, oldDictCode);
        setCacheComponent.setCache("sys_dict");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }

    @Override
    public int add(SysDict sysDict) {
        sysDict.setId(this.snowflake.nextId());
        int num = mapper.add(sysDict);
        if (num == 0) ResultUtils.fail("添加失败");
        setCacheComponent.setCache("sys_dict");
        setCacheComponent.setCache("sys_dict_data");
        return num;
    }
}
