/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysRole;
import com.lup.system.sys.domain.SysRolePageAction;
import com.lup.system.sys.domain.SysRoleTree;
import com.lup.system.sys.mapper.SysAccountMapper;
import com.lup.system.sys.mapper.SysRoleMapper;
import com.lup.system.sys.mapper.SysRolePageActionMapper;
import com.lup.system.sys.mapper.SysRoleTreeMapper;
import com.lup.system.sys.service.SysRoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Service
public class SysRoleServiceImpl implements SysRoleService {
    private final SysRoleMapper mapper;
    private final SysAccountMapper sysAccountMapper;
    private final SysRoleTreeMapper sysRoleTreeMapper;
    private final SysRolePageActionMapper sysRolePageActionMapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysRole> lists(JSONObject dataJson) {
        return this.mapper.lists(dataJson);
    }

    @Override
    public SysRole query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(JSONObject idsJson) {
        String id = JsonUtils.getString("ids",idsJson);
        System.out.println("id="+id);
        // 删除前需要判断数据库是否还有账号
        if (this.sysAccountMapper.queryByRoleId(id).size() > 0) throw new LupException("该角色下还存在账号，请先删除或编辑该角色的账号!");
        int num = mapper.del(id);
        if (num == 0) throw new LupException("数据不存在，请刷新");
        sysRoleTreeMapper.delByRoleId(id);
        sysRolePageActionMapper.delByRoleId(id);
        setCacheComponent.setCache("sys_role");
        setCacheComponent.setCache("sys_role_page_action");
        setCacheComponent.setCache("sys_role_tree");
        return num;
    }

    @Override
    public int edit(JSONObject dataJson) {
        SysRole sysRole = JSONObject.parseObject(dataJson.toJSONString(), SysRole.class);
        int num = mapper.edit(sysRole);
        String roleId = dataJson.getString("id");
        //添加之前先删除
        sysRoleTreeMapper.delByRoleId(roleId);
        sysRolePageActionMapper.delByRoleId(roleId);

        //添加菜单表
        String treeIds = dataJson.getString("treeIds");
        if (treeIds != null && !treeIds.equals("")) {
            String[] treeIdsList;
            treeIdsList = treeIds.split(",");
            int i, n = treeIdsList.length;
            for (i = 0; i < n; i++) {
                SysRoleTree sysRoleTree = new SysRoleTree();
                sysRoleTree.setId(this.snowflake.nextId());
                sysRoleTree.setTreeId(treeIdsList[i]);
                sysRoleTree.setRoleId(dataJson.getString("id"));
                //添加
                this.sysRoleTreeMapper.add(sysRoleTree);
            }
        }
        //添加功能权限表
        String pageIds = dataJson.getString("pageIds");
        if (pageIds != null && !pageIds.equals("")) {
            JSONObject jsonObject = JSONObject.parseObject(pageIds);
            jsonObject.forEach((key, value) -> {
                JSONArray actionIdsArr = (JSONArray) JSON.parse(value.toString());
                String[] _actionIds = new String[actionIdsArr.size()];
                for (int i = 0; i < actionIdsArr.size(); i++) {
                    _actionIds[i] = actionIdsArr.get(i).toString();
                }
                String actionIds = String.join(",", _actionIds);
                //开始插入数据库
                SysRolePageAction sysRolePageAction = new SysRolePageAction();
                sysRolePageAction.setId(this.snowflake.nextId());
                sysRolePageAction.setRoleId(roleId);
                sysRolePageAction.setPageId(key);
                sysRolePageAction.setActionIds(actionIds);
                sysRolePageActionMapper.add(sysRolePageAction);
            });
        }
        setCacheComponent.setCache("sys_role");
        setCacheComponent.setCache("sys_role_page_action");
        setCacheComponent.setCache("sys_role_tree");
        return num;
    }

    @Override
    public int add(JSONObject dataJson) {
        String roleId = this.snowflake.nextId();
        dataJson.put("id", roleId);
        SysRole sysRole = JSONObject.parseObject(dataJson.toJSONString(), SysRole.class);
        int addNum = mapper.add(sysRole);
        if (addNum == 0) {
            throw new LupException("添加失败");
        }

        //添加菜单表
        String treeIds = dataJson.getString("treeIds");
        if (treeIds != null && !treeIds.equals("")) {
            String[] treeIdsList;
            treeIdsList = treeIds.split(",");
            int i, n = treeIdsList.length;
            for (i = 0; i < n; i++) {
                SysRoleTree sysRoleTree = new SysRoleTree();
                sysRoleTree.setId(this.snowflake.nextId());
                sysRoleTree.setTreeId(treeIdsList[i]);
                sysRoleTree.setRoleId(dataJson.getString("id"));
                //添加
                this.sysRoleTreeMapper.add(sysRoleTree);
            }
        }
        //添加功能权限表

        String pageIds = dataJson.getString("pageIds");

        if (pageIds != null && !pageIds.equals("")) {
            JSONObject jsonObject = JSONObject.parseObject(pageIds);
            for (Map.Entry<String, Object> stringObjectEntry : jsonObject.entrySet()) {
                String pageId = stringObjectEntry.getKey();
                JSONArray actionIdsArr = (JSONArray) JSON.parse(stringObjectEntry.getValue().toString());
                String[] _actionIds = new String[actionIdsArr.size()];
                for (int i = 0; i < actionIdsArr.size(); i++) {
                    _actionIds[i] = actionIdsArr.get(i).toString();
                }
                String actionIds = String.join(",", _actionIds);
                //开始插入数据库
                SysRolePageAction sysRolePageAction = new SysRolePageAction();
                sysRolePageAction.setId(this.snowflake.nextId());
                sysRolePageAction.setRoleId(roleId);
                sysRolePageAction.setPageId(pageId);
                sysRolePageAction.setActionIds(actionIds);
                sysRolePageActionMapper.add(sysRolePageAction);
            }
        }
        setCacheComponent.setCache("sys_role");
        setCacheComponent.setCache("sys_role_page_action");
        setCacheComponent.setCache("sys_role_tree");
        return addNum;
    }
}
