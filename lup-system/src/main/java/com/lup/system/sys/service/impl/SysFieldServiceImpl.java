/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.LupConfig;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.core.utils.Snowflake;
import com.lup.core.utils.Utils;
import com.lup.system.component.BuildCodeComponent;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysAction;
import com.lup.system.sys.domain.SysField;
import com.lup.system.sys.domain.SysPage;
import com.lup.system.sys.domain.SysTable;
import com.lup.system.sys.mapper.*;
import com.lup.system.sys.service.SysFieldService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Service
public class SysFieldServiceImpl implements SysFieldService {
    private final SysFieldMapper mapper;
    private final MySqlMapper mySqlMapper;
    private final SysTableMapper sysTableMapper;
    private final LupConfig lupConfig;
    private final Snowflake snowflake;
    private final BuildCodeComponent buildCode;
    private final SysPageMapper sysPageMapper;
    private final SysActionMapper sysActionMapper;
    private final SetCacheComponent setCacheComponent;

    @Override
    public List<SysField> lists(JSONObject dataJson) {
        return mapper.lists(dataJson);
    }

    @Override
    public SysField query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(JSONObject dataJson) {
        String id = JsonUtils.getString("ids", dataJson, "0");
        return mapper.del(id);
    }

    @Override
    public int edit(JSONObject dataJson) {
        SysField sysField = JSONObject.parseObject(dataJson.toJSONString(), SysField.class);
        try {
            // 判断是否重复
            List<?> rows = mapper.getByTableNameFieldName(dataJson.getString("tableName"), dataJson.getString("fieldName"), dataJson.getString("id"));
            if (rows.size() > 0) {
                throw new LupException("同一张表中字段不能重复");
            }
            mapper.edit(sysField);
        } catch (Exception e) {
            throw new LupException("修改失败");
        }
        try {
            mySqlMapper.editColumn(dataJson);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new LupException("修改成功，字段修改失败，请手动修改数据库字段");
        }
    }

    @Transactional
    @Override
    public int add(JSONObject dataJson) {
        dataJson.put("id", this.snowflake.nextId());
        SysField sysField = JSONObject.parseObject(dataJson.toJSONString(), SysField.class);
        // 判断是否重复
        List<?> rows = mapper.getByTableNameFieldName(dataJson.getString("tableName"), dataJson.getString("fieldName"), null);
        if (rows.size() > 0) {
            throw new LupException("同一张表中字段不能重复");
        }
        try {
            if (mapper.add(sysField) == 0) {
                throw new LupException("添加失败");
            }
        } catch (Exception e) {
            throw new LupException("添加失败");
        }
        try {
            mySqlMapper.addColumn(dataJson);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw new LupException("添加成功，字段创建失败，请手动创建");
        }
    }

    @Override
    public JSONObject getTemplate(String tableName) {
        return this.buildCode.getTemplate(tableName);
    }

    @Override
    public JSONObject syncCode(JSONObject dataJson) {
        String tableName = JsonUtils.getString("tableName", dataJson, "");
        String actions = JsonUtils.getString("actions", dataJson, "");
        String pages = JsonUtils.getString("pages", dataJson, "");
        String[] pagesArray = pages.split(",");
        JSONObject template = buildCode.getTemplate(tableName).getJSONObject("data");
        String domainTpl = template.getString("domain");
        String controllerTpl = template.getString("controller");
        String serviceTpl = template.getString("service");
        String serviceImplTpl = template.getString("serviceImpl");
        String mapperTpl = template.getString("mapper");
        String xmlTpl = template.getString("xml");
        String indexTpl = template.getString("index");
        String addTpl = template.getString("add");
        String editTpl = template.getString("edit");

        String prefix = tableName.split("_")[0];
        String htmlPath = Utils.lcFirst(Utils.lineToHump(tableName.substring(prefix.length())));

//        String smallHumpTableName = Utils.lineToHump(tableName);
        String domain = Utils.ucFirst(Utils.lineToHump(tableName));
//        String domainName = Utils.lcFirst(Utils.lineToHump(tableName));

        if (prefix.equals("sys")) {
            return ResultUtils.fail("系统表不允许同步！");
        }
        String controllerPath = lupConfig.getDevJavaWorkspace() + "lup-admin/src/main/java/com/lup/admin/controller/" + prefix + "/";
        if (!new File(lupConfig.getDevJavaWorkspace()).exists()) {
            return ResultUtils.fail("写入失败，请确认当前是否开发环境！");
        }
        String fileSystemPath = lupConfig.getDevJavaWorkspace() + "lup-system/src/main/java/com/lup/system/" + prefix + "/";
        String xmlPath = lupConfig.getDevJavaWorkspace() + "lup-system/src/main/resources/mapper/" + prefix + "/";
        String devHtmlWorkspace = lupConfig.getDevHtmlWorkspace() + "pages/" + prefix + "/" + htmlPath + "/";
        if (!new File(xmlPath).exists()) {
            new File(xmlPath).mkdirs();
        }
        if (!new File(devHtmlWorkspace).exists()) {
            new File(devHtmlWorkspace).mkdirs();
        }
        List<String> msg = new ArrayList<>();

        String domainPath = fileSystemPath + "/domain/";

        if (!new File(domainPath).exists()) {
            new File(domainPath).mkdirs();
        }

        if (!new File(controllerPath).exists()) {
            new File(controllerPath).mkdirs();
        }

        String serviceImplPath = fileSystemPath + "/service/impl/";
        if (!new File(serviceImplPath).exists()) {
            new File(serviceImplPath).mkdirs();
        }

        String mapperPath = fileSystemPath + "/mapper/";
        if (!new File(mapperPath).exists()) {
            new File(mapperPath).mkdirs();
        }
        String _tmpContent;
        if (Arrays.asList(pagesArray).contains("Domain.java")) {
            //domain
            _tmpContent = Utils.readFile(fileSystemPath + "/domain/" + domain + ".java");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + ".java】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(fileSystemPath + "/domain/" + domain + ".java", domainTpl);
                msg.add("<span style='color:green'>【" + domain + ".java】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("Controller.java")) {
            //Controller
            _tmpContent = Utils.readFile(controllerPath + domain + "Controller.java");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + "Controller.java】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(controllerPath + domain + "Controller.java", controllerTpl);
                msg.add("<span style='color:green'>【" + domain + "Controller.java】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("Service.java")) {
            //Service
            _tmpContent = Utils.readFile(fileSystemPath + "/service/" + domain + "Service.java");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + "Service.java】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(fileSystemPath + "/service/" + domain + "Service.java", serviceTpl);
                msg.add("<span style='color:green'>【" + domain + "Service.java】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("ServiceImpl.java")) {
            //ServiceImpl
            _tmpContent = Utils.readFile(fileSystemPath + "/service/impl/" + domain + "ServiceImpl.java");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + "ServiceImpl.java】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(fileSystemPath + "/service/impl/" + domain + "ServiceImpl.java", serviceImplTpl);
                msg.add("<span style='color:green'>【" + domain + "ServiceImpl.java】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("Mapper.java")) {
            //Mapper
            _tmpContent = Utils.readFile(fileSystemPath + "/mapper/" + domain + "Mapper.java");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + "Mapper.java】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(fileSystemPath + "/mapper/" + domain + "Mapper.java", mapperTpl);
                msg.add("<span style='color:green'>【" + domain + "Mapper.java】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("Mapper.xml")) {
            //xml
            _tmpContent = Utils.readFile(xmlPath + domain + "Mapper.xml");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【" + domain + "Mapper.xml】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(xmlPath + domain + "Mapper.xml", xmlTpl);
                msg.add("<span style='color:green'>【" + domain + "Mapper.xml】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("index.html")) {
            //index
            _tmpContent = Utils.readFile(devHtmlWorkspace + "index.html");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【index.html】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(devHtmlWorkspace + "index.html", indexTpl);
                msg.add("<span style='color:green'>【index.html】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("add.html")) {
            //add
            _tmpContent = Utils.readFile(devHtmlWorkspace + "add.html");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【add.html】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(devHtmlWorkspace + "add.html", addTpl);
                msg.add("<span style='color:green'>【add.html】写入成功！</span><br>");
            }
        }
        if (Arrays.asList(pagesArray).contains("edit.html")) {
            //edit
            _tmpContent = Utils.readFile(devHtmlWorkspace + "edit.html");
            if (_tmpContent.contains("[LupMisAllowedSync-lock]")) {
                msg.add("<span style='color:red'>【edit.html】 写入失败，[LupMisAllowedSync-lock]！</span><br>");
            } else {
                Utils.write(devHtmlWorkspace + "edit.html", editTpl);
                msg.add("<span style='color:green'>【edit.html】写入成功！</span><br>");
            }
        }
        //判断是否同步页面

        if (StringUtils.isNotEmpty(actions)) {
            //开始初始化操作功能
            String[] actionsArray = actions.split(",");
            //先添加页面,判断该页面是否存在
            String pageCode = prefix + ":" + htmlPath + ":lists";
            SysPage sysPage = this.sysPageMapper.queryByCode(pageCode);
            String pageId = this.snowflake.nextId();
            SysTable tableRes = sysTableMapper.getByTableName(tableName, null);
            String tableRemarks = tableRes.getTableRemarks();
            int pageAddNum = 1;
            if (sysPage != null) {
                pageId = sysPage.getId();
                msg.add("<span style='color:red'>页面接口初始化失败（已经存在）</span><br>");
            } else {
                //开始同步页面
                SysPage _sysPage = new SysPage();
                _sysPage.setId(pageId);
                _sysPage.setPageName(tableRemarks);
                _sysPage.setTableName(tableName);
                _sysPage.setCode(pageCode);
                _sysPage.setJump("pages/" + prefix + "/" + htmlPath + "/index.html");
                _sysPage.setQueryUrl(prefix + "/" + htmlPath + "/lists");
                _sysPage.setApiUrl("#");
                _sysPage.setStatus(1);
                _sysPage.setSort(255);
                _sysPage.setRemarks("");
                _sysPage.setPageType(tableRes.getTableType());
                pageAddNum = this.sysPageMapper.add(_sysPage);
                if (pageAddNum > 0) {
                    msg.add("<span style='color:green'>页面接口同步成功</span><br>");
                } else {
                    msg.add("<span style='color:red'>页面接口初始化失败（添加失败）</span><br>");
                }
            }
            if (pageAddNum > 0) {
                //开始同步操作功能
                for (String action : actionsArray) {
                    String actionCode = prefix + ":" + htmlPath + ":" + action;
                    SysAction actionRs = this.sysActionMapper.getByPageIdActionCode(pageId, actionCode);
                    switch (action) {
                        case "add":
                            if (actionRs != null) {
                                msg.add("<span style='color:red'>添加功能同步失败（已经存在）</span><br>");
                            } else {
                                //开始入库
                                SysAction _sysAction = new SysAction();
                                _sysAction.setId(this.snowflake.nextId());
                                _sysAction.setPageId(pageId);
                                _sysAction.setActionName("添加");
                                _sysAction.setTableName(tableName);
                                _sysAction.setCode(actionCode);
                                _sysAction.setBindEvent(action);
                                _sysAction.setTips("添加" + tableRemarks);
                                _sysAction.setBgColor("#009688");
                                _sysAction.setTargetType("openFrame");
                                _sysAction.setWidth("100%");
                                _sysAction.setHeight("100%");
                                _sysAction.setJump("add.html");
                                _sysAction.setQueryUrl("#");
                                _sysAction.setApiUrl(prefix + "/" + htmlPath + "/" + action);
                                _sysAction.setStatus(1);
                                _sysAction.setSort(255);
                                int add = this.sysActionMapper.add(_sysAction);
                                if (add > 0) {
                                    msg.add("<span style='color:green'>添加功能同步成功</span><br>");
                                } else {
                                    msg.add("<span style='color:red'>添加功能同步失败</span><br>");
                                }
                            }
                            break;
                        case "del":
                            if (actionRs != null) {
                                msg.add("<span style='color:red'>删除初始化失败（已经存在）</span><br>");
                            } else {
                                //开始入库
                                SysAction _sysAction = new SysAction();
                                _sysAction.setId(this.snowflake.nextId());
                                _sysAction.setPageId(pageId);
                                _sysAction.setActionName("删除");
                                _sysAction.setTableName(tableName);
                                _sysAction.setCode(actionCode);
                                _sysAction.setBindEvent(action);
                                _sysAction.setTips("确定要删除选中的数据吗?");
                                _sysAction.setBgColor("#ff5722");
                                _sysAction.setTargetType("doApi");
                                _sysAction.setWidth("100%");
                                _sysAction.setHeight("100%");
                                _sysAction.setJump("#");
                                _sysAction.setQueryUrl("#");
                                _sysAction.setApiUrl(prefix + "/" + htmlPath + "/" + action);
                                _sysAction.setStatus(1);
                                _sysAction.setSort(255);
                                int add = this.sysActionMapper.add(_sysAction);
                                if (add > 0) {
                                    msg.add("<span style='color:green'>删除功能同步成功</span><br>");
                                } else {
                                    msg.add("<span style='color:red'>删除功能同步失败</span><br>");
                                }
                            }
                            break;
                        case "edit":
                            if (actionRs != null) {
                                msg.add("<span style='color:red'>编辑初始化失败（已经存在）</span><br>");
                            } else {
                                //开始入库
                                SysAction _sysAction = new SysAction();
                                _sysAction.setId(this.snowflake.nextId());
                                _sysAction.setPageId(pageId);
                                _sysAction.setActionName("编辑");
                                _sysAction.setTableName(tableName);
                                _sysAction.setCode(actionCode);
                                _sysAction.setBindEvent(action);
                                _sysAction.setTips("编辑" + tableRemarks);
                                _sysAction.setBgColor("#1e9fff");
                                _sysAction.setTargetType("openFrame");
                                _sysAction.setWidth("100%");
                                _sysAction.setHeight("100%");
                                _sysAction.setJump("edit.html");
                                _sysAction.setQueryUrl(prefix + "/" + htmlPath + "/query");
                                _sysAction.setApiUrl(prefix + "/" + htmlPath + "/" + action);
                                _sysAction.setStatus(1);
                                _sysAction.setSort(255);
                                int add = this.sysActionMapper.add(_sysAction);
                                if (add > 0) {
                                    msg.add("<span style='color:green'>编辑功能同步成功</span><br>");
                                } else {
                                    msg.add("<span style='color:red'>编辑功能同步失败</span><br>");
                                }
                            }
                            break;
                    }
                }
            }
        }
        setCacheComponent.setCache("sys_action");
        setCacheComponent.setCache("sys_page");
        return ResultUtils.success(msg);
    }

}
