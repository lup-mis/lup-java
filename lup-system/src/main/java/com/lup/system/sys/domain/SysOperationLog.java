/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-03-27 15:35:31; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
package com.lup.system.sys.domain; 
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date; 
import lombok.Data; 
 
@Data 
public class SysOperationLog { 

    //ID
    @NotEmpty(message = "ID不能为空")
    private String id;

    //账号
    @NotEmpty(message = "账号不能为空")
    private String accountName;

    //用户操作
    @NotEmpty(message = "用户操作不能为空")
    private String operation;

    //响应时间(毫秒)
    @NotNull(message = "响应时间(毫秒)不能为空")
    private Integer time;

    //请求方法
    @NotEmpty(message = "请求方法不能为空")
    private String method;

    //请求参数
    @NotEmpty(message = "请求参数不能为空")
    private String pars;

    //IP地址
    @NotEmpty(message = "IP地址不能为空")
    private String ip;

    //创建时间
    private Date createTime;
 
} 
