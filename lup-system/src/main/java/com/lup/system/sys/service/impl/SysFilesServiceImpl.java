/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:55:31;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.DateUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.sys.domain.SysFiles;
import com.lup.system.sys.mapper.SysFilesMapper;
import com.lup.system.sys.service.SysFilesService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class SysFilesServiceImpl implements SysFilesService {
    private final SysFilesMapper mapper;
    private final Snowflake snowflake;

    @Override
    public List<SysFiles> lists(JSONObject dataJson) {
        return this.mapper.lists(dataJson);
    }

    @Override
    public SysFiles query(String id) {
        return mapper.query(id);
    }

    @Override
    public int del(JSONObject idsJson) {
        String ids = idsJson.getString("ids");
        String[] strArray = ids.split(",");
        int num = 0;
        for (String id : strArray) {
            int delNum = this.mapper.del(id);
            num += delNum;
        }
        return num;
    }

    @Override
    public int edit(SysFiles sysFiles) {
        return this.mapper.edit(sysFiles);
    }

    @Override
    public int add(SysFiles sysFiles) {
        sysFiles.setId(this.snowflake.nextId());
        sysFiles.setUploadTime(DateUtils.getNowDate());
        return this.mapper.add(sysFiles);
    }
} 
