/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.SetCacheComponent;
import com.lup.system.sys.domain.SysAction;
import com.lup.system.sys.domain.SysPage;
import com.lup.system.sys.domain.SysRolePageAction;
import com.lup.system.sys.domain.vo.SysActionVo;
import com.lup.system.sys.domain.vo.SysPageVo;
import com.lup.system.sys.mapper.SysActionMapper;
import com.lup.system.sys.mapper.SysPageMapper;
import com.lup.system.sys.mapper.SysRolePageActionMapper;
import com.lup.system.sys.service.SysPageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Service
public class SysPageServiceImpl implements SysPageService {
    private final SysPageMapper mapper;
    private final SysActionMapper sysActionMapper;
    private final Snowflake snowflake;
    private final SetCacheComponent setCacheComponent;
    private final SysRolePageActionMapper sysRolePageActionMapper;

    @Override
    public List<SysPageVo> lists(JSONObject dataJson) {
        List<SysPageVo> list = mapper.lists(dataJson);
        String roleId = JsonUtils.getString("roleId", dataJson, "");
        //根据角色查询所有sys_role_page_action
        List<SysRolePageAction> sysRolePageActionList = sysRolePageActionMapper.queryByRoleId(roleId);
        String[] pageIdArray = new String[sysRolePageActionList.size()];
        List<String> actionIdsArray = new ArrayList<>();
        int i = 0;
        for (SysRolePageAction item : sysRolePageActionList) {
            pageIdArray[i] = item.getPageId();
            String actionIds = item.getActionIds();
            String[] _actionIdsArray = actionIds.split(",");
            for (String id : _actionIdsArray) {
                if (!id.equals("auth")) actionIdsArray.add(id);
            }
            i++;
        }
        //开始查询该页面的操作功能
        for (SysPageVo item : list) {
            item.setChecked(Arrays.asList(pageIdArray).contains(item.getId()));
            //查询该页面的操作功能
            //先添加一个页面授权功能
            List<SysActionVo> sysActionVos = new LinkedList<>();
            SysActionVo authActionVo = new SysActionVo();
            authActionVo.setId(item.getId() + "@auth");
            authActionVo.setActionName("接口授权");
            authActionVo.setCode(item.getCode());
            authActionVo.setApiUrl(item.getApiUrl());
            //两种方法判断数组是否包含（第一种方法）
            authActionVo.setChecked(Arrays.asList(pageIdArray).contains(item.getId()));
            authActionVo.setSort(0);
            authActionVo.setStatus(1);
            sysActionVos.add(authActionVo);
            //设置操作权限
            List<SysAction> sysAction = sysActionMapper.getByPageId(item.getId(), 1);
            for (SysAction _item : sysAction) {
                SysActionVo authActionVoF = new SysActionVo();
                authActionVoF.setId(item.getId() + "@" + _item.getId());
                authActionVoF.setActionName(_item.getActionName());
                authActionVoF.setCode(_item.getCode());
                authActionVoF.setApiUrl(_item.getApiUrl());
                //两种方法判断数组是否包含（第二种方法）
                authActionVoF.setChecked(actionIdsArray.contains(_item.getId()));
                authActionVoF.setSort(_item.getSort());
                authActionVoF.setStatus(_item.getSort());
                sysActionVos.add(authActionVoF);
            }
            item.setSysAction(sysActionVos);
        }
        return list;
    }

    @Override
    public SysPageVo query(String id) {
        return mapper.query(id);
    }


    @Transactional
    @Override
    public int del(JSONObject idsJson) {
        //删除操作按钮
        sysActionMapper.delByPageId(JsonUtils.getString("ids", idsJson));
        int num = mapper.del(JsonUtils.getString("ids", idsJson));
        if (num > 0) {
            setCacheComponent.setCache("sys_page");
            setCacheComponent.setCache("sys_action");
        }
        return num;
    }

    @Override
    public int edit(SysPage sysPage) {
        int num = mapper.edit(sysPage);
        if (num == 0) throw new LupException("您未修改任何数据");
        setCacheComponent.setCache("sys_page");
        return num;
    }

    @Override
    public int add(SysPage sysPage) {
        sysPage.setId(this.snowflake.nextId());
        int num = mapper.add(sysPage);
        if (num == 0) throw new LupException("添加失败");
        setCacheComponent.setCache("sys_page");
        return num;
    }

    @Override
    public String initBtn(JSONObject data) {
        String pageId = data.getString("pageId");
        SysPage sysPage = this.mapper.query(pageId);
        String tableName = sysPage.getTableName();
        String pageName = sysPage.getPageName();
        String pageCode = sysPage.getCode();
        String[] pageCodeArray = pageCode.split(":");
        String codePre = pageCodeArray[0] + ":" + pageCodeArray[1];
        String actions = data.getString("actions");
        String[] actionsArr = actions.split(",");
        StringBuilder msg = new StringBuilder();
        for (String s : actionsArr) {
            //开始初始化操作功能
            String actionName = "";
            String code = codePre + ":" + s;
            String bindEvent = "";
            String tips = "";
            String bgColor = "";
            String targetType = "";
            String width = "100%";
            String height = "100%";
            String jump = "#";
            String queryUrl = "#";
            String apiUrl = code.replace(":", "/");
            int status = 1;
            int sort = 255;
            switch (s) {
                case "add" -> {
                    actionName = "添加";
                    bindEvent = "add";
                    tips = "添加" + pageName;
                    bgColor = "#009688";
                    targetType = "openFrame";
                    jump = "add.html";
                }
                case "del" -> {
                    actionName = "删除";
                    bindEvent = "del";
                    tips = "确定要删除选中的数据吗？";
                    bgColor = "#ff5722";
                    targetType = "doApi";
                }
                case "edit" -> {
                    actionName = "编辑";
                    bindEvent = "edit";
                    tips = "编辑" + pageName;
                    bgColor = "#1e9fff";
                    targetType = "openFrame";
                    jump = "edit.html";
                    queryUrl = codePre.replace(":", "/") + "/query";
                }
            }
            //判断数据库是否存在该编码
            SysAction sysAction = this.sysActionMapper.queryByCode(code);
            if (sysAction != null) {
                msg.append("<font style='color:red'>【").append(actionName).append("】功能初始化失败</font><br>");
            } else {
                msg.append("<font style='color:green'>【").append(actionName).append("】功能初始化成功</font><br>");
                JSONObject json = new JSONObject(true);
                json.put("id", this.snowflake.nextId());
                json.put("pageId", pageId);
                json.put("actionName", actionName);
                json.put("tableName", tableName);
                json.put("code", code);
                json.put("bindEvent", bindEvent);
                json.put("tips", tips);
                json.put("bgColor", bgColor);
                json.put("targetType", targetType);
                json.put("width", width);
                json.put("height", height);
                json.put("jump", jump);
                json.put("queryUrl", queryUrl);
                json.put("apiUrl", apiUrl);
                json.put("status", status);
                json.put("sort", sort);
                this.sysActionMapper.add(JSONObject.parseObject(json.toJSONString(), SysAction.class));
            }
        }
        //从新给root 赋予最大权限#TODO
        setCacheComponent.setCache("sys_action");
        return msg.toString();
    }

}
