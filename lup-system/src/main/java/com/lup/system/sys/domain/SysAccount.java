/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.domain;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Data
public class SysAccount {
    private String id;
    @NotBlank(message = "账号不能为空")
    private String accountName;
    private String password;
    private String fullName;
    private String sex;
    private String email;
    private String phone;
    private String fixedTelephone;
    private String safeIps;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JSONField(name="createTime",format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String orgId;
    private SysOrg orgRes;

    private Integer status;
    @NotBlank(message = "角色不能为空!")
    private String roleIds;
    private List<SysRole> roleRes;

    private JSONObject accountToken;

}
