/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 04:20:44;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.LupConfig;
import com.lup.core.utils.*;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.sys.domain.SysAccount;
import com.lup.system.sys.domain.SysAccountLoginLog;
import com.lup.system.sys.mapper.SysAccountLoginLogMapper;
import com.lup.system.sys.mapper.SysAccountMapper;
import com.lup.system.sys.service.SysLoginService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

@AllArgsConstructor
@Service
public class SysLoginServiceImpl implements SysLoginService {
    private final RedisUtils redisUtils;
    private final GetCacheComponent getCacheComponent;
    private final SysAccountLoginLogMapper sysAccountLoginLogMapper;
    private final Snowflake snowflake;
    private final SysAccountMapper sysAccountMapper;
    private final LupConfig lupConfig;


    @Override
    public JSONObject login(JSONObject dataJson) {
        String accountName = JsonUtils.getString("accountName", dataJson);
        String password = JsonUtils.getString("password", dataJson);
        String verifyCode = JsonUtils.getString("verifyCode", dataJson);
        String verifyCodeToken = JsonUtils.getString("verifyCodeToken", dataJson);
        if (StringUtils.isEmpty(accountName)) return ResultUtils.fail("登录账号不能为空");
        if (StringUtils.isEmpty(password)) return ResultUtils.fail("登录密码不能为空");
        if (StringUtils.isEmpty(verifyCode)) return ResultUtils.fail("验证码不能为空");
        if (StringUtils.isEmpty(verifyCodeToken)) return ResultUtils.fail("验证码错误.");
        //先校验验证码
        String redisVerifyCode = redisUtils.get(verifyCodeToken);
        if (StringUtils.isEmpty(redisVerifyCode)) return ResultUtils.fail("验证码错误。");
        if (!redisVerifyCode.equals(verifyCode)) return ResultUtils.fail("验证码错误！");

        //先查询登录错误的次数
        JSONObject sysConfigJson = getCacheComponent.config();
        String safeIps = JsonUtils.getString("safeIps", sysConfigJson);
        int loginErrorLock = JsonUtils.getInt("loginErrorLock", sysConfigJson, 1);
        int loginErrorNum = JsonUtils.getInt("loginErrorNum", sysConfigJson, 3);
        int lockTime = JsonUtils.getInt("lockTime", sysConfigJson, 300);
        assert safeIps != null;
        String userIp = IpUtils.getIPAddress();
        safeIps = safeIps.replace("|", ",");
        if (!safeIps.equals("0.0.0.0")) {
            //开始校验ip
            String[] safeIpsArr = safeIps.split(",");
            boolean contains = Arrays.asList(safeIpsArr).contains(userIp);
            if (!contains) {
                return ResultUtils.fail("非法登录");
            }
        }
        if (loginErrorLock == 1) {
            //获取登录log
            List<SysAccountLoginLog> loginLogList = this.sysAccountLoginLogMapper.queryByIp(userIp, loginErrorNum);
            int errNum = 0;
            int lastLoginTime = 0;
            int i = 0, n = loginLogList.size();
            while (i < n) {
                if (loginLogList.get(i).getStatus() == -1) {
                    errNum += 1;
                }
                if (i == 0) {
                    lastLoginTime = (int) (loginLogList.get(0).getLoginTime().getTime() / 1000);
                }
                i++;
            }
            if (errNum >= loginErrorNum) {
                int diffTime = DateUtils.getTimeStampSecond() - lastLoginTime;
                if ((lockTime * 60) >= diffTime) {
                    return ResultUtils.fail("账号密码输入次数过多，请" + lockTime + "分钟后再试！");
                }
            }
        }
        //开始记录登录日志
        JSONObject loginLogJson = new JSONObject(true);
        loginLogJson.put("id", this.snowflake.nextId());
        loginLogJson.put("accountName", accountName);
        loginLogJson.put("password", password.charAt(0) + "****" + password.substring(password.length() - 1));
        loginLogJson.put("ip", IpUtils.getIPAddress());
        loginLogJson.put("browser", Utils.getBrowser());
        loginLogJson.put("loginTime", DateUtils.getNowDateTime());
        loginLogJson.put("userAgent", Utils.getUserAgent());
        loginLogJson.put("status", 1);
        loginLogJson.put("remarks", "登录成功");
        SysAccount sysAccount = this.sysAccountMapper.queryByAccountName(accountName);
        if (sysAccount == null) {
            loginLogJson.put("remarks", "账号或密码错误！");
            loginLogJson.put("status", -1);
            this.sysAccountLoginLogMapper.add(JSONObject.parseObject(loginLogJson.toJSONString(), SysAccountLoginLog.class));
            return ResultUtils.fail("账号或密码错误！");
        }

        String dbPassword = sysAccount.getPassword();
        if (!Utils.password(password).equals(dbPassword)) {
            loginLogJson.put("remarks", "账号或密码错误.");
            loginLogJson.put("status", -1);
            this.sysAccountLoginLogMapper.add(JSONObject.parseObject(loginLogJson.toJSONString(), SysAccountLoginLog.class));
            return ResultUtils.fail("账号或密码错误.");
        }
        if (sysAccount.getStatus() == -1) {
            loginLogJson.put("remarks", "该账号已被禁用，请联系管理员！");
            loginLogJson.put("status", -1);
            this.sysAccountLoginLogMapper.add(JSONObject.parseObject(loginLogJson.toJSONString(), SysAccountLoginLog.class));
            return ResultUtils.fail("该账号已被禁用，请联系管理员！");
        }
        //生成token
        String token = Utils.md5(Utils.uuid() + sysAccount.getId());
        int expireTime = DateUtils.getTimeStampSecond() + lupConfig.getTokenExpireTime();
        JSONObject accountTokenJson = new JSONObject(true);
        accountTokenJson.put("accountId", sysAccount.getId());
        accountTokenJson.put("accountName", sysAccount.getAccountName());
        accountTokenJson.put("token", token);
        accountTokenJson.put("expireTime", expireTime);
        accountTokenJson.put("ip", IpUtils.getIPAddress());
        accountTokenJson.put("userAgent", Utils.getUserAgent());
        //写入redis
        redisUtils.add("token:admin:web:" + sysAccount.getId(), JSON.toJSONString(accountTokenJson), lupConfig.getTokenExpireTime(), TimeUnit.SECONDS);

        loginLogJson.put("remarks", "登录成功！");
        loginLogJson.put("status", 1);
        this.sysAccountLoginLogMapper.add(JSONObject.parseObject(loginLogJson.toJSONString(), SysAccountLoginLog.class));

        //返回客户端登录成功token
        JSONObject loginSuccessJson = new JSONObject(true);
        loginSuccessJson.put("uid", sysAccount.getId());
        loginSuccessJson.put("token", token);
        return ResultUtils.success("登录成功！", loginSuccessJson);
    }

    @Override
    public JSONObject logout(String accessUid,String terminal) {
        redisUtils.delete("token:admin:"+terminal+":" + accessUid);
        return ResultUtils.success("退出登录成功！");

    }
}
