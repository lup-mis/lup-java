/**
 * [LupMisAllowedSync-lock]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2022-03-27 15:29:18;
 * 版权所有 2020-2022 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.sys.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.config.exception.LupException;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.Snowflake;
import com.lup.system.component.DataComponent;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.sys.domain.SysAccountLoginLog;
import com.lup.system.sys.domain.SysDictData;
import com.lup.system.sys.mapper.SysAccountLoginLogMapper;
import com.lup.system.sys.service.SysAccountLoginLogService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@AllArgsConstructor
@Service
public class SysAccountLoginLogServiceImpl implements SysAccountLoginLogService {
    private final SysAccountLoginLogMapper mapper;
    private final Snowflake snowflake;
    private final GetCacheComponent getCacheComponent;

    @Override
    public List<SysAccountLoginLog> lists(JSONObject dataJson) {
        List<SysAccountLoginLog> list = this.mapper.lists(dataJson);
        //从redis获取数据字典数据;
        JSONArray sysDictData = getCacheComponent.getCache("sys_dict_data");
        for (SysAccountLoginLog item : list) {
            item.setStatusRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(item.getStatus().toString(), "common:success_fail", sysDictData), SysDictData.class));
        }
        return list;
    }

    @Override
    public SysAccountLoginLog query(String id) {
        SysAccountLoginLog sysAccountLoginLog = mapper.query(id);
        //从redis获取数据字典数据;
        JSONArray sysDictData = getCacheComponent.getCache("sys_dict_data");
        sysAccountLoginLog.setStatusRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(sysAccountLoginLog.getStatus().toString(), "common:success_fail", sysDictData), SysDictData.class));
        return sysAccountLoginLog;
    }

    @Override
    public int del(JSONObject idsJson) {
        String ids = idsJson.getString("ids");
        String[] strArray = ids.split(",");
        int num = 0;
        for (String id : strArray) {
            int del = this.mapper.del(id);
            num += del;
        }
        return num;
    }

    @Override
    public int edit(SysAccountLoginLog sysAccountLoginLog) {
        int num = this.mapper.edit(sysAccountLoginLog);
        if (num == 0) throw new LupException("您未修改任何数据");
        return num;
    }

    @Override
    public int add(SysAccountLoginLog sysAccountLoginLog) {
        sysAccountLoginLog.setId(this.snowflake.nextId());
        int num = this.mapper.add(sysAccountLoginLog);
        if (num == 0) throw new LupException("添加失败");
        return num;
    }

} 
