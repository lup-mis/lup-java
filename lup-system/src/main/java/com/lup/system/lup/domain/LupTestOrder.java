/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-12 09:45:24; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
 
package com.lup.system.lup.domain; 
import javax.validation.constraints.NotEmpty;
import java.util.List;
import com.lup.system.sys.domain.SysCities;
import com.lup.system.sys.domain.SysAreas;
import javax.validation.constraints.NotNull;
import com.lup.system.sys.domain.SysDictData;
import javax.validation.constraints.Size;
import com.lup.system.sys.domain.SysCatalog;
import com.lup.system.sys.domain.SysProvinces;
import java.util.Date; 
import lombok.Data; 
 
@Data 
public class LupTestOrder { 

    //ID
    private String id;

    //标题
    @Size(max = 50,message = "标题不能超过{max}个字符")
    @NotEmpty(message = "标题不能为空")
    private String title;

    //简介
    @NotEmpty(message = "简介不能为空")
    private String remarks;

    //内容
    @NotEmpty(message = "内容不能为空")
    private String content;

    //时间戳
    @NotNull(message = "时间戳不能为空")
    private Long timeStamp;

    //生日
    private String birthday;

    //订单类别
    @NotNull(message = "订单类别不能为空")
    private Integer orderType;
    private SysDictData orderTypeRes;

    //用户
    @Size(max = 50,message = "用户不能超过{max}个字符")
    @NotEmpty(message = "用户不能为空")
    private String userId;
    private List<LupTestUser> userIdRes;

    //订单标签
    @Size(max = 500,message = "订单标签不能超过{max}个字符")
    @NotEmpty(message = "订单标签不能为空")
    private String orderTags;
    private List<SysDictData> orderTagsRes;

    //产品分类
    private String goodsType;
    private List<SysCatalog> goodsTypeRes;

    //商品图片
    @Size(max = 500,message = "商品图片不能超过{max}个字符")
    @NotEmpty(message = "商品图片不能为空")
    private String goodsPhotos;

    //证据
    @Size(max = 500,message = "证据不能超过{max}个字符")
    @NotEmpty(message = "证据不能为空")
    private String certificatePhotos;

    //代理商
    @Size(max = 19,message = "代理商不能超过{max}个字符")
    @NotEmpty(message = "代理商不能为空")
    private String agentId;
    private LupTestAgent agentIdRes;

    //单附件(带描述)
    private String singleFileRemarks;

    //多附件(带描述)
    private String multiFileRemarks;

    //省份
    @Size(max = 50,message = "省份不能超过{max}个字符")
    @NotEmpty(message = "省份不能为空")
    private String provinceCode;
    private SysProvinces provinceCodeRes;

    //城市
    @Size(max = 50,message = "城市不能超过{max}个字符")
    @NotEmpty(message = "城市不能为空")
    private String cityCode;
    private SysCities cityCodeRes;

    //区县
    @Size(max = 50,message = "区县不能超过{max}个字符")
    @NotEmpty(message = "区县不能为空")
    private String areaCode;
    private SysAreas areaCodeRes;

    //创建时间
    private Date createTime;
 
} 
