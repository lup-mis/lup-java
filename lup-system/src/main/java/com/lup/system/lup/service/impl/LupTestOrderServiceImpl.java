/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-12 09:45:24; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
 
package com.lup.system.lup.service.impl; 
 
import java.util.List; 
import com.lup.core.utils.*; 
import org.springframework.stereotype.Service; 
import com.alibaba.fastjson.JSONObject; 
import com.lup.core.config.exception.LupException; 
import lombok.AllArgsConstructor; 
import com.lup.system.lup.domain.LupTestOrder; 
import com.lup.system.lup.mapper.LupTestOrderMapper; 
import com.lup.system.lup.service.LupTestOrderService; 
import com.lup.system.lup.mapper.LupTestAgentMapper;
import com.lup.system.lup.domain.LupTestUser;
import java.util.HashSet;
import com.lup.system.sys.mapper.SysAreasMapper;
import com.alibaba.fastjson.JSONArray;
import com.lup.system.sys.domain.SysCatalog;
import com.lup.system.sys.mapper.SysProvincesMapper;
import com.lup.system.sys.mapper.SysCitiesMapper;
import com.lup.system.sys.domain.SysCities;
import com.lup.system.sys.domain.SysAreas;
import com.lup.system.sys.domain.SysDictData;
import org.apache.commons.lang3.StringUtils;
import com.lup.system.component.GetCacheComponent;
import com.lup.system.lup.mapper.LupTestUserMapper;
import com.lup.system.lup.domain.LupTestAgent;
import com.lup.system.sys.domain.SysProvinces;
import com.lup.system.component.DataComponent;
import java.util.Set; 
 
@AllArgsConstructor 
@Service 
public class LupTestOrderServiceImpl implements LupTestOrderService { 
    private final LupTestOrderMapper mapper; 
    private final Snowflake snowflake; 
    private final LupTestUserMapper lupTestUserMapper;
    private final SysAreasMapper sysAreasMapper;
    private final LupTestAgentMapper lupTestAgentMapper;
    private final SysProvincesMapper sysProvincesMapper;
    private final SysCitiesMapper sysCitiesMapper;
    private final GetCacheComponent getCacheComponent; 
 
    @Override 
    public List<LupTestOrder> lists(JSONObject dataJson) { 
        List<LupTestOrder> list = this.mapper.lists(dataJson); 
        //取出数据库对应关系的字段的值（这里用逗号隔开）
        Set<String> agentIdsArray = new HashSet<>();
        Set<String> areaCodesArray = new HashSet<>();
        Set<String> provinceCodesArray = new HashSet<>();
        Set<String> cityCodesArray = new HashSet<>();
        Set<String> userIdsArray = new HashSet<>();
        for (LupTestOrder item : list) {
            agentIdsArray.add(item.getAgentId());
            areaCodesArray.add(item.getAreaCode());
            provinceCodesArray.add(item.getProvinceCode());
            cityCodesArray.add(item.getCityCode());
            userIdsArray.add(item.getUserId());
        }
        //从redis获取数据字典数据;
        JSONArray sysDictData = getCacheComponent.getCache("sys_dict_data");
        //从redis获取目录分类;
        JSONArray sysCatalog = getCacheComponent.getCache("sys_catalog");
        //将数据表lup_test_agent中根据id字段取出符合符合条件的数据
        List<LupTestAgent> lupTestAgentList = lupTestAgentMapper.listsIn("id",!agentIdsArray.isEmpty() ? StringUtils.join(agentIdsArray, ",") : "");
        //将数据表sys_areas中根据id字段取出符合符合条件的数据
        List<SysAreas> sysAreasList = sysAreasMapper.listsIn("area_code",!areaCodesArray.isEmpty() ? StringUtils.join(areaCodesArray, ",") : "");
        //将数据表sys_provinces中根据id字段取出符合符合条件的数据
        List<SysProvinces> sysProvincesList = sysProvincesMapper.listsIn("province_code",!provinceCodesArray.isEmpty() ? StringUtils.join(provinceCodesArray, ",") : "");
        //将数据表sys_cities中根据id字段取出符合符合条件的数据
        List<SysCities> sysCitiesList = sysCitiesMapper.listsIn("city_code",!cityCodesArray.isEmpty() ? StringUtils.join(cityCodesArray, ",") : "");
        //将数据表lup_test_user中根据id字段取出符合符合条件的数据
        List<LupTestUser> lupTestUserList = lupTestUserMapper.listsIn("id",!userIdsArray.isEmpty() ? StringUtils.join(userIdsArray, ",") : "");
        for (LupTestOrder item : list) {
            item.setOrderTypeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(item.getOrderType().toString(), "test_order:type", sysDictData), SysDictData.class));
            item.setOrderTagsRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayDictData(item.getOrderTags(), "order:tags", sysDictData), SysDictData.class));
            item.setGoodsTypeRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayCatalog(item.getGoodsType(), sysCatalog), SysCatalog.class));
            item.setAgentIdRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("id", item.getAgentId(), lupTestAgentList), LupTestAgent.class));
            item.setAreaCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("areaCode", item.getAreaCode(), sysAreasList), SysAreas.class));
            item.setProvinceCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("provinceCode", item.getProvinceCode(), sysProvincesList), SysProvinces.class));
            item.setCityCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("cityCode", item.getCityCode(), sysCitiesList), SysCities.class));
            item.setUserIdRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayDbField("id", item.getUserId(), lupTestUserList), LupTestUser.class));
        }
 
        return list; 
    } 
 
    @Override 
    public LupTestOrder query(String id) { 
        LupTestOrder lupTestOrder = mapper.query(id); 
        //从redis获取数据字典数据;
        JSONArray sysDictData = getCacheComponent.getCache("sys_dict_data");
        //从redis获取目录分类;
        JSONArray sysCatalog = getCacheComponent.getCache("sys_catalog");
        //将数据表lup_test_agent中根据id字段取出符合符合条件的数据
        List<LupTestAgent> lupTestAgentList = lupTestAgentMapper.listsIn("id",!lupTestOrder.getAgentId().isEmpty() ? StringUtils.join(lupTestOrder.getAgentId(), ",") : "");
        //将数据表sys_areas中根据id字段取出符合符合条件的数据
        List<SysAreas> sysAreasList = sysAreasMapper.listsIn("area_code",!lupTestOrder.getAreaCode().isEmpty() ? StringUtils.join(lupTestOrder.getAreaCode(), ",") : "");
        //将数据表sys_provinces中根据id字段取出符合符合条件的数据
        List<SysProvinces> sysProvincesList = sysProvincesMapper.listsIn("province_code",!lupTestOrder.getProvinceCode().isEmpty() ? StringUtils.join(lupTestOrder.getProvinceCode(), ",") : "");
        //将数据表sys_cities中根据id字段取出符合符合条件的数据
        List<SysCities> sysCitiesList = sysCitiesMapper.listsIn("city_code",!lupTestOrder.getCityCode().isEmpty() ? StringUtils.join(lupTestOrder.getCityCode(), ",") : "");
        //将数据表lup_test_user中根据id字段取出符合符合条件的数据
        List<LupTestUser> lupTestUserList = lupTestUserMapper.listsIn("id",!lupTestOrder.getUserId().isEmpty() ? StringUtils.join(lupTestOrder.getUserId(), ",") : "");
        lupTestOrder.setOrderTypeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDictData(lupTestOrder.getOrderType().toString(), "test_order:type", sysDictData), SysDictData.class));
        lupTestOrder.setOrderTagsRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayDictData(lupTestOrder.getOrderTags(), "order:tags", sysDictData), SysDictData.class));
        lupTestOrder.setGoodsTypeRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayCatalog(lupTestOrder.getGoodsType(), sysCatalog), SysCatalog.class));
        lupTestOrder.setAgentIdRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("id", lupTestOrder.getAgentId(), lupTestAgentList), LupTestAgent.class));
        lupTestOrder.setAreaCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("areaCode", lupTestOrder.getAreaCode(), sysAreasList), SysAreas.class));
        lupTestOrder.setProvinceCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("provinceCode", lupTestOrder.getProvinceCode(), sysProvincesList), SysProvinces.class));
        lupTestOrder.setCityCodeRes(JsonUtils.jsonObject2Domain(DataComponent.getJsonDbField("cityCode", lupTestOrder.getCityCode(), sysCitiesList), SysCities.class));
        lupTestOrder.setUserIdRes(JsonUtils.jsonArray2Domain(DataComponent.getArrayDbField("id", lupTestOrder.getUserId(), lupTestUserList), LupTestUser.class));
 
        return lupTestOrder; 
    } 
 
    @Override 
    public int del(JSONObject idsJson) { 
        String ids = idsJson.getString("ids"); 
        String[] strArray = ids.split(","); 
        int delTotalNum = 0; 
        for (String id : strArray) { 
            int num = this.mapper.del(id); 
            delTotalNum += num; 
        } 
        
        return delTotalNum; 
    } 
 
    @Override 
    public int edit(LupTestOrder lupTestOrder) { 
        int editNum = this.mapper.edit(lupTestOrder); 
        if(editNum == 0) throw new LupException("您未修改任何数据"); 
        
        return editNum; 
    } 
 
    @Override 
    public int add(LupTestOrder lupTestOrder) { 
        lupTestOrder.setId(this.snowflake.nextId()); 
        lupTestOrder.setCreateTime(DateUtils.getNowDate()); 
        int addNum = this.mapper.add(lupTestOrder); 
        if(addNum == 0) throw new LupException("添加失败"); 
        
        return addNum; 
    } 
 
} 
