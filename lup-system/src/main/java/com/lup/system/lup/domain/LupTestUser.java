/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-02 14:49:11; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
 
package com.lup.system.lup.domain; 
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date; 
import lombok.Data; 
 
@Data 
public class LupTestUser { 

    //ID
    private String id;

    //用户名称
    @Size(max = 50,message = "用户名称不能超过{max}个字符")
    @NotEmpty(message = "用户名称不能为空")
    private String userName;

    //登录密码
    private String password;

    //手机号
    @Size(max = 50,message = "手机号不能超过{max}个字符")
    @NotEmpty(message = "手机号不能为空")
    private String tel;

    //邮箱
    @Size(max = 50,message = "邮箱不能超过{max}个字符")
    @NotEmpty(message = "邮箱不能为空")
    private String email;

    //个人主页
    @Size(max = 50,message = "个人主页不能超过{max}个字符")
    @NotEmpty(message = "个人主页不能为空")
    private String homeSite;

    //年龄
    @NotNull(message = "年龄不能为空")
    private Integer age;

    //存款
    @NotNull(message = "存款不能为空")
    private Double balance;

    //身份证号码
    private String idNo;

    //创建时间
    private Date createTime;
 
} 
