/** 
 * [LupMisAllowedSync] 
 * 本代码为系统自动生成代码，请根据自己业务进行修改; 
 * 生成时间 2022-08-02 14:49:15; 
 * 版权所有 2020-2022 lizhongwen，并保留所有权利; 
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布; 
 * 作者: 中文Lee; 
 * 作者主页: http://www.lizhongwen.com; 
 * 邮箱: 360811363@qq.com; 
 * QQ: 360811363; 
 */ 
 
package com.lup.system.lup.service.impl; 
 
import java.util.List; 
import com.lup.core.utils.*; 
import org.springframework.stereotype.Service; 
import com.alibaba.fastjson.JSONObject; 
import com.lup.core.config.exception.LupException; 
import lombok.AllArgsConstructor; 
import com.lup.system.lup.domain.LupTestAgent; 
import com.lup.system.lup.mapper.LupTestAgentMapper; 
import com.lup.system.lup.service.LupTestAgentService; 
 
 
@AllArgsConstructor 
@Service 
public class LupTestAgentServiceImpl implements LupTestAgentService { 
    private final LupTestAgentMapper mapper; 
    private final Snowflake snowflake; 
 
 
    @Override 
    public List<LupTestAgent> lists(JSONObject dataJson) { 
        List<LupTestAgent> list = this.mapper.lists(dataJson); 
 
        return list; 
    } 
 
    @Override 
    public LupTestAgent query(String id) { 
        LupTestAgent lupTestAgent = mapper.query(id); 
 
        return lupTestAgent; 
    } 
 
    @Override 
    public int del(JSONObject idsJson) { 
        String ids = idsJson.getString("ids"); 
        String[] strArray = ids.split(","); 
        int delTotalNum = 0; 
        for (String id : strArray) { 
            int num = this.mapper.del(id); 
            delTotalNum += num; 
        } 
        
        return delTotalNum; 
    } 
 
    @Override 
    public int edit(LupTestAgent lupTestAgent) { 
        int editNum = this.mapper.edit(lupTestAgent); 
        if(editNum == 0) throw new LupException("您未修改任何数据"); 
        
        return editNum; 
    } 
 
    @Override 
    public int add(LupTestAgent lupTestAgent) { 
        lupTestAgent.setId(this.snowflake.nextId()); 
        lupTestAgent.setCreateTime(DateUtils.getNowDate()); 
        int addNum = this.mapper.add(lupTestAgent); 
        if(addNum == 0) throw new LupException("添加失败"); 
        
        return addNum; 
    } 
 
} 
