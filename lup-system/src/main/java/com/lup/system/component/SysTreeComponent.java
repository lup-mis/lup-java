/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SysTreeComponent {


    public JSONArray getTree(JSONArray data, String pid, Map<String, JSONObject> sysPageMap) {
        JSONArray tree = new JSONArray();
        for (int i = 0; i < data.size(); i++) {
            JSONObject treeJson = new JSONObject(true);
            JSONObject item = data.getJSONObject(i);
            if (item.getString("pid").equals(pid)) {
                // 父亲找到儿子
                treeJson.put("id", item.getString("id"));
                treeJson.put("treeName", item.getString("treeName"));
                if (item.getString("icon").equals("")) {
                    treeJson.put("icon", "'fa fa-circle-o");
                } else {
                    treeJson.put("icon", "fa " + item.getString("icon"));
                }
                treeJson.put("pid", item.getString("pid"));
                treeJson.put("pageId", item.getString("pageId"));
                treeJson.put("sysPageRes", sysPageMap.getOrDefault(item.getString("pageId"), new JSONObject()));
                treeJson.put("child", this.getTree(data, item.getString("id"), sysPageMap));
                tree.add(treeJson);
            }
        }
        return tree;
    }
}
