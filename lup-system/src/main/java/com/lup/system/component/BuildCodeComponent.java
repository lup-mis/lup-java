/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.build.*;
import com.lup.core.config.LupConfig;
import com.lup.core.utils.JsonUtils;
import com.lup.core.utils.ResultUtils;
import com.lup.system.sys.domain.SysField;
import com.lup.system.sys.domain.SysTable;
import com.lup.system.sys.mapper.SysFieldMapper;
import com.lup.system.sys.mapper.SysTableMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public record BuildCodeComponent(SysTableMapper sysTableMapper, SysFieldMapper sysFieldMapper, LupConfig lupConfig) {

    public JSONObject getTemplate(String tableName) {
        //查询数据表信息
        SysTable sysTable = sysTableMapper.getByTableName(tableName, null);
        //查询数据字段
        List<SysField> sysFieldList = this.sysFieldMapper.getByTableName(tableName);

        JSONObject sysTableJson = JsonUtils.domain2JsonObject(sysTable);
        JSONArray sysFieldListArray = JsonUtils.domain2JsonArray(sysFieldList);

        //domain 构建
        BuildDomain buildDomain = new BuildDomain();
        String domainStr = buildDomain.getDomain(sysTableJson, sysFieldListArray);

        //Controller构建
        BuildController buildController = new BuildController();
        String controllerStr = buildController.getController(sysTableJson);

        //service构建
        BuildService buildService = new BuildService();
        String serviceStr = buildService.getService(sysTableJson);

        //serviceImpl构建
        BuildServiceImpl buildServiceImpl = new BuildServiceImpl();
        String serviceImplStr = buildServiceImpl.getServiceImpl(sysTableJson, sysFieldListArray);

        //mapper构建
        BuildMapper buildMapper = new BuildMapper();
        String mapperStr = buildMapper.getMapper(sysTableJson);

        //xml 构建
        BuildXml buildXml = new BuildXml();
        String xmlStr = buildXml.getXml(sysTableJson, sysFieldListArray);

        //index.html 构建
        BuildIndexHtml buildIndexHtml = new BuildIndexHtml(lupConfig);
        String indexStr = buildIndexHtml.getIndexHtml(sysTableJson, sysFieldListArray);

        //add.html构建
        BuildAddHtml buildAddHtml = new BuildAddHtml(lupConfig);
        String addStr = buildAddHtml.getAddHtml(sysTableJson, sysFieldListArray);

        //edit.html构建
        BuildEditHtml buildEditHtml = new BuildEditHtml(lupConfig);
        String editStr = buildEditHtml.getEditHtml(sysTableJson, sysFieldListArray);

        JSONObject json = new JSONObject(true);
        json.put("domain", domainStr);
        json.put("controller", controllerStr);
        json.put("service", serviceStr);
        json.put("serviceImpl", serviceImplStr);
        json.put("mapper", mapperStr);
        json.put("xml", xmlStr);
        json.put("index", indexStr);
        json.put("add", addStr);
        json.put("edit", editStr);
        return ResultUtils.success(json);
    }
}
