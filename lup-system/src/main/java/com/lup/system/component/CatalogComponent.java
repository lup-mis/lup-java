/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class CatalogComponent {
    public JSONArray catalogList = new JSONArray();

    public void catalogList(Object obj, String id) {
        JSONArray lists = JSONArray.parseArray(JSON.toJSONString(obj));
        for (int i = 0; i < lists.size(); i++) {
            JSONObject jsonObject = lists.getJSONObject(i);
            if (jsonObject != null) {
                if (jsonObject.getString("pid").equals(id)) {
                    this.catalogList.add(jsonObject);
                    this.catalogList(obj, jsonObject.getString("id"));
                }
            }
        }
    }
}
