/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.RedisUtils;
import com.lup.system.sys.domain.SysConfig;
import com.lup.system.sys.mapper.MySqlMapper;
import com.lup.system.sys.mapper.SysConfigMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;


@Component
public record GetCacheComponent(RedisUtils redisUtils, SysConfigMapper sysConfigMapper, MySqlMapper mySqlMapper,SetCacheComponent setCacheComponent) {

    //获取配置信息
    public JSONObject config() {
        String redisStr = this.redisUtils.get("table:sys_config");
        JSONObject json;
        if (StringUtils.isEmpty(redisStr)) {
            SysConfig lists = this.sysConfigMapper.lists();
            json = JSONObject.parseObject(JSON.toJSONString(lists));
            //写入redis
            redisUtils.add("table:sys_config", json);
        } else {
            json = JSONObject.parseObject(redisStr);
        }
        return json;
    }


    //获取数据表缓存,如果没有则添加缓存
    public JSONArray getCache(String tableName) {
        String redisStr = this.redisUtils.get("table:"+tableName);
        if (StringUtils.isEmpty(redisStr)) {
            setCacheComponent.setCache(tableName);
            redisStr = this.redisUtils.get("table:"+tableName);
        }
        return JSONArray.parseArray(redisStr);
    }
}
