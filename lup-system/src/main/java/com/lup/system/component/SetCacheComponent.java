/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lup.core.utils.RedisUtils;
import com.lup.system.sys.domain.SysConfig;
import com.lup.system.sys.mapper.MySqlMapper;
import com.lup.system.sys.mapper.SysConfigMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public record SetCacheComponent(RedisUtils redisUtils, SysConfigMapper sysConfigMapper,MySqlMapper mySqlMapper) {

    //获取配置信息
    public void config() {
        SysConfig lists = this.sysConfigMapper.lists();
        JSONObject json = JSONObject.parseObject(JSON.toJSONString(lists));
        //写入redis
        redisUtils.add("table:sys_config", json);
    }

    /**
     * 设置所有表的缓存
     */
    public void setCache() {
        List<Map<String, Object>> res = mySqlMapper.getRedisTable();
        for (Map<String, Object> item : res) {
            String tableName = item.get("tableName").toString();
            String orderBy = item.get("orderBy").toString();
            List<Map<String, Object>> tableRes = mySqlMapper.getListsByTable(tableName, orderBy);
            if (tableName.equals("sys_config")) {
                Map<String, Object> configRes = tableRes.get(0);
                redisUtils.add("table:sys_config", JSONObject.parseObject(JSON.toJSONString(configRes)));
            } else {
                List<Map<String, Object>> setRs = new ArrayList<>();
                for (Map<String, Object> obj : tableRes) {
                    obj.put("id",obj.get("id").toString());
                    setRs.add(obj);
                }
                redisUtils.add("table:"+tableName, JSONArray.parseArray(JSON.toJSONString(setRs)));
            }
        }
    }

    /**
     * 设置某张表的缓存
     */
    public void setCache(String tableName) {

        Map<String, Object> rs = mySqlMapper.getTableAttr(tableName);
        int isRedis = (int) rs.get("isRedis");
        String orderBy = rs.get("orderBy").toString();
        if (isRedis == 1) {
            List<Map<String, Object>> tableRes = mySqlMapper.getListsByTable(tableName, orderBy);
            List<Map<String, Object>> setRs = new ArrayList<>();
            for (Map<String, Object> obj : tableRes) {
                obj.put("id",obj.get("id").toString());
                setRs.add(obj);
            }

            redisUtils.add("table:"+tableName, JSONArray.parseArray(JSON.toJSONString(setRs)));
        }
    }

}
