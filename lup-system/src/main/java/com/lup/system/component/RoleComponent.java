/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 * 获取角色权限信息
 */
@Component
public record RoleComponent(GetCacheComponent getCacheComponent) {

    //获取某用户的菜单集合
    public Set<String> getTreeIds(String accessUid){
        Set<String> treeIdsArray = new TreeSet<>();
        Set<String> roleIdsArray = new TreeSet<>();
        JSONArray sysRoleTreeArray = this.getCacheComponent.getCache("sys_role_tree");
        JSONArray sysAccountArray = this.getCacheComponent.getCache("sys_account");
        for (int i = 0; i < sysAccountArray.size(); i++) {
            JSONObject accountRes = sysAccountArray.getJSONObject(i);
            if (accountRes.getString("id").equals(accessUid)) {
                String[] _roleIdsArray = accountRes.getString("roleIds").split(",");
                Collections.addAll(roleIdsArray, _roleIdsArray);
            }
        }
        for (String roleId : roleIdsArray) {
            //组装菜单id
            for (int i=0;i<sysRoleTreeArray.size();i++){
                JSONObject sysRoleTreeRes = sysRoleTreeArray.getJSONObject(i);
                if (sysRoleTreeRes.getString("roleId").equals(roleId)) {
                    treeIdsArray.add(sysRoleTreeRes.getString("treeId"));
                }
            }
        }
        return treeIdsArray;
    }
    //根据用户id获取操作功能
    public JSONObject getRolePageAction(String accessUid) {
        Set<String> pageIdsArray = new TreeSet<>();
        Set<String> actionIdsArray = new TreeSet<>();
        Set<String> roleIdsArray = new TreeSet<>();
        //获取用户操作功能
        JSONArray sysRolePageActionArray = this.getCacheComponent.getCache("sys_role_page_action");
        //获取所有的页面
        JSONArray sysPageArray = this.getCacheComponent.getCache("sys_page");
        //获取所有的操作功能
        JSONArray sysActionArray = this.getCacheComponent.getCache("sys_action");
        //从缓存获取所有用户信息（这里获取所有用户信息，然后从列表中选择该用户）
        JSONArray sysAccountArray = this.getCacheComponent.getCache("sys_account");
        for (int i = 0; i < sysAccountArray.size(); i++) {
            JSONObject accountRes = sysAccountArray.getJSONObject(i);
            if (accountRes.getString("id").equals(accessUid)) {
                String[] _roleIdsArray = accountRes.getString("roleIds").split(",");
                Collections.addAll(roleIdsArray, _roleIdsArray);
            }
        }
        for (String roleId : roleIdsArray) {
            for (int i = 0; i < sysRolePageActionArray.size(); i++) {
                JSONObject sysRolePageActionRes = sysRolePageActionArray.getJSONObject(i);
                if (sysRolePageActionRes.getString("roleId").equals(roleId)) {
                    String[] _actionIdsArray = sysRolePageActionRes.getString("actionIds").split(",");
                    for (String actionId : _actionIdsArray) {
                        if (actionId.equals("auth")) {
                            pageIdsArray.add(sysRolePageActionRes.getString("pageId"));
                        } else {
                            actionIdsArray.add(actionId);
                        }
                    }
                }
            }
        }
        //组装pageCode
        Set<String> pageCodeArray = new TreeSet<>();
        for (String pageId : pageIdsArray) {
            for (int i = 0; i < sysPageArray.size(); i++) {
                JSONObject _json = sysPageArray.getJSONObject(i);
                if (_json.getString("id").equals(pageId)) {
                    String pageCode = _json.getString("code");
                    pageCodeArray.add(pageCode);
                }
            }
        }
        //组装actionCode
        Set<String> actionCodeArray = new TreeSet<>();
        for (String actionId : actionIdsArray) {
            for (int i = 0; i < sysActionArray.size(); i++) {
                JSONObject _json = sysActionArray.getJSONObject(i);
                if (_json.getString("id").equals(actionId)) {
                    String actionCode = _json.getString("code");
                    actionCodeArray.add(actionCode);
                }
            }
        }
        JSONObject retData = new JSONObject(true);
        retData.put("pageId", pageIdsArray);
        retData.put("actionId", actionIdsArray);
        retData.put("pageCode", pageCodeArray);
        retData.put("actionCode", actionCodeArray);
        return retData;
    }
}
