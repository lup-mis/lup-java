/**
 * [LupMisNotAllowedSync]
 * 本代码为系统自动生成代码，请根据自己业务进行修改;
 * 生成时间 2021-08-21 07:52:57;
 * 版权所有 2020-2021 lizhongwen，并保留所有权利;
 * 说明: 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用；不允许对程序代码以任何形式任何目的的再发布;
 * 作者: 中文Lee;
 * 作者主页: http://www.lizhongwen.com;
 * 邮箱: 360811363@qq.com;
 * QQ: 360811363;
 */
package com.lup.system.component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
public class DataComponent {
    //    private final GetCacheComponent getCacheComponent;
//    public static GetCacheComponent staticGetCacheComponent;
//    private static JSONArray sysDictData;
//    public DataComponent(GetCacheComponent getCacheComponent) {
//        this.getCacheComponent = getCacheComponent;
//    }
//    @PostConstruct
//    public void init() {
//        staticGetCacheComponent = this.getCacheComponent;
//        log.info("系统启动中。。。执行初始化。。");
//    }
//
//    @PreDestroy
//    public void destory() {
//        log.info("系统运行结束。。");
//    }
    //获取db-table 的字段映射数据（多条）
    public static JSONArray getArrayDbField(String key, String value, Object res) {

        if (StringUtils.isEmpty(value)) return new JSONArray();
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(res));
        JSONArray retArray = new JSONArray();
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            String[] valueArray = value.split(",");
            for (String v : valueArray) {
                if (jsonObject.get(key).toString().equals(v)) {
                    retArray.add(jsonObject);
                }
            }
        }
        return retArray;
    }
    //获取db-table 的字段映射数据（单条条）

    public static JSONObject getJsonDbField(String key, String value, Object res) {
        if (StringUtils.isEmpty(value)) return new JSONObject();
        JSONArray array = JSONArray.parseArray(JSON.toJSONString(res));
        for (int i = 0; i < array.size(); i++) {
            JSONObject jsonObject = array.getJSONObject(i);
            if (jsonObject.get(key).toString().equals(value)) {
                return jsonObject;
            }
        }
        return new JSONObject();
    }

    //获取字典数据(单条)
    public static JSONObject getJsonDictData(String value, String dictCode, JSONArray sysDictData) {
        if (StringUtils.isEmpty(value)) return new JSONObject();
        JSONObject retJson = new JSONObject();
        for (int i = 0; i < sysDictData.size(); i++) {
            JSONObject jsonObject = sysDictData.getJSONObject(i);
            if (jsonObject.getString("dictCode").equals(dictCode)) {
                if (value.equals(jsonObject.getString("dictValue"))) {
                    return jsonObject;
                }
            }
        }
        return retJson;
    }

    //获取字典数据(多条)
    public static JSONArray getArrayDictData(String value, String dictCode, JSONArray sysDictData) {
        if (StringUtils.isEmpty(value)) return new JSONArray();
        JSONArray retArray = new JSONArray();
        for (int i = 0; i < sysDictData.size(); i++) {
            JSONObject jsonObject = sysDictData.getJSONObject(i);
            if (jsonObject.getString("dictCode").equals(dictCode)) {
                if (Arrays.asList(value.split(",")).contains(jsonObject.getString("dictValue"))) {
                    retArray.add(jsonObject);
                }
            }
        }
        return retArray;
    }

    //获取catalog(多条)
    public static JSONArray getArrayCatalog(String value, JSONArray catalog) {
        if (StringUtils.isEmpty(value)) return new JSONArray();
        JSONArray retArray = new JSONArray();
        for (int i = 0; i < catalog.size(); i++) {
            JSONObject jsonObject = catalog.getJSONObject(i);
            if (Arrays.asList(value.split(",")).contains(jsonObject.getString("id"))) {
                retArray.add(jsonObject);
            }
        }
        return retArray;
    }

    //获取catalog(单条)
    public static JSONObject getJsonCatalog(String value, JSONArray catalog) {
        if (StringUtils.isEmpty(value)) return new JSONObject();
        JSONObject retJson = new JSONObject();
        for (int i = 0; i < catalog.size(); i++) {
            JSONObject jsonObject = catalog.getJSONObject(i);
            if (value.equals(jsonObject.getString("id"))) {
                return jsonObject;
            }
        }
        return retJson;
    }
}
